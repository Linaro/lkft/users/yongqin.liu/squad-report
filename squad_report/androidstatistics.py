#!/usr/bin/env python3

import argparse
import bugzilla
import datetime
import jinja2
import json
import math
import logging
import os
import re
import resource
import tarfile
import tempfile
import threading
import time
import sys
import zipfile

import xml.etree.ElementTree as ET

import squad_report.logging

from collections import Counter, defaultdict, OrderedDict
from urllib.parse import urlparse

from squad_client.core.models import Squad, Build
from squad_report.androidreportconfig import get_all_report_kernels, get_all_report_projects, get_all_kerver_reports
from squad_report.androidreport import download_attachments_save_result, get_lkft_build_status
from squad_report.androidreport import reset_qajob_failure_msg, get_classified_jobs, is_cts_vts_job, get_job_short_name
from squad_report.androidcommon import get_aware_datetime_from_str, TestNumbers, thread_pool, LinaroAndroidLKFTBug
from squad_report.androidcommon import set_job_name_for_unsubmitted_jobs

from squad_client.core.api import SquadApi
from squad_client.core.api import ApiException as SquadApiException

# logging.basicConfig(format="[%(asctime)s.%(msecs)03d] %(levelname)s [%(name)s:%(lineno)s] %(message)s")
# logger = logging.getLogger(__name__)
# logger.setLevel("INFO")
logger = squad_report.logging.getLogger(__name__)

# Get a snapshot of the current mem consumption
mem_snapshot = lambda: resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024

tab_spaces = ' ' * 4

def get_parser():
    parser = argparse.ArgumentParser(
        prog="android-report-tools", description="tools for android report work"
    )

    parser.add_argument(
        "--cache",
        default=0,
        type=int,
        help="Cache squad-client requests with a timeout",
    )

    parser.add_argument(
        "--token",
        default=os.environ.get("SQUAD_TOKEN", None),
        help="Authenticate to SQUAD using this token",
    )

    parser.add_argument(
        "--url",
        default=os.environ.get("SQUAD_URL", "https://qa-reports.linaro.org"),
        help="URL of the SQUAD service",
    )

    parser.add_argument(
        "--bugzilla-url",
        dest="bugzilla_url",
        default=os.environ.get("BUGZILLA_URL", "https://bugs.linaro.org"),
        help="URL of the bugzilla service",
    )

    parser.add_argument(
        "--bugzilla-token",
        dest="bugzilla_token",
        default=os.environ.get("BUGZILLA_TOKEN", ""),
        help="Authenticate to bugzilla using this token",
    )

    parser.add_argument(
        "--output",
        default="/tmp/android-statistics.txt",
        help="Write the report to this file",
    )

    return parser

def sort_and_uniq_bugs(src_bugs=[]):
    bugs_sorted = sorted(src_bugs, key=lambda bug: bug.id)
    bug_ids_list = []
    bugs_list = []
    for bug in bugs_sorted:
        if bug.id in bug_ids_list:
            continue
        bug_ids_list.append(bug.id)
        bugs_list.append(bug)
    return bugs_list

def main():
    before_mem = mem_snapshot()
    start = time.time()

    parser = get_parser()
    subparser = parser.add_subparsers(help='available subcommands', dest='command')
    args = parser.parse_args()
    if not args.bugzilla_url.startswith('https://') \
            and not args.bugzilla_url.startswith('http://') \
            and args.bugzilla_url.find('/') > 0:
        print(f"The bugzilla url is not specified correctly: {args.bugzilla_url}")
        return 1

    try:
        SquadApi.configure(url=args.url, token=args.token, cache=args.cache)
    except SquadApiException as sae:
        logger.error("Failed to configure the squad api: %s", sae)
        return -1

    squad = Squad()

    bugzilla_instance = LinaroAndroidLKFTBug(host_name=args.bugzilla_url, api_key=args.bugzilla_token)

    target_squad_projects = []
    all_report_kernels = get_all_report_kernels()
    all_supported_projects = get_all_report_projects()
    all_kerver_reports = get_all_kerver_reports()
    all_kerver_reports_rawkernels = []
    for kerver, projs in all_kerver_reports.items():
        all_kerver_reports_rawkernels.extend(projs)
    kernel_squad_projects_dict = defaultdict(list)
    for kernel, project_alias_names in all_report_kernels.items():
        if not kernel in all_kerver_reports_rawkernels:
            continue
        alias_projects = []
        for project_alias_name in project_alias_names:
            alias_project = all_supported_projects.get(project_alias_name)
            if alias_project is None:
                print("The project is not configured correctly: %s" % project_alias_name)
                return
            alias_project['alias_name'] = project_alias_name
            alias_projects.append(alias_project)

        def get_squad_project(alias_project):
            ## https://gist.github.com/mrchapp/b902ae8f27adfb5b420d271d3e9869d7
            if alias_project.get('slug') and alias_project.get('group'):
                squad_group = squad.group(alias_project.get('group'))
                squad_project = squad_group.project(alias_project.get('slug'))
            elif alias_project.get('project_id'):
                project_id = alias_project.get('project_id')
                squad_project = squad.projects(count=1, id=project_id)[project_id]
            else:
                squad_project = None

            alias_project['squad_project'] = squad_project

        thread_pool(func=get_squad_project, elements=alias_projects, subgroup_count=10)

        for alias_project in alias_projects:
            squad_project = alias_project.get('squad_project', None)
            if squad_project is not None:
                squad_project.project_alias = alias_project
                squad_project.kernel_report = kernel
                target_squad_projects.append(squad_project)
                # alias_name = alias_project.get("alias_name")
                # logger.info(f"{kernel}, {alias_name}")
                kernel_squad_projects_dict[kernel].append(squad_project)
            else:
                print("The project is not configured correctly: %s" % project_alias_name)
                continue

    def sort_by_job_name_url(job):
        return (job.name, job.external_url)

    all_jobs = []
    datetime_now = datetime.datetime.now(datetime.timezone.utc)
    duration_one_week = datetime.timedelta(days=7)
    datetime_since = datetime_now - duration_one_week
    kernel_builds_dict = defaultdict(OrderedDict)
    build_projects_dict = defaultdict(list)
    kc_builds_dict = defaultdict(list)
    project_platform_bugs = {}
    for target_squad_project in target_squad_projects:
        project_alias = target_squad_project.project_alias
        kernel_report = target_squad_project.kernel_report

        branch = project_alias.get('branch')
        builds = list(target_squad_project.builds(count=30, fields="id,version,finished,datetime,created_at", ordering="-datetime").values())

        valid_builds = []
        for build in builds:
            created_at = get_aware_datetime_from_str(build.created_at)
            # TODO: created 2 weeks ago, but updated after that, to catch the NOT FINISHED builds in last last week
            if datetime_now - created_at > duration_one_week:
                print(f"%s {build.version} {build.created_at} over one week" % project_alias.get("alias_name"))
                continue


            build.target_squad_project = target_squad_project

            kernel_builds_dict[kernel_report][build.version] = None

            kernel_build_key = "%s#%s" % (kernel_report, build.version)
            build_projects_dict[kernel_build_key].append(target_squad_project)
            kc_builds_dict[kernel_build_key].append(build)
            valid_builds.append(build)

            print(f"%s {build.version} {build.created_at} less than one week" % project_alias.get("alias_name"))

        def get_build_jobs(build):
            jobs = build.testjobs().values()
            set_job_name_for_unsubmitted_jobs(jobs)

            logger.info("Downloading started for %d jobs of build %s" % (len(jobs), build.version))
            download_attachments_save_result(jobs=list(jobs))
            logger.info("Downloading ended for %d jobs of build %s" % (len(jobs), build.version))

        thread_pool(func=get_build_jobs, elements=valid_builds, subgroup_count=10)
        target_squad_project.bugs = bugzilla_instance.get_bugs_for_project(project_name=target_squad_project.name, cachepool=project_platform_bugs)

    class KernelChangeset():
        kernel_report = None
        build_version = None
        kc_created_at = None
        kc_last_fetched_timestamp = None
        kc_job_durations = []
        kc_status = "UNKNOWN"
        kc_numbers = TestNumbers()
        kc_builds = []
        kc_failures = []
        kc_modules = []

        kc_completed_build_number = 0
        kc_jobs_total_number = 0 # the total number of the jobs be checked for all projects
        kc_jobs_success_number = 0
        kc_jobs_failed = []
        kc_jobs_duration = datetime.timedelta(milliseconds=0)

    kernels_no_update = []
    kernels_updated = []
    projects_all = []
    projects_tested = []
    kernelreport_kcs_dict = OrderedDict()
    summary_jobs_duration = []
    summary_kc_lasted_times = []
    summary_numbers = TestNumbers()
    summary_jobs_total_number = 0
    summary_jobs_success_number = 0
    for kernel_report, project_alias_names in all_report_kernels.items():
        if not kernel_report in all_kerver_reports_rawkernels:
            continue
        kcs = kernel_builds_dict[kernel_report].keys()
        projects_all.extend(project_alias_names)
        if kcs is None or len(kcs) == 0:
            kernels_no_update.append(kernel_report)
            continue
        else:
            kernels_updated.append(kernel_report)
            projects_tested.extend(project_alias_names)

        kc_obj_list = []
        for kc in kcs:
            kernel_build_key = "%s#%s" % (kernel_report, kc)
            kc_builds = kc_builds_dict.get(kernel_build_key)

            kc_numbers = TestNumbers()
            kc_has_cancelled = False
            kc_has_not_submitted = False
            kc_has_inprogress = False
            kc_completed_build_number = 0
            kc_jobs_total_number = 0 # the total number of to be checked jobs
            kc_jobs_success_number = 0
            kc_jobs_failed = []
            kc_last_fetched_timestamp = None
            kc_created_at = None
            kc_job_durations = []
            kc_failures = []
            kc_modules = []
            if kc_builds is None:
                # output.write(" " * 4 * 3 + f"No build tested for {build.version}, please check and try again\n")
                kc_builds = []
            for kc_build in kc_builds:
                target_squad_project = kc_build.target_squad_project
                project_alias = target_squad_project.project_alias
                project_alias_name = project_alias['alias_name']

                jobs = kc_build.testjobs().values()
                set_job_name_for_unsubmitted_jobs(jobs)
                jobs_to_be_checked = get_classified_jobs(jobs=jobs).get('final_jobs')

                # JOBSNOTSUBMITTED, JOBSINPROGRESS, CANCELED, JOBSCOMPLETED
                build_status = get_lkft_build_status(kc_build, jobs_to_be_checked)
                if build_status.get('has_unsubmitted'):
                    kc_has_not_submitted = True
                elif build_status.get('is_inprogress'):
                    kc_has_inprogress = True
                elif build_status.get("has_canceled"):
                    kc_has_cancelled = True
                else:
                    kc_completed_build_number = kc_completed_build_number + 1

                kc_build.last_fetched_timestamp = build_status.get('last_fetched_timestamp')
                if kc_last_fetched_timestamp is None or kc_last_fetched_timestamp < kc_build.last_fetched_timestamp:
                    kc_last_fetched_timestamp = kc_build.last_fetched_timestamp

                kc_build_created_at = get_aware_datetime_from_str(kc_build.created_at)
                if kc_created_at is None or kc_created_at > kc_build_created_at:
                    kc_created_at = kc_build_created_at

                build_jobs_finished_number = 0
                kc_build.numbers = TestNumbers()
                kc_build_job_durations = []
                kc_build.failures = []
                kc_build.modules = []
                for job in jobs_to_be_checked:
                    job.target_squad_project = target_squad_project
                    jobstatus = job.job_status
                    jobfailure = job.failure
                    # for some failed cases, numbers are not set for the job
                    job_numbers = getattr(job, 'numbers', TestNumbers())
                    if is_cts_vts_job(job.name) and jobstatus == 'Complete' and jobfailure is None and \
                            job_numbers is not None and job_numbers.modules_total !=0 :
                        build_jobs_finished_number = build_jobs_finished_number + 1
                    elif jobstatus == 'Complete' and jobfailure is None:
                        # for non-cts vts jobs, assuming it's a finished successful job
                        build_jobs_finished_number = build_jobs_finished_number + 1
                    else:
                        # failed jobs
                        kc_jobs_failed.append(job)

                    if (jobstatus == "Complete" or jobstatus == "Incomplete") \
                            and job.fetched and job.started_at and job.ended_at:
                        job_start_time = get_aware_datetime_from_str(job.started_at, str_format_includes_ms=False)
                        job_end_time = get_aware_datetime_from_str(job.ended_at, str_format_includes_ms=False)
                        job_duration = job_end_time - job_start_time
                        kc_build_job_durations.append(job_duration)

                    kc_build.numbers.addWithTestNumbers(job_numbers)
                    job_failures = getattr(job, 'failures', [])
                    kc_build.failures.extend(job_failures)
                    job_modules = getattr(job, 'modules', [])
                    kc_build.modules.extend(job_modules)

                kc_build.jobs_duration = sum(kc_build_job_durations, datetime.timedelta())
                kc_failures.extend(kc_build.failures)
                kc_modules.extend(kc_build.modules)

                kc_numbers.addWithTestNumbers(kc_build.numbers)
                kc_jobs_total_number = kc_jobs_total_number + len(jobs_to_be_checked)
                kc_jobs_success_number = kc_jobs_success_number + build_jobs_finished_number
                kc_job_durations.extend(kc_build_job_durations)

            # need to consider the builds status too
            kc_status = "UNKNOWN"
            if kc_has_not_submitted:
                kc_status = "JOBSNOTSUBMITTED"
            elif kc_has_inprogress:
                kc_status = 'JOBSINPROGRESS'
            elif kc_has_cancelled:
                kc_status = 'CANCELED'
            else:
                kc_status = 'COMPLETED'

            kc_obj = KernelChangeset()
            kc_obj.kernel_report = kernel_report
            kc_obj.build_version = kc
            kc_obj.kc_status = kc_status
            kc_obj.kc_job_durations = kc_job_durations
            kc_obj.kc_jobs_total_number = kc_jobs_total_number
            kc_obj.kc_jobs_success_number = kc_jobs_success_number
            kc_obj.kc_created_at = kc_created_at
            kc_obj.kc_last_fetched_timestamp = kc_last_fetched_timestamp
            kc_obj.kc_numbers = kc_numbers
            kc_obj.kc_jobs_failed = kc_jobs_failed
            kc_obj.kc_failures = kc_failures
            kc_obj.kc_modules = kc_modules
            kc_obj_list.append(kc_obj)
            summary_jobs_duration.extend(kc_job_durations)
            summary_kc_lasted_times.append(kc_obj.kc_last_fetched_timestamp - kc_obj.kc_created_at)
            summary_numbers.addWithTestNumbers(kc_numbers)
            summary_jobs_total_number = summary_jobs_total_number + kc_jobs_total_number
            summary_jobs_success_number = summary_jobs_success_number + kc_jobs_success_number

        kernelreport_kcs_dict[kernel_report] = kc_obj_list

    bugs_created_before_but_open = []
    bugs_reported_last_week = []
    all_lkft_bugs = bugzilla_instance.get_lkft_bugs()
    for bug in all_lkft_bugs:
        # 'creation_time': '2019-01-13T16:49:14Z',
        # 'last_change_time': '2019-01-13T16:52:49Z',
        bug_created_at = get_aware_datetime_from_str(bug.creation_time, str_format_includes_ms=False)
        if (datetime_now - bug_created_at < duration_one_week) or (datetime_now - bug_created_at == duration_one_week):
            # bugs created in last week, no matter cloesed or not
            bugs_reported_last_week.append(bug)
        elif (bug.status != "RESOLVED" and bug.status != "VERIFIED" ):
            # bugs created before but still open
            bugs_created_before_but_open.append(bug)
        else:
            # bugs created before last week, and closed
            continue

    with open(args.output, 'w') as output:
        output.write("=====================================================================\n")
        output.write("Google Meeting (complete)(%s ~ %s)\n" % (datetime_since.strftime("%Y-%m-%d %H:%M:%S"), datetime_now.strftime("%Y-%m-%d %H:%M:%S")))
        output.write("=====================================================================\n")
        output.write("Testing results\n")
        output.write(tab_spaces * 1 + "Issues:\n")
        for kerver, kernel_reports in all_kerver_reports.items():
            for kernel_report in kernel_reports:
                open_bugs = []
                for squad_project in kernel_squad_projects_dict.get(kernel_report, []):
                    for bug in squad_project.bugs :
                        bug_created_at = get_aware_datetime_from_str(bug.creation_time, str_format_includes_ms=False)
                        if (bug.status == "RESOLVED" or bug.status == "VERIFIED" ) and (datetime_now - bug_created_at > duration_one_week):
                            continue
                        open_bugs.append(bug)
                if len(open_bugs) > 0:
                    kc_obj_list = kernelreport_kcs_dict.get(kernel_report, [])
                    kc_build_vers = []
                    for kc_obj in kc_obj_list:
                        if kc_obj.kc_status == "JOBSNOTSUBMITTED" or kc_obj.kc_status == "JOBSINPROGRESS":
                            kc_build_vers.append(f"{kc_obj.build_version}*")
                        else:
                            kc_build_vers.append(f"{kc_obj.build_version}")
                    kc_build_vers_str = ", ".join(kc_build_vers)
                    if kc_build_vers_str:
                        output.write(tab_spaces * 2 + f"{kernel_report}: {kc_build_vers_str}\n")
                    else:
                        output.write(tab_spaces * 2 + f"{kernel_report}:\n")

                    open_bugs = sort_and_uniq_bugs(src_bugs=open_bugs)
                    for bug in open_bugs:
                        output.write(tab_spaces * 3 + f"{bug.id} {bug.status} (\"{bug.summary}\")\n")

        for kerver, kernel_reports in all_kerver_reports.items():
            kerver_updated_kernels = []
            for kernel_report in kernel_reports:
                if kernel_report in kernels_updated:
                    kerver_updated_kernels.append(kernel_report)
            if len(kerver_updated_kernels) > 0:
                output.write(tab_spaces * 1 + f"{kerver}:\n")
                for kernel_report in kerver_updated_kernels:
                    kc_obj_list = kernelreport_kcs_dict.get(kernel_report)
                    kc_build_vers = []
                    for kc_obj in kc_obj_list:
                        if kc_obj.kc_status == "JOBSNOTSUBMITTED" or kc_obj.kc_status == "JOBSINPROGRESS":
                            kc_build_vers.append(f"{kc_obj.build_version}*")
                        else:
                            kc_build_vers.append(f"{kc_obj.build_version}")
                    kc_build_vers_str = ", ".join(kc_build_vers)
                    output.write(tab_spaces * 2 + f"{kernel_report}: {kc_build_vers_str}\n")

        if len(kernels_no_update) == 0:
            output.write(tab_spaces * 1 + f"Untested: All kernels have updates last week\n")
        else:
            output.write(tab_spaces * 1 + f"Untested this week:\n")
            kernels_no_update_str = ", ".join(kernels_no_update)
            output.write(tab_spaces * 2 + f"{kernels_no_update_str}\n")


        output.write("\n")
        output.write("\n")
        output.write("=====================================================================\n")
        output.write("Summary since last week(%s ~ %s)\n" % (datetime_since.strftime("%Y-%m-%d %H:%M:%S"), datetime_now.strftime("%Y-%m-%d %H:%M:%S")))
        output.write("=====================================================================\n")
        summary_duration = sum(summary_jobs_duration, datetime.timedelta())
        dut_days = summary_duration.days + summary_duration.seconds / 3600 / 24
        output.write(tab_spaces * 1 + f"Cost: {len(bugs_reported_last_week)} bugs reported / {dut_days:.2f} DUT days\n")
        output.write(tab_spaces * 1 + f"Test Cases: {summary_jobs_success_number}/{summary_jobs_total_number} jobs success/total, " + \
                                                    f"{summary_numbers.modules_done}/{summary_numbers.modules_total} modules done/total, " + \
                                                    f"{summary_numbers.number_passed} passed, {summary_numbers.number_failed} failed, " + \
                                                    f"{summary_numbers.number_assumption_failure} assumption failures, {summary_numbers.number_ignored} ignored, "+ \
                                                    f"{summary_numbers.number_total} total\n")
        output.write(tab_spaces * 1 + f"Kernel tested of Total: {len(kernels_updated)} / {len(all_report_kernels)}\n")
        if (len(kernels_updated)) == 0:
            output.write(tab_spaces * 2 + f"Tested Kernels: " + "No kernel updated since last week\n")
        else:
            output.write(tab_spaces * 2 + f"Tested Kernels: " + ", ".join(kernels_updated) + "\n")
        if len(kernels_no_update) == 0:
            output.write(tab_spaces * 2 + "No update Kernels: All kernels have updates last week\n")
        else:
            output.write(tab_spaces * 2 + "No update Kernels: " + ", ".join(kernels_no_update) + "\n")

        output.write(tab_spaces * 1 + f"Projects tested of Total: {len(projects_tested)} / {len(projects_all)}\n")
        output.write(tab_spaces * 1 + f"Change Sets tested Total: {len(kc_builds_dict)}\n")
        sum_kc_lasted_time = sum(summary_kc_lasted_times, datetime.timedelta())
        output.write(tab_spaces * 1 + f"Time lasted per Change Set:\n")
        output.write(tab_spaces * 2 + f"Maximum: {max(summary_kc_lasted_times)}\n")
        output.write(tab_spaces * 2 + f"Minimum: {min(summary_kc_lasted_times)}\n")
        output.write(tab_spaces * 2 + f"Average: {sum_kc_lasted_time / len(summary_kc_lasted_times)} for {len(summary_kc_lasted_times)} Changesets\n")
        output.write(tab_spaces * 1 + f"Jobs Execution Time\n")
        output.write(tab_spaces * 2 + f"Total: {summary_duration}\n")
        output.write(tab_spaces * 2 + f"Maximum: {max(summary_jobs_duration)}\n")
        output.write(tab_spaces * 2 + f"Minimum: {min(summary_jobs_duration)}\n")
        output.write(tab_spaces * 2 + f"Average: {summary_duration / len(summary_jobs_duration)} for {len(summary_jobs_duration)} Jobs\n")
        output.write(tab_spaces * 1 + f"Kernel Build Duration: TBD\n")
        bugs_reported_last_week = sort_and_uniq_bugs(src_bugs=bugs_reported_last_week)
        output.write(tab_spaces * 1 + f"Bugs reported last week: {len(bugs_reported_last_week)}\n")
        for bug in bugs_reported_last_week:
            output.write(tab_spaces * 2 + f"{bug.id} {bug.status} {bug.assigned_to} (\"{bug.summary}\")\n")
        bugs_created_before_but_open = sort_and_uniq_bugs(src_bugs=bugs_created_before_but_open)
        output.write(tab_spaces * 1 + f"Open bugs created before last week: {len(bugs_created_before_but_open)}\n")
        for bug in bugs_created_before_but_open:
            output.write(tab_spaces * 2 + f"{bug.id} {bug.status} {bug.assigned_to} (\"{bug.summary}\")\n")

        output.write("\n")
        output.write("\n")
        output.write("=====================================================================\n")
        output.write("Details since last week(%s ~ %s)\n" % (datetime_since.strftime("%Y-%m-%d %H:%M:%S"), datetime_now.strftime("%Y-%m-%d %H:%M:%S")))
        output.write("=====================================================================\n")
        for kernel_report, kc_obj_list in kernelreport_kcs_dict.items():
            output.write(tab_spaces + f"{kernel_report}:\n")
            for kc_obj in kc_obj_list:
                # need to consider the builds status too
                kc_status = kc_obj.kc_status
                kc_numbers = kc_obj.kc_numbers
                kc_jobs_failed = kc_obj.kc_jobs_failed
                kc_created_at = kc_obj.kc_created_at
                kc_last_fetched_timestamp = kc_obj.kc_last_fetched_timestamp
                kc_jobs_success_number = kc_obj.kc_jobs_success_number
                kc_jobs_total_number = kc_obj.kc_jobs_total_number


                output.write(tab_spaces * 2 + f"{kc_obj.build_version} {kc_status} {kc_jobs_success_number}/{kc_jobs_total_number} jobs success/total, " + \
                                                                                f"{kc_numbers.modules_done}/{kc_numbers.modules_total} modules done/total, " + \
                                                                                f"{kc_numbers.number_passed} passed, {kc_numbers.number_failed} failed, {kc_numbers.number_assumption_failure} assumption failures, " + \
                                                                                f"{kc_numbers.number_ignored} ignored,  {kc_numbers.number_total} total\n")
                for job in kc_jobs_failed:
                    job_short_nme = get_job_short_name(job.name)
                    target_squad_project = job.target_squad_project
                    project_alias = target_squad_project.project_alias
                    project_alias_name = project_alias['alias_name']
                    failure_msg = "UNKNOWN"
                    if job.failure: # and job.failure.get('error_msg'):
                        failure_msg_dict = reset_qajob_failure_msg(job.failure)
                        failure_msg = failure_msg_dict.get('error_msg', 'Failed to parse')
                    if job.job_status == "Running"  or job.job_status == "Submitted":
                        output.write(tab_spaces * 3 + f"{project_alias_name} {job.external_url} {job_short_nme} {job.job_status}\n")
                    else:
                        output.write(tab_spaces * 3 + f"{project_alias_name} {job.external_url} {job_short_nme} {job.job_status} (\"{failure_msg}\")\n")

                output.write("\n")
                if kc_created_at and kc_last_fetched_timestamp:
                    output.write(tab_spaces * 3 + f"Time used: %s\n" % (kc_last_fetched_timestamp - kc_created_at))
                    output.write(tab_spaces * 4 + f"# Including queue time, from when the build created to when the last job fetched by SQUAD)\n" )
                if kc_created_at:
                    output.write(tab_spaces * 4 + f"Builds created since: %s\n" % kc_created_at.strftime("%Y-%m-%d %H:%M:%S"))
                if kc_last_fetched_timestamp:
                    output.write(tab_spaces * 4 + f"Jobs last fetched at: %s\n" % kc_last_fetched_timestamp.strftime("%Y-%m-%d %H:%M:%S"))

                kc_job_durations = kc_obj.kc_job_durations
                if kc_job_durations:
                    # output.write(" " * 4 * 4 + f"Kernel builds duration: TBD)\n")
                    kc_jobs_duration = sum(kc_job_durations, datetime.timedelta())
                    output.write(tab_spaces * 3 + f"Jobs Execution Time: {kc_jobs_duration} Total, {max(kc_job_durations)} Maximum, {min(kc_job_durations)} Minimum, {kc_jobs_duration/len(kc_job_durations)} Average\n")
                    output.write(tab_spaces * 4 + f"# Not include information for jobs resubmitted\n")
                else:
                    output.write(tab_spaces * 3 + f"Jobs Execution Time: no job finished yet\n")

                modules_undone = []
                modules_done_with_failures = []
                for module in kc_obj.kc_modules:
                    if module.get('done'):
                        number_passed = module.get('number_passed')
                        number_total = module.get('number_total')
                        if number_total != 0 and number_total != number_passed:
                            modules_done_with_failures.append(module)
                        continue
                    else:
                        modules_undone.append(module)

                def module_name_abi_environment(module):
                    return(module.get('name'), module.get('abi'), module.get('job').environment)

                modules_undone_sorted = sorted(modules_undone, key=module_name_abi_environment)
                if len(modules_undone_sorted) > 0:
                    output.write(tab_spaces * 3 + f"Modules Undone({len(modules_undone_sorted)}):\n")
                    for module in modules_undone_sorted:
                        module_name = module.get('name')
                        module_abi = module.get('abi')
                        number_passed = module.get('number_passed')
                        number_total = module.get('number_total')
                        job = module.get('job')
                        target_squad_project = job.target_squad_project
                        project_alias = target_squad_project.project_alias
                        project_alias_name = project_alias['alias_name']
                        reason = module.get('reason')
                        if reason is not None:
                            output.write(tab_spaces * 4 + f"{module_name} {module_abi} {project_alias_name} {number_passed}/{number_total} pass/total (\"{reason}\")\n")
                        else:
                            output.write(tab_spaces * 4 + f"{module_name} {module_abi} {project_alias_name} {number_passed}/{number_total} pass/total\n")

                # modules_done_with_failures_sorted = sorted(modules_done_with_failures, key=module_name_abi_environment)
                # if len(modules_done_with_failures_sorted) > 0:
                #     output.write(tab_spaces * 3 + f"Modules with Failures({len(modules_done_with_failures_sorted)}):\n")
                #     for module in modules_done_with_failures_sorted:
                #         module_name = module.get('name')
                #         module_abi = module.get('abi')
                #         number_passed = module.get('number_passed')
                #         number_total = module.get('number_total')
                #         job = module.get('job')
                #         target_squad_project = job.target_squad_project
                #         project_alias = target_squad_project.project_alias
                #         project_alias_name = project_alias['alias_name']
                #         output.write(tab_spaces * 4 + f"{module_name} {module_abi} {project_alias_name} {number_passed}/{number_total} pass/total\n")

                # if len(kc_obj.kc_failures) > 0:
                #     output.write(tab_spaces * 3 + f"Failures reported({len(kc_obj.kc_failures)}):\n")

                #     for failure in kc_obj.kc_failures:
                #         testsuite = failure.get('testsuite')
                #         testname = failure.get('test_name')
                #         test_class = failure.get('test_class')
                #         test_method = failure.get('test_method')
                #         result = failure.get('result')
                #         module_name = testsuite.get('name')
                #         module_abi = testsuite.get('abi')
                #         job = failure.get('job')
                #         output.write(tab_spaces * 4 + f"{module_name} {module_abi} {job.environment} {test_class}#{test_method} {result}\n")

            ## TODO: output bugs
            output.write("\n")
            open_bugs = []
            for squad_project in kernel_squad_projects_dict.get(kernel_report, []):
                open_bugs.extend([bug for bug in squad_project.bugs if bug.status != "RESOLVED" and bug.status != "VERIFIED" ])
            open_bugs = sort_and_uniq_bugs(src_bugs=open_bugs)
            output.write(" " * 4 * 2 + f"{len(open_bugs)} bugs filed:\n")
            for bug in open_bugs:
                if bug.status == "RESOLVED":
                    continue
                output.write(" " * 4 * 3 + f"{bug.id} {bug.status} {bug.assigned_to} (\"{bug.summary}\")\n")


    # def job_to_str(job):
    #     branch = job.get('branch')
    #     project_name = job.get('project_name')
    #     build_version = job.get('build_version')
    #     job_name = job.get('job_name')
    #     return f"{branch}|{project_name}|{build_version}|{job_name}"

    # items = ['job_name']

    # for item in items:
    #     counter = Counter(job_to_str(job) for job in all_jobs)
    #     for key in sorted(counter.keys()):
    #         print("%s: %d" % (key, counter.get(key)))
    #     print("===========")
    duration = time.time() - start
    print("PERFORMANCE INFO: it took %.3f seconds to finish the report" % (duration))
    after_mem = mem_snapshot()
    print("PERFORMANCE INFO: mem before: %d MB, mem after: %d MB (%d MB more)" % (before_mem, after_mem, after_mem - before_mem))


if __name__ == "__main__":
    sys.exit(main())
