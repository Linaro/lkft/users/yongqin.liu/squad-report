#!/usr/bin/env python3

import argparse
import datetime
import jinja2
import json
import math
import logging
import os
import re
import resource
import tarfile
import tempfile
import threading
import time
import sys
import zipfile

from io import BytesIO, StringIO
from matplotlib import pyplot as plt

from reportlab.lib import colors
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, ListStyle
from reportlab.lib.units import cm, mm
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, Table, TableStyle, ListFlowable, PageBreak

import xml.etree.ElementTree as ET

import squad_report.logging

from collections import Counter, defaultdict, OrderedDict
from urllib.parse import urlparse

from squad_client.core.models import Squad, Build
from squad_report.androidreportconfig import get_all_report_kernels, get_all_report_projects, get_all_kerver_reports
from squad_report.androidreport import download_attachments_save_result, get_lkft_build_status
from squad_report.androidreport import reset_qajob_failure_msg, get_classified_jobs, is_cts_vts_job, get_job_short_name
from squad_report.androidcommon import get_aware_datetime_from_str, TestNumbers
from squad_report.androidcommon import set_job_name_for_unsubmitted_jobs

from squad_client.core.api import SquadApi
from squad_client.core.api import ApiException as SquadApiException

# logging.basicConfig(format="[%(asctime)s.%(msecs)03d] %(levelname)s [%(name)s:%(lineno)s] %(message)s")
# logger = logging.getLogger(__name__)
# logger.setLevel("INFO")
logger = squad_report.logging.getLogger(__name__)

# Get a snapshot of the current mem consumption
mem_snapshot = lambda: resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024

tab_spaces = ' ' * 4

def get_parser():
    parser = argparse.ArgumentParser(
        prog="android-report-tools", description="tools for android report work"
    )

    parser.add_argument(
        "--cache",
        default=0,
        type=int,
        help="Cache squad-client requests with a timeout",
    )

    parser.add_argument(
        "--thread-count",
        ##default=os.cpu_count(),
        default=10,
        type=int,
        help="How many threads to be run in parallel",
    )


    parser.add_argument(
        "--token",
        default=os.environ.get("SQUAD_TOKEN", None),
        help="Authenticate to SQUAD using this token",
    )

    parser.add_argument(
        "--url",
        default=os.environ.get("SQUAD_URL", "https://qa-reports.linaro.org"),
        help="URL of the SQUAD service",
    )

    parser.add_argument(
        "--output",
        default="/tmp/android-trend.txt",
        help="Write the report to this file",
    )


    return parser


def thread_pool(func=None, elements=[], subgroup_count=10):
    number_of_elements = len(elements)
    number_of_subgroup = math.ceil(number_of_elements/subgroup_count)
    finished_count = 0
    for i in range(number_of_subgroup):
        subgroup_elements = elements[i*subgroup_count: (i+1)*subgroup_count]
        finished_count = finished_count + len(subgroup_elements)

        threads = list()
        for element in subgroup_elements:
            t = threading.Thread(target=func, args=(element,))
            threads.append(t)
            t.start()

        for t in threads:
            t.join()

    logger.info(f"Finished getting information for all elements: number_of_elements={number_of_elements}, number_of_subgroup={number_of_subgroup}, finished_count={finished_count}, subgroup_count={subgroup_count}")


def get_hardware_from_pname(pname=None, env=''):
    if not pname:
        # for aosp master build
        if env.find('hi6220-hikey')>=0:
            return 'HiKey'
        else:
            return None
    if pname.find('hikey960') >= 0:
        return 'HiKey960'
    elif pname.find('hikey') >= 0:
        return 'HiKey'
    elif pname.find('x15') >= 0:
        return 'BeagleBoard-X15'
    elif pname.find('am65x') >= 0:
        return 'AM65X'
    elif pname.find('db845c') >= 0:
        return 'Dragonboard 845c'
    elif pname.find('rb5') >= 0:
        return 'Dragonboard RB5'
    else:
        return 'Other'

def generate_matplot_image(x_ticks=[], datasets=[], colors=[], labels=[], title=""):
    # https://stackoverflow.com/questions/3899980/how-to-change-the-font-size-on-a-matplotlib-plot
    fig = plt.figure()
    plt.rcParams.update({'font.size': 8})
    # plt_font = {'family':'serif','color':'darkred','size':8}
    for i, dataset in enumerate(datasets):
        plt.plot(x_ticks, dataset, color=colors[i], marker='o', label=labels[i])
    plt.title(title)
    plt.legend(loc=9)
    fig.autofmt_xdate(rotation=45)
    fig.tight_layout()
    # ax = plt.gca()
    # ax.tick_params(axis='x', labelrotation = 45)
    # plt.xticks(rotation = 270)

    # https://stackoverflow.com/questions/13953659/insert-image-into-reportlab-either-from-pil-image-or-stringio
    imgdata = BytesIO()
    plt.savefig(imgdata, format='png')
    imgdata.seek(0)  # rewind the data

    # https://stackoverflow.com/questions/21884271/warning-about-too-many-open-figures
    plt.cla()
    plt.close('all')

    trend_chart = Image(imgdata, width=18*cm, height=5*cm)
    return trend_chart


def generate_pdf_report(output, all_kerver_reports, kernelreport_kcs_dict, kernels_updated):
    def get_link_text(url=None, text=""):
        if not url or url == '--':
            return text
        else:
            return '<link href="%s" underline="true" textColor="navy">%s</link>' % (url, text)

    Story = []

    styles = getSampleStyleSheet()
    styleHeader = styles["Normal"].clone(ParagraphStyle)
    styleHeader.alignment = TA_CENTER
    styleHeader.textColor = 'white'
    styleHeader.backColor = 'black'
    styleHeader.fontSize = 8

    styleHeaderVertical = styleHeader.clone(ParagraphStyle)
    styleHeaderVertical.alignment = TA_LEFT


    styleContent = styles["Normal"].clone(ParagraphStyle)
    styleContent.fontSize = 8

    styleContentRightAlign = styleContent.clone(ParagraphStyle)
    styleContentRightAlign.alignment = TA_RIGHT

    stylelist = styles["OrderedList"].clone(ListStyle)
    stylelist.bulletFontSize = 8

    styleTitle = styles["Title"]

    styleHeading1 = styles["Heading1"]
    styleHeading1.backColor = 'darkgrey'

    styleHeading2 = styles["Heading2"]
    styleHeading2.backColor = 'darkgrey'

    styleBlockQuote = styles["Normal"].clone(ParagraphStyle)
    styleBlockQuote.leftIndent = 20
    styleBlockQuote.allowWidows = 1
    styleBlockQuote.backColor = '#FFFFE0'

    #######################################################################
    ##  Create the Title Page
    #######################################################################
    date_today = datetime.datetime.now()

    # STATICS_DIR = os.path.join(BASE_DIR, "static")
    # IMAGES_DIR = os.path.join(STATICS_DIR, "images")
    # LOGO_PATH = os.path.join(IMAGES_DIR, "Linaro-Logo_standard.png")

    # logo = Image(LOGO_PATH, width=9*cm, height=4.5*cm, hAlign='RIGHT')
    # Story.append(logo)
    # Story.append(Spacer(1, 24))

    Story.append(Paragraph('<b>LKFT Report for the latest 10 builds(%s)</b>' % (date_today.strftime('%y.%m.%d')), styleTitle))
    Story.append(Spacer(1, 240))

    lines = []
    lines.append(('Date', date_today.strftime('%Y.%m.%d')))
    lines.append(('Author', 'Yongqin Liu <yongqin.liu@linaro.org>'))

    table_style = TableStyle([
            ("BOX", (0, 0), (-1, -1), 0.5, "black"), # outter border
            ("INNERGRID", (0, 0), (-1, -1), 0.25, "black"), # inner grid
        ])

    table = Table(lines, colWidths=[60,300], style=table_style, hAlign='CENTER', vAlign='BOTTOM')
    Story.append(table)
    Story.append(PageBreak())

    #######################################################################
    ##  Create the Failed Jobs table
    #######################################################################
    for kerver, kernel_reports in all_kerver_reports.items():
        kerver_updated_kernels = []
        for kernel_report in kernel_reports:
            if kernel_report in kernels_updated:
                kerver_updated_kernels.append(kernel_report)
        if len(kerver_updated_kernels) == 0:
            continue

        Story.append(Paragraph(f'<b>{kerver}</b>', styleHeading1))
        Story.append(Spacer(1, 12))
        for kernel_report in kerver_updated_kernels:
            Story.append(Paragraph(f'<b>{kernel_report} summary</b>', styleHeading2))
            Story.append(Spacer(1, 12))
            kc_obj_list = kernelreport_kcs_dict.get(kernel_report)
            table_style = TableStyle([
                ("BOX", (0, 0), (-1, -1), 0.5, "black"),
                ("INNERGRID", (0, 0), (-1, -1), 0.25, "black"),

                ('BACKGROUND', (0, 0), (-1, 0), 'black'),  # background for the header
            ])
            table_header_line = (
                        Paragraph('<b>No.</b>', styleHeader),
                        Paragraph('<b>Kernel Branch</b>', styleHeader),
                        Paragraph('<b>Build Version</b>', styleHeader),
                        Paragraph('<b>Status</b>', styleHeader),
                        Paragraph('<b>Jobs Completed/Total</b>', styleHeader),
                        Paragraph('<b>Modules Done/Total</b>', styleHeader),
                        Paragraph('<b>Pass</b>', styleHeader),
                        Paragraph('<b>Fail</b>', styleHeader),
                        Paragraph('<b>Assumption</b>', styleHeader),
                        Paragraph('<b>Ignored</b>', styleHeader),
                        Paragraph('<b>Total</b>', styleHeader),
                    )
            lines = [table_header_line]

            trend_data = []
            project_lines = defaultdict(list)
            project_trend_datas = defaultdict(list)
            for index, kc_obj in enumerate(kc_obj_list):
                kc_status = kc_obj.kc_status
                kc_numbers = kc_obj.kc_numbers
                kc_jobs_failed = kc_obj.kc_jobs_failed
                kc_created_at = kc_obj.kc_created_at
                kc_last_fetched_timestamp = kc_obj.kc_last_fetched_timestamp
                kc_jobs_success_number = kc_obj.kc_jobs_success_number
                kc_jobs_total_number = kc_obj.kc_jobs_total_number

                lines.append((
                                Paragraph(f"{index + 1}", styleContent),
                                Paragraph(f"{kernel_report}", styleContent),
                                Paragraph(f"{kc_obj.build_version}", styleContent),
                                Paragraph(f"{kc_status}", styleContent),
                                Paragraph(f"{kc_jobs_success_number}/{kc_jobs_total_number}", styleContentRightAlign),
                                Paragraph(f"{kc_numbers.modules_done}/{kc_numbers.modules_total}", styleContentRightAlign),
                                Paragraph(f"{kc_numbers.number_passed}", styleContentRightAlign),
                                Paragraph(f"{kc_numbers.number_failed}", styleContentRightAlign),
                                Paragraph(f"{kc_numbers.number_assumption_failure}", styleContentRightAlign),
                                Paragraph(f"{kc_numbers.number_ignored}", styleContentRightAlign),
                                Paragraph(f"{kc_numbers.number_total}", styleContentRightAlign),
                            ))
                trend_data.append((kc_obj.build_version, kc_jobs_success_number, kc_jobs_total_number, kc_numbers.modules_done, kc_numbers.modules_total, kc_numbers.number_passed, kc_numbers.number_failed, kc_numbers.number_assumption_failure, kc_numbers.number_ignored, kc_numbers.number_total))

                if len(kc_obj.kc_builds) == 1:
                    # there is only one project, the data will be the same as the summary
                    continue

                for kc_build in kc_obj.kc_builds:
                    target_squad_project = kc_build.target_squad_project
                    project_alias = target_squad_project.project_alias
                    project_alias_name = project_alias['alias_name']
                    kc_build_number = kc_build.numbers

                    project_lines[project_alias_name].append((
                                Paragraph(f"{index + 1}", styleContent),
                                Paragraph(f"{kernel_report}", styleContent),
                                Paragraph(f"{kc_build.version}", styleContent),
                                Paragraph(f"{kc_build.build_status}", styleContent),
                                Paragraph(f"{kc_build.jobs_success_number}/{kc_build.jobs_total_number}", styleContentRightAlign),
                                Paragraph(f"{kc_build_number.modules_done}/{kc_build_number.modules_total}", styleContentRightAlign),
                                Paragraph(f"{kc_build_number.number_passed}", styleContentRightAlign),
                                Paragraph(f"{kc_build_number.number_failed}", styleContentRightAlign),
                                Paragraph(f"{kc_build_number.number_assumption_failure}", styleContentRightAlign),
                                Paragraph(f"{kc_build_number.number_ignored}", styleContentRightAlign),
                                Paragraph(f"{kc_build_number.number_total}", styleContentRightAlign),
                            ))
                    project_trend_datas[project_alias_name].append((kc_build.version, kc_build.jobs_success_number, kc_build.jobs_total_number,
                                                                    kc_build_number.modules_done, kc_build_number.modules_total,
                                                                    kc_build_number.number_passed, kc_build_number.number_failed, kc_build_number.number_assumption_failure, kc_build_number.number_ignored, kc_build_number.number_total))


            trend_data.reverse()
            kc_vers, pass_job_numbers, total_job_numbers, module_done_numbers, module_total_numbers, pass_numbers, failed_numbers, assumption_numbers, ignored_numbers, total_numbers = zip(*trend_data)

            trend_chart = generate_matplot_image(x_ticks=kc_vers,
                                                 datasets=[pass_job_numbers, total_job_numbers],
                                                 colors=['g', 'm'],
                                                 labels=["Job passed", "Job total"],
                                                 title="Jobs Status")
            Story.append(trend_chart)
            Story.append(Spacer(1, 24))

            trend_chart = generate_matplot_image(x_ticks=kc_vers,
                                                 datasets=[module_done_numbers, module_total_numbers],
                                                 colors=['g', 'm'],
                                                 labels=["Modules done", "Modules total"],
                                                 title="Modules Status")
            Story.append(trend_chart)
            Story.append(Spacer(1, 24))

            trend_chart = generate_matplot_image(x_ticks=kc_vers,
                                                 datasets=[pass_numbers, total_numbers],
                                                 colors=['g', 'm'],
                                                 labels=["Passed", 'Total'],
                                                 title="Passed/Total TestCases Status")
            Story.append(trend_chart)
            Story.append(Spacer(1, 24))

            trend_chart = generate_matplot_image(x_ticks=kc_vers,
                                                 datasets=[failed_numbers, assumption_numbers, ignored_numbers],
                                                 colors=['r', 'c', 'm'],
                                                 labels=["Failed", "Assumption", 'Ignored'],
                                                 title="Failed/Assumption/Ignored TestCases Status")
            Story.append(trend_chart)
            Story.append(Spacer(1, 24))
            Story.append(PageBreak())

            table = Table(lines, colWidths=[25, 70, 95, 90, 35, 55, 45, 30, 30, 30, 45], style=table_style, hAlign='LEFT')
            Story.append(table)
            Story.append(PageBreak())

            project_alias_names = get_all_report_kernels().get(kernel_report)
            if len(project_alias_names) <=1:
                continue
            for project_alias_name in project_alias_names:
                Story.append(Paragraph(f'<b>{kernel_report} {project_alias_name}</b>', styleHeading2))
                Story.append(Spacer(1, 12))

                project_trend_data = project_trend_datas[project_alias_name]
                project_trend_data.reverse()
                # if len(project_trend_data) == 0:
                #     logger.error(f"{project_alias_name} has no data")
                #     continue
                kc_vers, pass_job_numbers, total_job_numbers, module_done_numbers, module_total_numbers, pass_numbers, failed_numbers, assumption_numbers, ignored_numbers, total_numbers = zip(*project_trend_data)
                trend_chart = generate_matplot_image(x_ticks=kc_vers,
                                                 datasets=[pass_job_numbers, total_job_numbers],
                                                 colors=['g', 'm'],
                                                 labels=["Job passed", "Job total"],
                                                 title="Jobs Status")
                Story.append(trend_chart)
                Story.append(Spacer(1, 24))

                trend_chart = generate_matplot_image(x_ticks=kc_vers,
                                                     datasets=[module_done_numbers, module_total_numbers],
                                                     colors=['g', 'm'],
                                                     labels=["Modules done", "Modules total"],
                                                     title="Modules Status")
                Story.append(trend_chart)
                Story.append(Spacer(1, 24))

                trend_chart = generate_matplot_image(x_ticks=kc_vers,
                                                     datasets=[pass_numbers, total_numbers],
                                                     colors=['g', 'm'],
                                                     labels=["Passed", 'Total'],
                                                     title="Passed/Total TestCases Status")
                Story.append(trend_chart)
                Story.append(Spacer(1, 24))


                trend_chart = generate_matplot_image(x_ticks=kc_vers,
                                                     datasets=[failed_numbers, assumption_numbers, ignored_numbers],
                                                     colors=['r', 'c', 'm'],
                                                     labels=["Failed", "Assumption", 'Ignored'],
                                                     title="Failed/Assumption/Ignored TestCases Status")
                Story.append(trend_chart)
                Story.append(Spacer(1, 24))
                Story.append(PageBreak())

                table = Table([ table_header_line ] + project_lines[project_alias_name], colWidths=[25, 70, 95, 90, 35, 55, 45, 30, 30, 30, 45], style=table_style, hAlign='LEFT')
                Story.append(table)
                Story.append(PageBreak())


    # Create the PDF object, using the BytesIO object as its "file."
    class TestReportDocTemplate(SimpleDocTemplate):
        def afterFlowable(self, flowable):
            "Registers TOC entries."
            if flowable.__class__.__name__ == 'Paragraph':
                text = flowable.getPlainText()
                style = flowable.style.name
                if style == 'Heading1':
                    self.notify('TOCEntry', (0, text, self.page))
                    key = text.replace(' ', '-')
                    self.canv.bookmarkPage(key)
                    self.canv.addOutlineEntry(text, key, 0, 0)
                elif style == 'Heading2':
                    self.notify('TOCEntry', (0, text, self.page))
                    key = text.replace(' ', '-')
                    self.canv.bookmarkPage(key)
                    self.canv.addOutlineEntry(f"  {text}", key, 0, 0)

    def addPageNumber(canvas, doc):
        """
        Add the page number
        """
        page_num = canvas.getPageNumber()
        text = "Page #%s" % page_num
        canvas.drawRightString(200*mm, 20*mm, text)

    doc = TestReportDocTemplate(output, rightMargin=20, leftMargin=20, topMargin=20, bottomMargin=18)
    doc.build(Story, onFirstPage=addPageNumber, onLaterPages=addPageNumber)


def main():
    before_mem = mem_snapshot()
    start = time.time()

    parser = get_parser()
    subparser = parser.add_subparsers(help='available subcommands', dest='command')
    args = parser.parse_args()

    try:
        SquadApi.configure(url=args.url, token=args.token, cache=args.cache)
    except SquadApiException as sae:
        logger.error("Failed to configure the squad api: %s", sae)
        return -1

    squad = Squad()

    target_squad_projects = []
    all_report_kernels = get_all_report_kernels()
    all_supported_projects = get_all_report_projects()
    all_kerver_reports = get_all_kerver_reports()
    kernel_squad_projects_dict = defaultdict(list)
    for kernel, project_alias_names in all_report_kernels.items():
        alias_projects = []
        for project_alias_name in project_alias_names:
            alias_project = all_supported_projects.get(project_alias_name)
            if alias_project is None:
                print("The project is not configured correctly: %s" % project_alias_name)
                return
            alias_project['alias_name'] = project_alias_name
            alias_projects.append(alias_project)

        def get_squad_project(alias_project):
            ## https://gist.github.com/mrchapp/b902ae8f27adfb5b420d271d3e9869d7
            if alias_project.get('slug') and alias_project.get('group'):
                squad_group = squad.group(alias_project.get('group'))
                squad_project = squad_group.project(alias_project.get('slug'))
            elif alias_project.get('project_id'):
                project_id = alias_project.get('project_id')
                squad_project = squad.projects(count=1, id=project_id)[project_id]
            else:
                squad_project = None

            alias_project['squad_project'] = squad_project
        thread_pool(func=get_squad_project, elements=alias_projects, subgroup_count=args.thread_count)

        for alias_project in alias_projects:
            squad_project = alias_project.get('squad_project', None)
            if squad_project is not None:
                squad_project.project_alias = alias_project
                squad_project.kernel_report = kernel
                target_squad_projects.append(squad_project)
                # alias_name = alias_project.get("alias_name")
                # logger.info(f"{kernel}, {alias_name}")
                kernel_squad_projects_dict[kernel].append(squad_project)
            else:
                print("The project is not configured correctly: %s" % project_alias_name)
                continue

    def sort_by_job_name_url(job):
        return (job.name, job.external_url)

    all_jobs = []
    datetime_now = datetime.datetime.now(datetime.timezone.utc)
    duration_one_week = datetime.timedelta(days=7)
    datetime_since = datetime_now - duration_one_week
    kernel_builds_dict = defaultdict(OrderedDict)
    build_projects_dict = defaultdict(list)
    kc_builds_dict = defaultdict(list)
    project_platform_bugs = {}
    for target_squad_project in target_squad_projects:
        project_alias = target_squad_project.project_alias
        kernel_report = target_squad_project.kernel_report

        branch = project_alias.get('branch')
        builds = list(target_squad_project.builds(count=10, fields="id,version,finished,datetime,created_at", ordering="-datetime").values())

        valid_builds = []
        for build in builds:
            build.target_squad_project = target_squad_project
            kernel_builds_dict[kernel_report][build.version] = None

            kernel_build_key = "%s#%s" % (kernel_report, build.version)
            build_projects_dict[kernel_build_key].append(target_squad_project)
            kc_builds_dict[kernel_build_key].append(build)
            valid_builds.append(build)

            # print(f"%s {build.version} {build.created_at} less than one week" % project_alias.get("alias_name"))

        def get_build_jobs(build):
            jobs = build.testjobs().values()
            set_job_name_for_unsubmitted_jobs(jobs)

            logger.info("Downloading started for %d jobs of build %s" % (len(jobs), build.version))
            download_attachments_save_result(jobs=list(jobs))
            logger.info("Downloading ended for %d jobs of build %s" % (len(jobs), build.version))

        print("PERFORMANCE INFO: mem before get_build_jobs for project %s: %d MB" % (project_alias.get('alias_name'), mem_snapshot()))
        thread_pool(func=get_build_jobs, elements=valid_builds, subgroup_count=args.thread_count)
        print("PERFORMANCE INFO: mem after get_build_jobs for project %s: %d MB" % (project_alias.get('alias_name'), mem_snapshot()))

    class KernelChangeset():
        kernel_report = None
        build_version = None
        kc_created_at = None
        kc_last_fetched_timestamp = None
        kc_job_durations = []
        kc_status = "UNKNOWN"
        kc_numbers = TestNumbers()
        kc_builds = []
        kc_failures = []
        kc_modules = []

        kc_completed_build_number = 0
        kc_jobs_total_number = 0 # the total number of the jobs be checked for all projects
        kc_jobs_success_number = 0
        kc_jobs_failed = []
        kc_jobs_duration = datetime.timedelta(milliseconds=0)

    kernels_no_update = []
    kernels_updated = []
    projects_all = []
    projects_tested = []
    kernelreport_kcs_dict = OrderedDict()
    summary_jobs_duration = []
    summary_kc_lasted_times = []
    summary_numbers = TestNumbers()
    summary_jobs_total_number = 0
    summary_jobs_success_number = 0
    for kernel_report, project_alias_names in all_report_kernels.items():
        kcs = kernel_builds_dict[kernel_report].keys()
        projects_all.extend(project_alias_names)
        if kcs is None or len(kcs) == 0:
            kernels_no_update.append(kernel_report)
            continue
        else:
            kernels_updated.append(kernel_report)
            projects_tested.extend(project_alias_names)

        kc_obj_list = []
        for kc in kcs:
            kernel_build_key = "%s#%s" % (kernel_report, kc)
            kc_builds = kc_builds_dict.get(kernel_build_key)

            kc_numbers = TestNumbers()
            kc_has_cancelled = False
            kc_has_not_submitted = False
            kc_has_inprogress = False
            kc_completed_build_number = 0
            kc_jobs_total_number = 0 # the total number of to be checked jobs
            kc_jobs_success_number = 0
            kc_jobs_failed = []
            kc_last_fetched_timestamp = None
            kc_created_at = None
            kc_job_durations = []
            kc_failures = []
            kc_modules = []
            if kc_builds is None:
                # output.write(" " * 4 * 3 + f"No build tested for {build.version}, please check and try again\n")
                kc_builds = []
            for kc_build in kc_builds:
                target_squad_project = kc_build.target_squad_project
                project_alias = target_squad_project.project_alias
                project_alias_name = project_alias['alias_name']

                jobs = kc_build.testjobs().values()
                set_job_name_for_unsubmitted_jobs(jobs)
                jobs_to_be_checked = get_classified_jobs(jobs=jobs).get('final_jobs')

                # JOBSNOTSUBMITTED, JOBSINPROGRESS, CANCELED, JOBSCOMPLETED
                build_status = get_lkft_build_status(kc_build, jobs_to_be_checked)
                if build_status.get('has_unsubmitted'):
                    kc_has_not_submitted = True
                elif build_status.get('is_inprogress'):
                    kc_has_inprogress = True
                elif build_status.get("has_canceled"):
                    kc_has_cancelled = True
                else:
                    kc_completed_build_number = kc_completed_build_number + 1

                kc_build.last_fetched_timestamp = build_status.get('last_fetched_timestamp')
                if kc_last_fetched_timestamp is None or kc_last_fetched_timestamp < kc_build.last_fetched_timestamp:
                    kc_last_fetched_timestamp = kc_build.last_fetched_timestamp

                kc_build_created_at = get_aware_datetime_from_str(kc_build.created_at)
                if kc_created_at is None or kc_created_at > kc_build_created_at:
                    kc_created_at = kc_build_created_at

                build_jobs_finished_number = 0
                kc_build.numbers = TestNumbers()
                kc_build_job_durations = []
                kc_build.failures = []
                kc_build.modules = []
                for job in jobs_to_be_checked:
                    job.target_squad_project = target_squad_project
                    jobstatus = job.job_status
                    jobfailure = job.failure
                    # for some failed cases, numbers are not set for the job
                    job_numbers = getattr(job, 'numbers', TestNumbers())
                    if is_cts_vts_job(job.name) and jobstatus == 'Complete' and jobfailure is None and \
                            job_numbers is not None and job_numbers.modules_total !=0 :
                        build_jobs_finished_number = build_jobs_finished_number + 1
                    elif jobstatus == 'Complete' and jobfailure is None:
                        # for non-cts vts jobs, assuming it's a finished successful job
                        build_jobs_finished_number = build_jobs_finished_number + 1
                    else:
                        # failed jobs
                        kc_jobs_failed.append(job)

                    if jobstatus == "Complete" or jobstatus == "Incomplete":
                        job_start_time = get_aware_datetime_from_str(job.started_at, str_format_includes_ms=False)
                        job_end_time = get_aware_datetime_from_str(job.ended_at, str_format_includes_ms=False)
                        job_duration = job_end_time - job_start_time
                        kc_build_job_durations.append(job_duration)

                    kc_build.numbers.addWithTestNumbers(job_numbers)
                    job_failures = getattr(job, 'failures', [])
                    kc_build.failures.extend(job_failures)
                    job_modules = getattr(job, 'modules', [])
                    kc_build.modules.extend(job_modules)

                kc_build.jobs_duration = sum(kc_build_job_durations, datetime.timedelta())
                kc_failures.extend(kc_build.failures)
                kc_modules.extend(kc_build.modules)

                kc_numbers.addWithTestNumbers(kc_build.numbers)
                kc_jobs_total_number = kc_jobs_total_number + len(jobs_to_be_checked)
                kc_jobs_success_number = kc_jobs_success_number + build_jobs_finished_number
                kc_job_durations.extend(kc_build_job_durations)

                kc_build.jobs_total_number = len(jobs_to_be_checked)
                kc_build.jobs_success_number = build_jobs_finished_number


            # need to consider the builds status too
            kc_status = "UNKNOWN"
            if kc_has_not_submitted:
                kc_status = "JOBSNOTSUBMITTED"
            elif kc_has_inprogress:
                kc_status = 'JOBSINPROGRESS'
            elif kc_has_cancelled:
                kc_status = 'CANCELED'
            else:
                kc_status = 'COMPLETED'

            kc_obj = KernelChangeset()
            kc_obj.kernel_report = kernel_report
            kc_obj.build_version = kc
            kc_obj.kc_status = kc_status
            kc_obj.kc_job_durations = kc_job_durations
            kc_obj.kc_jobs_total_number = kc_jobs_total_number
            kc_obj.kc_jobs_success_number = kc_jobs_success_number
            kc_obj.kc_created_at = kc_created_at
            kc_obj.kc_last_fetched_timestamp = kc_last_fetched_timestamp
            kc_obj.kc_numbers = kc_numbers
            kc_obj.kc_jobs_failed = kc_jobs_failed
            kc_obj.kc_failures = kc_failures
            kc_obj.kc_modules = kc_modules
            kc_obj.kc_builds = kc_builds
            kc_obj_list.append(kc_obj)
            summary_jobs_duration.extend(kc_job_durations)
            summary_kc_lasted_times.append(kc_obj.kc_last_fetched_timestamp - kc_obj.kc_created_at)
            summary_numbers.addWithTestNumbers(kc_numbers)
            summary_jobs_total_number = summary_jobs_total_number + kc_jobs_total_number
            summary_jobs_success_number = summary_jobs_success_number + kc_jobs_success_number

        kernelreport_kcs_dict[kernel_report] = kc_obj_list

    generate_pdf_report(args.output.replace('.txt', ".pdf"), all_kerver_reports, kernelreport_kcs_dict, kernels_updated)

    with open(args.output, 'w') as output:
        tab_start_index = 0
        for kerver, kernel_reports in all_kerver_reports.items():
            kerver_updated_kernels = []
            for kernel_report in kernel_reports:
                if kernel_report in kernels_updated:
                    kerver_updated_kernels.append(kernel_report)
            if len(kerver_updated_kernels) > 0:
                output.write(tab_spaces * (tab_start_index) + f"{kerver}:\n")
                for kernel_report in kerver_updated_kernels:
                    kc_obj_list = kernelreport_kcs_dict.get(kernel_report)
                    kc_build_vers = []
                    project_lines = defaultdict(list)
                    headrs = [
                                "Index",
                                "Build Version",
                                "Status",
                                "jobs success/total",
                                "modules done/total",
                                "passed",
                                "failed",
                                "assumption failures",
                                "ignored",
                                "total",
                            ]
                    output.write(tab_spaces * (tab_start_index + 1) + f"summary\n")
                    output.write(tab_spaces * (tab_start_index + 2) + '\t'.join(headrs) + "\n")
                    for index, kc_obj in enumerate(kc_obj_list):
                        # if kc_obj.kc_status == "JOBSNOTSUBMITTED" or kc_obj.kc_status == "JOBSINPROGRESS":
                        #     kc_build_vers.append(f"{kc_obj.build_version}*")
                        # else:
                        #     kc_build_vers.append(f"{kc_obj.build_version}")
                        #         #         # need to consider the builds status too
                        kc_status = kc_obj.kc_status
                        kc_numbers = kc_obj.kc_numbers
                        kc_jobs_failed = kc_obj.kc_jobs_failed
                        kc_created_at = kc_obj.kc_created_at
                        kc_last_fetched_timestamp = kc_obj.kc_last_fetched_timestamp
                        kc_jobs_success_number = kc_obj.kc_jobs_success_number
                        kc_jobs_total_number = kc_obj.kc_jobs_total_number

                        output.write(tab_spaces * (tab_start_index + 2) + f"{index+1}\t{kc_obj.build_version}\t{kc_status}\t{kc_jobs_success_number}/{kc_jobs_total_number}" + \
                                                                                        f"\t{kc_numbers.modules_done}/{kc_numbers.modules_total}" + \
                                                                                        f"\t{kc_numbers.number_passed}\t{kc_numbers.number_failed}\t{kc_numbers.number_assumption_failure}" + \
                                                                                        f"\t{kc_numbers.number_ignored}\t{kc_numbers.number_total}\n")
                        if len(kc_obj.kc_builds) == 1:
                            # there is only one project, the data will be the same as the summary
                            continue

                        for index_build, kc_build in enumerate(kc_obj.kc_builds):
                            target_squad_project = kc_build.target_squad_project
                            project_alias = target_squad_project.project_alias
                            project_alias_name = project_alias['alias_name']
                            kc_build_number = kc_build.numbers

                            project_line = f"{kc_build.version}\t{kc_build.build_status}\t{kc_build.jobs_success_number}/{kc_build.jobs_total_number}" + \
                                                                                        f"\t{kc_build_number.modules_done}/{kc_build_number.modules_total}" + \
                                                                                        f"\t{kc_build_number.number_passed}\t{kc_build_number.number_failed}\t{kc_build_number.number_assumption_failure}" + \
                                                                                        f"\t{kc_build_number.number_ignored}\t{kc_build_number.number_total}\n"
                            project_lines[project_alias_name].append(project_line)

                    project_alias_names = get_all_report_kernels().get(kernel_report)
                    if len(project_alias_names) <= 1:
                        # there is only one project, the data will be the same as the summary
                        continue
                    for project_alias_name in project_alias_names:

                        output.write(tab_spaces * (tab_start_index + 1) + f"{project_alias_name}\n")
                        output.write(tab_spaces * (tab_start_index + 2) + '\t'.join(headrs) + "\n")
                        lines = project_lines[project_alias_name]
                        for index_line, line in enumerate(lines):
                            output.write(tab_spaces * (tab_start_index + 2) + f"{index_line}\t" + line)

    duration = time.time() - start
    print("PERFORMANCE INFO: it took %.3f seconds to finish the report" % (duration))
    print("PERFORMANCE INFO: mem before: %d MB, mem after: %d MB" % (before_mem, mem_snapshot()))


if __name__ == "__main__":
    sys.exit(main())
