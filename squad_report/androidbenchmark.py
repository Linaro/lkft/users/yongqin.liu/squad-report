#!/usr/bin/env python3

import argparse
import bugzilla
import datetime
import jinja2
import json
import math
import logging
import os
import re
import resource
import tarfile
import tempfile
import threading
import time
import sys
import uuid
import zipfile

from io import BytesIO, StringIO
from matplotlib import pyplot as plt

from reportlab.lib import colors
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, ListStyle
from reportlab.lib.units import cm, mm
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, Table, TableStyle, ListFlowable, PageBreak

import xml.etree.ElementTree as ET

import squad_report.logging

from collections import Counter, defaultdict, OrderedDict
from urllib.parse import urlparse

from squad_client.core.models import Squad, Build
from squad_report.androidreportconfig import get_all_boottime_report_projects, get_all_boottime_report_kernels
from squad_report.androidreportconfig import get_all_benchmark_report_projects, get_all_benchmark_report_kernels
from squad_report.androidreportconfig import get_all_benchmark_reports, get_all_boottime_reports
from squad_report.androidreport import download_attachments_save_result, get_classified_jobs, assign_testrun_for_testjobs

from squad_report.androidcommon import thread_pool, LinaroAndroidLKFTBug, get_hardware_from_pname
from squad_report.androidcommon import set_job_name_for_unsubmitted_jobs

from squad_client.core.api import SquadApi
from squad_client.core.api import ApiException as SquadApiException

# logging.basicConfig(format="[%(asctime)s.%(msecs)03d] %(levelname)s [%(name)s:%(lineno)s] %(message)s")
# logger = logging.getLogger(__name__)
# logger.setLevel("INFO")
logger = squad_report.logging.getLogger(__name__)

# Get a snapshot of the current mem consumption
mem_snapshot = lambda: resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024

tab_spaces = ' ' * 4


benchmarks_common = {  # job_name: {'test_suite':['test_case',]},
                "boottime": {
                              'boottime-fresh-install': ['KERNEL_BOOT_TIME_avg', 'ANDROID_BOOT_TIME_avg', 'TOTAL_BOOT_TIME_avg' ],
                              'boottime-reboot': ['KERNEL_BOOT_TIME_avg', 'ANDROID_BOOT_TIME_avg', 'TOTAL_BOOT_TIME_avg' ],
                            },
                'benchmarkpi': {'benchmarkpi': ['benchmarkpi',]},
                'caffeinemark': {'caffeinemark': ['Caffeinemark-Collect-score', 'Caffeinemark-Float-score', 'Caffeinemark-Loop-score',
                                      'Caffeinemark-Method-score', 'Caffeinemark-score', 'Caffeinemark-Sieve-score', 'Caffeinemark-String-score']},
                'cf-bench': {'cf-bench': ['cfbench-Overall-Score', 'cfbench-Java-Score', 'cfbench-Native-Score']},
                'gearses2eclair': {'gearses2eclair': ['gearses2eclair',]},
                'geekbench4': {'geekbench4': ['Geekbench4-Multi-Core', 'Geekbench4-Single-Core']},
                'javawhetstone': {'javawhetstone': ['javawhetstone-MWIPS', 'javawhetstone-N1-float', 'javawhetstone-N2-float', 'javawhetstone-N3-if', 'javawhetstone-N4-fixpt',
                                       'javawhetstone-N5-cos', 'javawhetstone-N6-float', 'javawhetstone-N7-equal', 'javawhetstone-N8-exp',]},
                'jbench': {'jbench': ['jbench',]},
                'linpack': {'linpack': ['Linpack-MFLOPSSingleScore', 'Linpack-MFLOPSMultiScore', 'Linpack-TimeSingleScore', 'Linpack-TimeMultiScore']},
                'quadrantpro': {'quadrantpro': ['quadrandpro-benchmark-memory', 'quadrandpro-benchmark', 'quadrandpro-benchmark-g2d', 'quadrandpro-benchmark-io',
                                     'quadrandpro-benchmark-cpu', 'quadrandpro-benchmark-g3d',]},
                'rl-sqlite': {'rl-sqlite': ['RL-sqlite-Overall',]},
                'scimark': {'scimark': ['scimark-FFT-1024', 'scimark-LU-100x100', 'scimark-SOR-100x100', 'scimark-Monte-Carlo', 'scimark-Composite-Score',]},
                'vellamo3': {'vellamo3': ['vellamo3-Browser-total', 'vellamo3-Metal-total', 'vellamo3-Multi-total', 'vellamo3-total-score',]},
             }
less_is_better_measurement = [
                              'KERNEL_BOOT_TIME_avg', 'ANDROID_BOOT_TIME_avg', 'TOTAL_BOOT_TIME_avg',
                              'benchmarkpi-mean',
                              'Linpack-TimeSingleScore-mean', 'Linpack-TimeMultiScore-mean', 'RL-sqlite-Overall-mean'
                             ]


def get_expected_benchmarks():
    return benchmarks_common

def get_benchmark_testjob_names():
    return benchmarks_common.keys()

def get_benchmark_testsuites(lava_job_name):
    b_name = get_benchmark_job_name(lava_job_name)
    if b_name is not None:
        return benchmarks_common.get(b_name)
    else:
        return None

def get_benchmark_job_name(lava_job_name):
    for b_job_name in get_benchmark_testjob_names():
        if lava_job_name.endswith(b_job_name):
            return b_job_name
    return None

def is_benchmark_job(lava_job_name):
    benchmark_job_name = get_benchmark_job_name(lava_job_name)
    return benchmark_job_name is not None


def generate_matplot_image(x_ticks=[], datasets=[], colors=[], labels=[], title=""):
    # https://stackoverflow.com/questions/3899980/how-to-change-the-font-size-on-a-matplotlib-plot
    fig = plt.figure()
    plt.rcParams.update({'font.size': 10})
    # plt_font = {'family':'serif','color':'darkred','size':8}
    for i, dataset in enumerate(datasets):
        if colors is None or len(colors) == 0:
            plt.plot(x_ticks, dataset, marker='o', label=labels[i])
        else:
            plt.plot(x_ticks, dataset, color=colors[i], marker='o', label=labels[i])
        plt.grid()
    if title is not None and len(title) > 0:
        plt.title(title)
    # plt.legend(loc="best")
    # add legends and set its box position
    # plt.legend(bbox_to_anchor=(0.0, 0.0))
    plt.legend(
                # loc='best',
                loc=(0.02, 1.2),
                # bbox_to_anchor=(1.1, 0.9),
                borderaxespad=0,
                fontsize=8,
                edgecolor='yellow')
    fig.autofmt_xdate(rotation=45)
    fig.tight_layout()
    # ax = plt.gca()
    # ax.tick_params(axis='x', labelrotation = 45)
    # plt.xticks(rotation = 270)

    # https://stackoverflow.com/questions/13953659/insert-image-into-reportlab-either-from-pil-image-or-stringio
    imgdata = BytesIO()
    plt.savefig(imgdata, format='png')
    imgdata.seek(0)  # rewind the data

    # https://stackoverflow.com/questions/21884271/warning-about-too-many-open-figures
    plt.cla()
    plt.close('all')

    # the size of A4 paper is 210mm×297mm
    trend_chart = Image(imgdata, width=20*cm, height=10*cm)
    return trend_chart


def generate_pdf_report(output, kernel_squad_projects_dict):
    def get_link_text(url=None, text=""):
        if not url or url == '--':
            return text
        else:
            return '<link href="%s" underline="true" textColor="navy">%s</link>' % (url, text)

    Story = []

    styles = getSampleStyleSheet()
    styleHeader = styles["Normal"].clone(ParagraphStyle)
    styleHeader.alignment = TA_CENTER
    styleHeader.textColor = 'white'
    styleHeader.backColor = 'black'
    styleHeader.fontSize = 8

    styleHeaderVertical = styleHeader.clone(ParagraphStyle)
    styleHeaderVertical.alignment = TA_LEFT


    styleContent = styles["Normal"].clone(ParagraphStyle)
    styleContent.fontSize = 8

    styleContentRightAlign = styleContent.clone(ParagraphStyle)
    styleContentRightAlign.alignment = TA_RIGHT

    stylelist = styles["OrderedList"].clone(ListStyle)
    stylelist.bulletFontSize = 8

    styleTitle = styles["Title"]

    styleHeading1 = styles["Heading1"]
    styleHeading1.backColor = 'darkgrey'

    styleHeading2 = styles["Heading2"]
    styleHeading2.backColor = 'darkgrey'

    styleBlockQuote = styles["Normal"].clone(ParagraphStyle)
    styleBlockQuote.leftIndent = 20
    styleBlockQuote.allowWidows = 1
    styleBlockQuote.backColor = '#FFFFE0'

    #######################################################################
    ##  Create the Title Page
    #######################################################################
    date_today = datetime.datetime.now()

    # STATICS_DIR = os.path.join(BASE_DIR, "static")
    # IMAGES_DIR = os.path.join(STATICS_DIR, "images")
    # LOGO_PATH = os.path.join(IMAGES_DIR, "Linaro-Logo_standard.png")

    # logo = Image(LOGO_PATH, width=9*cm, height=4.5*cm, hAlign='RIGHT')
    # Story.append(logo)
    # Story.append(Spacer(1, 24))

    Story.append(Paragraph('<b>LKFT Benchmarks Report for the latest 10 builds(%s)</b>' % (date_today.strftime('%y.%m.%d')), styleTitle))
    Story.append(Spacer(1, 240))

    lines = []
    lines.append(('Date', date_today.strftime('%Y.%m.%d')))
    lines.append(('Author', 'Yongqin Liu <yongqin.liu@linaro.org>'))

    table_style = TableStyle([
            ("BOX", (0, 0), (-1, -1), 0.5, "black"), # outter border
            ("INNERGRID", (0, 0), (-1, -1), 0.25, "black"), # inner grid
        ])

    table = Table(lines, colWidths=[60,300], style=table_style, hAlign='CENTER', vAlign='BOTTOM')
    Story.append(table)
    Story.append(PageBreak())

    #######################################################################
    ##  Create the Failed Jobs table
    #######################################################################
    for kerver, benchmark_projects in kernel_squad_projects_dict.items():
        for benchmark_project in benchmark_projects:
            squad_project = benchmark_project.get('squad_project')
            project_alias_name = benchmark_project.get("alias_name")
            Story.append(Paragraph(f'<b>{kerver} {project_alias_name}</b>', styleHeading1))
            Story.append(Spacer(1, 12))
            project_benchmarks = squad_project.benchmarks
            for benchmark_jobname, builds in project_benchmarks.items():
                Story.append(Paragraph(f'<b>{benchmark_jobname}</b>', styleHeading2))
                Story.append(Spacer(1, 10))

                expected_measurements = builds[0].measurements.keys()
                num_measurement = len(expected_measurements)

                table_header_line = [
                                        Paragraph('<b>No.</b>', styleHeader),
                                        Paragraph('<b>Build Version</b>', styleHeader),
                                    ]
                for measurement_name in expected_measurements:
                    table_header_line.append(Paragraph(f"<b>{measurement_name}</b>", styleHeader))
                lines = [table_header_line]

                trend_data = []
                for index, build in enumerate(builds):
                    build_measurements = build.measurements
                    row_data = [
                                    Paragraph(f"{index + 1}", styleContent),
                                    Paragraph(f"{build.version}", styleContent),
                                ]

                    for index_measurement, measurement in enumerate(build_measurements.values()):
                        if measurement is None:
                            row_data.append(
                                Paragraph(f"--", styleContentRightAlign),)
                        else:
                            row_data.append(
                                Paragraph(f"{measurement}", styleContentRightAlign),)

                    lines.append(row_data)

                    trend_data_one_line = [build.version]
                    trend_data_one_line.extend(build_measurements.values())
                    trend_data.append(trend_data_one_line)

                trend_data.reverse()
                zipped_data = list(zip(*trend_data))

                build_vers = zipped_data[0]
                datasets = zipped_data[1:]
                trend_chart = generate_matplot_image(x_ticks=build_vers,
                                                     datasets=datasets,
                                                     # colors=['r'],
                                                     labels=list(expected_measurements),
                                                     # title=f"{benchmark_jobname} Status",
                                                     )
                Story.append(trend_chart)
                Story.append(Spacer(1, 24))

                colWidths = [25, 100]
                if num_measurement >= 5:
                    col_witdh = int(440 / num_measurement)
                else:
                    col_witdh = 100
                colWidths.extend([col_witdh] * num_measurement)
                table = Table(lines, colWidths=colWidths, style=table_style, hAlign='LEFT')
                Story.append(table)
                Story.append(PageBreak())


    # Create the PDF object, using the BytesIO object as its "file."
    # https://www.reportlab.com/docs/reportlab-reference.pdf
    class TestReportDocTemplate(SimpleDocTemplate):
        def afterFlowable(self, flowable):
            "Registers TOC entries."
            if flowable.__class__.__name__ == 'Paragraph':
                text = flowable.getPlainText()
                style = flowable.style.name
                uuid1 = uuid.uuid1()
                key = text.replace(' ', '-')
                key_uuid = f"{uuid1}-{key}"
                self.canv.bookmarkPage(key_uuid)
                if style == 'Heading1':
                    self.notify('TOCEntry', (0, text, self.page, key_uuid))
                    self.canv.addOutlineEntry(f"{text}", key_uuid, level=0, closed=0)
                elif style == 'Heading2':
                    self.notify('TOCEntry', (1, text, self.page, key_uuid))
                    self.canv.addOutlineEntry(f"{text}", key_uuid, level=1, closed=0)

    def addPageNumber(canvas, doc):
        """
        Add the page number
        """
        page_num = canvas.getPageNumber()
        text = "Page #%s" % page_num
        canvas.drawRightString(200*mm, 20*mm, text)

    doc = TestReportDocTemplate(output, rightMargin=20, leftMargin=20, topMargin=20, bottomMargin=18)
    doc.build(Story, onFirstPage=addPageNumber, onLaterPages=addPageNumber)


class AndroidReport:
    args = None
    squad = None
    bugzilla_instance = None


    def get_parser(self):
        parser = argparse.ArgumentParser(
            prog="android-report-tools", description="tools for android report work"
        )

        parser.add_argument(
            "--cache",
            default=0,
            type=int,
            help="Cache squad-client requests with a timeout",
        )

        parser.add_argument(
            "--thread-count",
            ##default=os.cpu_count(),
            default=10,
            type=int,
            help="How many threads to be run in parallel",
        )


        parser.add_argument(
            "--token",
            default=os.environ.get("SQUAD_TOKEN", None),
            help="Authenticate to SQUAD using this token",
        )

        parser.add_argument(
            "--url",
            default=os.environ.get("SQUAD_URL", "https://qa-reports.linaro.org"),
            help="URL of the SQUAD service",
        )

        parser.add_argument(
            "--bugzilla-url",
            dest="bugzilla_url",
            default=os.environ.get("BUGZILLA_URL", "https://bugs.linaro.org"),
            help="URL of the bugzilla service",
        )

        parser.add_argument(
            "--bugzilla-token",
            dest="bugzilla_token",
            default=os.environ.get("BUGZILLA_TOKEN", ""),
            help="Authenticate to bugzilla using this token",
        )

        parser.add_argument(
            "--output",
            default="/tmp/android-benchmark.txt",
            help="Write the report to this file",
        )

        parser.add_argument(
            "--report-kernel",
            help="Specify which kernel branch to generate the report",
        )

        parser.add_argument(
            "--build",
            help="SQUAD build",
        )

        parser.add_argument(
            "--report-type",
            default=os.environ.get("REPORT_TYPE", "boottime"),
            help="Specify which kernel branch to generate the report",
        )
        return parser


    def main(self):
        parser = self.get_parser()
        subparser = parser.add_subparsers(help='available subcommands', dest='command')
        self.args = parser.parse_args()
        if not self.args.bugzilla_url.startswith('https://') \
                and not self.args.bugzilla_url.startswith('http://') \
                and self.args.bugzilla_url.find('/') > 0:
            print(f"The bugzilla url is not specified correctly: {self.args.bugzilla_url}")
            return 1

        try:
            SquadApi.configure(url=self.args.url, token=self.args.token, cache=self.args.cache)
        except SquadApiException as sae:
            logger.error("Failed to configure the squad api: %s", sae)
            return -1

        self.squad = Squad()

        self.bugzilla_instance = LinaroAndroidLKFTBug(host_name=self.args.bugzilla_url, api_key=self.args.bugzilla_token)


    def get_alias_project_with_alias_name(self, project_alias_name, all_supported_projects={}):
        alias_project = all_supported_projects.get(project_alias_name)
        if alias_project is not None:
            alias_project['alias_name'] = project_alias_name
        return alias_project

    def get_squad_project(self, alias_project):
        ## https://gist.github.com/mrchapp/b902ae8f27adfb5b420d271d3e9869d7
        if alias_project.get('slug') and alias_project.get('group'):
            squad_group = self.squad.group(alias_project.get('group'))
            squad_project = squad_group.project(alias_project.get('slug'))
        elif alias_project.get('project_id'):
            project_id = alias_project.get('project_id')
            squad_project = self.squad.projects(count=1, id=project_id)[project_id]
        else:
            squad_project = None

        alias_project['squad_project'] = squad_project
        return alias_project

    def fetch_all_squad_projects(self, all_report_kernels={}, all_branch_reports={}, all_projectids={}):
        target_squad_projects = []
        kernel_squad_projects_dict = defaultdict(list)
        for kernel, kernel_reports in all_report_kernels.items():
            for kernel_report in kernel_reports:
                alias_projects = []
                for project_alias_name in all_branch_reports.get(kernel_report, []):
                    alias_project = self.get_alias_project_with_alias_name(project_alias_name, all_supported_projects=all_projectids)
                    if alias_project is None:
                        print("The project is not configured correctly: %s, alias_project is not found" % project_alias_name)
                        return
                    alias_projects.append(alias_project)

                thread_pool(func=self.get_squad_project, elements=alias_projects, subgroup_count=self.args.thread_count)

                for alias_project in alias_projects:
                    squad_project = alias_project.get('squad_project', None)
                    if squad_project is not None:
                        squad_project.project_alias = alias_project
                        squad_project.kernel_report = kernel
                        target_squad_projects.append(squad_project)
                        # alias_name = alias_project.get("alias_name")
                        # logger.info(f"{kernel}, {alias_name}")
                        kernel_squad_projects_dict[kernel].append(alias_project)
                    else:
                        print("The project is not configured correctly: %s, squad project is not found" % project_alias_name)
                        continue
        return kernel_squad_projects_dict


    def fetch_all_squad_project_builds(self, squad_projects=[]):
        for target_squad_project in squad_projects:
            project_alias = target_squad_project.project_alias
            kernel_report = target_squad_project.kernel_report

            branch = project_alias.get('branch')
            builds = list(target_squad_project.builds(count=10, fields="id,version,finished,datetime,created_at", ordering="-datetime").values())

            valid_builds = []
            for build in builds:
                build.target_squad_project = target_squad_project
                valid_builds.append(build)

                # print(f"%s {build.version} {build.created_at} less than one week" % project_alias.get("alias_name"))

            def get_build_jobs(build):
                jobs = build.testjobs().values()
                set_job_name_for_unsubmitted_jobs(jobs)

                logger.info("Downloading started for %d jobs of build %s" % (len(jobs), build.version))
                download_attachments_save_result(jobs=list(jobs))
                logger.info("Downloading ended for %d jobs of build %s" % (len(jobs), build.version))

            print("PERFORMANCE INFO: mem before get_build_jobs for project %s: %d MB" % (project_alias.get('alias_name'), mem_snapshot()))
            thread_pool(func=get_build_jobs, elements=valid_builds, subgroup_count=args.thread_count)
            print("PERFORMANCE INFO: mem after get_build_jobs for project %s: %d MB" % (project_alias.get('alias_name'), mem_snapshot()))


def main():
    before_mem = mem_snapshot()
    start = time.time()

    benchmark_jobs = None
    testsuites = None
    testcases = None

    android_report = AndroidReport()
    android_report.main()
    if android_report.squad is None:
        print(f"Failed to initialize the SQUAD, please check and try again")
        return False

    if android_report.args.report_type == "boottime":
        benchmark_boottime_reports = get_all_boottime_reports()
        all_supported_projects = get_all_boottime_report_projects()
        all_kerver_reports = get_all_boottime_report_kernels()
    else:
        benchmark_boottime_reports = get_all_benchmark_reports()
        all_supported_projects = get_all_benchmark_report_projects()
        all_kerver_reports = get_all_benchmark_report_kernels()

    if android_report.args.report_kernel is not None:
        def find_kernelreport(report_kernel):
            for ker_ver, reportkernels in benchmark_boottime_reports.items():
                for reportkernel in reportkernels:
                    if report_kernel == reportkernel:
                        report_kernels = {
                                            ker_ver: [report_kernel],
                                            }
                        return report_kernels
            return None
        report_kernels = find_kernelreport(android_report.args.report_kernel)
        if report_kernels is None:
            print(f"{android_report.args.report_kernel} is not supported yet, please check and try again")
            return 1
    else:
        report_kernels = benchmark_boottime_reports

    kernel_squad_projects_dict = android_report.fetch_all_squad_projects(all_report_kernels=report_kernels, all_branch_reports=all_kerver_reports, all_projectids=all_supported_projects)
    if kernel_squad_projects_dict is None:
        logger.info("kernel_squad_projects_dict is None")
        return

    if benchmark_jobs is not None and len(benchmark_jobs) > 0:
        expected_benchmark_jobs = benchmark_jobs
    else:
        benchmark_tests = get_expected_benchmarks()
        expected_benchmark_jobs = sorted(benchmark_tests.keys())

    if android_report.args.report_type == "boottime":
        expected_benchmark_jobs = ['boottime']
    elif "boottime" in expected_benchmark_jobs:
        ## do not report for the boottime if it's for the benchmark projects
        expected_benchmark_jobs.remove('boottime')
    else:
        # the default jobs
        pass

    for kerver, benchmark_projects in kernel_squad_projects_dict.items():
        for benchmark_project in benchmark_projects:
            squad_project = benchmark_project.get('squad_project')
            project_alias_name = benchmark_project.get("alias_name")
            project_benchmarks = OrderedDict()
            squad_project.benchmarks = project_benchmarks
            for benchmark_jobname in expected_benchmark_jobs:
                onebenchmark_results = OrderedDict()

                logger.info(f"getting results for {project_alias_name} {benchmark_jobname}")
                test_suites = benchmark_tests.get(benchmark_jobname)
                if testsuites is not None and len(testsuites) > 0:
                    expected_testsuites = testsuites
                else:
                    expected_testsuites = sorted(test_suites.keys())

                expected_measurements = []
                for testsuite in expected_testsuites:
                    if testcases is not None and len(testcases) > 0:
                        expected_testcases = testcases
                    else:
                        expected_testcases = test_suites.get(testsuite)
                    for testcase in expected_testcases:
                        expected_measurements.append(f"{testsuite}#{testcase}")

                builds = list(squad_project.builds(count=10, fields="id,version,finished,datetime,created_at", ordering="-datetime").values())
                for index, build in enumerate(builds):
                    build_measurements = OrderedDict()
                    jobs = build.testjobs().values()
                    set_job_name_for_unsubmitted_jobs(jobs)
                    jobs_to_be_checked = get_classified_jobs(jobs=jobs).get('final_jobs')
                    assign_testrun_for_testjobs(jobs_to_be_checked)

                    for measurement_name in expected_measurements:
                        job_metrics = None
                        for job in jobs_to_be_checked:
                            if not job.name.endswith(benchmark_jobname):
                                continue
                            if not hasattr(job, 'metrics'):
                                continue
                            job_metrics = job.metrics.values()
                        if job_metrics is None:
                            build_measurements[measurement_name] = None
                            continue

                        measurment_result = None
                        for metric in job_metrics:
                            if metric.name == measurement_name.replace('#', '/'):
                                # metric.build = build
                                measurment_result = metric.result
                            else:
                                continue

                        build_measurements[measurement_name] = measurment_result
                    build.measurements = build_measurements

                project_benchmarks[benchmark_jobname] = builds

    generate_pdf_report(android_report.args.output.replace('.txt', ".pdf"), kernel_squad_projects_dict)

    with open(android_report.args.output, 'w') as output:
        if android_report.args.build and len(android_report.args.build) > 0:
            output.write('## * denotes the current report build\n\n')
        tab_start_index = 0
        for kerver, benchmark_projects in kernel_squad_projects_dict.items():
            for benchmark_project in benchmark_projects:
                squad_project = benchmark_project.get('squad_project')
                project_alias_name = benchmark_project.get("alias_name")
                output.write(tab_spaces * (tab_start_index) + f"{kerver}: {project_alias_name}\n")

                project_benchmarks = squad_project.benchmarks
                for benchmark_jobname, builds in project_benchmarks.items():
                    output.write(tab_spaces * (tab_start_index + 1 ) + f"{benchmark_jobname}:\n")

                    table_headers = ["Index", 'Build Number']
                    expected_measurements = builds[0].measurements.keys()
                    table_headers.extend(expected_measurements)

                    str_header = '\t'.join(table_headers)
                    output.write(tab_spaces * (tab_start_index + 1 ) + f"{str_header}\n")

                    for index, build in enumerate(builds):
                        build_measurements = build.measurements
                        if build.version == android_report.args.build:
                            row_data = [str(index + 1), build.version + "*"]
                        else:
                            row_data = [str(index + 1), build.version]
                        row_data.extend(build_measurements.values())

                        str_row_data = []
                        for data in row_data:
                            if data is None:
                                str_row_data.append('--')
                            else:
                                str_row_data.append(str(data))
                        str_row = '\t'.join(str_row_data)
                        output.write(tab_spaces * (tab_start_index + 1 ) + f"{str_row}\n")
                    output.write("\n")
            output.write("\n\n")

    duration = time.time() - start
    print("PERFORMANCE INFO: it took %.3f seconds to finish the report" % (duration))
    print("PERFORMANCE INFO: mem before: %d MB, mem after: %d MB" % (before_mem, mem_snapshot()))


if __name__ == "__main__":
    sys.exit(main())
