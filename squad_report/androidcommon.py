#!/usr/bin/env python3

import argparse
import bugzilla
import datetime
import math
import logging
import os
import pytz
import resource
import yaml
import threading

import squad_report.logging

from collections import defaultdict
from urllib.parse import urlparse

from squad_client.core.models import Squad, Build
from squad_client.core.api import SquadApi

from squad_client.core.api import SquadApi
from squad_client.core.api import ApiException as SquadApiException

# logging.basicConfig(format="[%(asctime)s.%(msecs)03d] %(levelname)s [%(name)s:%(lineno)s] %(message)s")
# logger = logging.getLogger(__name__)
# logger.setLevel("INFO")
logger = squad_report.logging.getLogger(__name__)

# Get a snapshot of the current mem consumption
mem_snapshot = lambda: resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024

tab_spaces = ' ' * 4

def thread_pool(func=None, elements=[], subgroup_count=10):
    number_of_elements = len(elements)
    number_of_subgroup = math.ceil(number_of_elements/subgroup_count)
    finished_count = 0
    for i in range(number_of_subgroup):
        subgroup_elements = elements[i*subgroup_count: (i+1)*subgroup_count]
        finished_count = finished_count + len(subgroup_elements)

        threads = list()
        for element in subgroup_elements:
            t = threading.Thread(target=func, args=(element,))
            threads.append(t)
            t.start()

        for t in threads:
            t.join()

    logger.info(f"Finished getting information for all elements: number_of_elements={number_of_elements}, number_of_subgroup={number_of_subgroup}, finished_count={finished_count}, subgroup_count={subgroup_count}")

def set_job_name_for_unsubmitted_jobs(jobs):
    for job in jobs:
        if not job.submitted:
            definition_url = job.definition
            response = SquadApi.get(definition_url)
            if response.content != "None":
                job.job_definition = yaml.safe_load(response.content)
                job.name = job.job_definition.get("job_name")


class LinaroAndroidLKFTBug(bugzilla.Bugzilla):

    def __init__(self, host_name, api_key):
        if host_name.startswith('https://') or host_name.startswith('http://'):
            host_name = urlparse(host_name).netloc
        self.host_name = host_name
        self.new_bug_url_prefix = "https://%s/enter_bug.cgi" % self.host_name
        self.rest_api_url = "https://%s/rest" % self.host_name
        self.show_bug_prefix = 'https://%s/show_bug.cgi?id=' % self.host_name

        self.product = 'Linaro Android'
        self.component = 'General'
        self.bug_severity = 'normal'
        self.op_sys = 'Android'
        self.keywords = "LKFT"

        if api_key is None:
            api_key = ""

        super(LinaroAndroidLKFTBug, self).__init__(self.rest_api_url, api_key)

        #self.build_version = None
        #self.hardware = None
        #self.version = None

    def get_lkft_bugs(self, summary_keyword=None, platform=None):
        bugs = []

        terms = [
                    {u'product': 'Linaro Android'},
                    {u'component': 'General'},
                    {u'op_sys': 'Android'},
                    {u'keywords': 'LKFT'}
                ]
        if platform is not None:
            terms.append({u'platform': platform})

        bugs_from_api = self.search_bugs(terms).bugs
        for bug in bugs_from_api:
            bug_dict = bugzilla.DotDict(bug)
            if summary_keyword is not None and \
                bug_dict.get('summary').find(summary_keyword) < 0:
                continue
            bugs.append(bug_dict)

        def get_bug_summary(item):
            return item.get('summary')

        sorted_bugs = sorted(bugs, key=get_bug_summary)
        return sorted_bugs

    def get_bugs_for_project(self, project_name="", cachepool={}):
        platform_name = get_hardware_from_pname(project_name)
        project_platform_key = "%s#%s" % (project_name, platform_name)
        bugs = cachepool.get(project_platform_key)
        if bugs is None:
            bugs = self.get_lkft_bugs(summary_keyword=project_name, platform=platform_name)
            cachepool[project_platform_key] = bugs

        return bugs


def get_hardware_from_pname(pname=None, env=''):
    if not pname:
        # for aosp master build
        if env.find('hi6220-hikey')>=0:
            return 'HiKey'
        else:
            return None
    if pname.find('hikey960') >= 0:
        return 'HiKey960'
    elif pname.find('hikey') >= 0:
        return 'HiKey'
    elif pname.find('x15') >= 0:
        return 'BeagleBoard-X15'
    elif pname.find('am65x') >= 0:
        return 'AM65X'
    elif pname.find('db845c') >= 0:
        return 'Dragonboard 845c'
    elif pname.find('rb5') >= 0:
        return 'RB5'
    else:
        return 'Other'


def get_aware_datetime_from_str(datetime_str, str_format_includes_ms=True):
    if datetime_str is None:
        return datetime_str
    # from python3.7, pytz is not necessary, we could use %z to get the timezone info
    #https://stackoverflow.com/questions/53291250/python-3-6-datetime-strptime-returns-error-while-python-3-7-works-well
    if type(datetime_str) is datetime.datetime:
        return datetime_str

    if datetime_str.split(":")[-1].find('.') > -1:
        str_format = '%Y-%m-%dT%H:%M:%S.%fZ'
    else:
        str_format = '%Y-%m-%dT%H:%M:%SZ'
    if type(datetime_str) is str:
        navie_datetime = datetime.datetime.strptime(datetime_str, str_format)
    else:
        navie_datetime = datetime.datetime.strptime(str(datetime_str), str_format)
    return pytz.utc.localize(navie_datetime)

class TestNumbers():
    number_passed = 0
    number_failed = 0
    number_assumption_failure = 0
    number_ignored = 0
    number_total = 0
    number_regressions = 0
    number_antiregressions = 0
    modules_done = 0
    modules_total = 0
    jobs_finished = 0
    jobs_total = 0


    def addWithHash(self, numbers_of_result):
        self.number_passed = self.number_passed + numbers_of_result.get('number_passed')
        self.number_failed = self.number_failed + numbers_of_result.get('number_failed')
        self.number_assumption_failure = self.number_assumption_failure + numbers_of_result.get('number_assumption_failure', 0)
        self.number_ignored = self.number_ignored + numbers_of_result.get('number_ignored', 0)
        self.number_total = self.number_total + numbers_of_result.get('number_total')
        self.number_regressions = self.number_regressions + numbers_of_result.get('number_regressions', 0)
        self.number_antiregressions = self.number_antiregressions + numbers_of_result.get('number_antiregressions', 0)
        self.modules_done = self.modules_done + numbers_of_result.get('modules_done')
        self.modules_total = self.modules_total + numbers_of_result.get('modules_total')
        self.jobs_finished = self.jobs_finished + numbers_of_result.get('jobs_finished', 0)
        self.jobs_total = self.jobs_total + numbers_of_result.get('jobs_total', 0)

        return self


    def toHash(self):
        return  {
            'number_passed': self.number_passed,
            'number_failed': self.number_failed,
            'number_assumption_failure': self.number_assumption_failure,
            'number_ignored': self.number_ignored,
            'number_total': self.number_total,
            'number_regressions': self.number_regressions,
            'number_antiregressions': self.number_antiregressions,
            'modules_done': self.modules_done,
            'modules_total': self.modules_total,
            'jobs_finished': self.jobs_finished,
            'jobs_total': self.jobs_total,
            }


    def addWithTestNumbers(self, testNumbers):
        self.number_passed = self.number_passed + testNumbers.number_passed
        self.number_failed = self.number_failed + testNumbers.number_failed
        self.number_assumption_failure = self.number_assumption_failure + testNumbers.number_assumption_failure
        self.number_ignored = self.number_ignored + testNumbers.number_ignored
        self.number_total = self.number_total + testNumbers.number_total
        self.number_regressions = self.number_regressions + testNumbers.number_regressions
        self.number_antiregressions = self.number_antiregressions + testNumbers.number_antiregressions
        self.modules_done = self.modules_done + testNumbers.modules_done
        self.modules_total = self.modules_total + testNumbers.modules_total
        self.jobs_finished = self.jobs_finished + testNumbers.jobs_finished
        self.jobs_total = self.jobs_total + testNumbers.jobs_total

        return self


    def addWithDatabaseRecord(self, db_record):
        self.number_passed = self.number_passed + db_record.number_passed
        self.number_failed = self.number_failed + db_record.number_failed
        self.number_assumption_failure = self.number_assumption_failure + db_record.number_assumption_failure
        self.number_ignored = self.number_ignored + db_record.number_ignored
        self.number_total = self.number_total + db_record.number_total
        self.modules_done = self.modules_done + db_record.modules_done
        self.modules_total = self.modules_total + db_record.modules_total
        if hasattr(db_record, 'jobs_finished'):
            self.jobs_finished = self.jobs_finished + db_record.jobs_finished
        if hasattr(db_record, 'jobs_total'):
            self.jobs_total = self.jobs_total + testNumbers.jobs_total

        if hasattr(db_record, 'number_regressions'):
            self.number_regressions = self.number_regressions + db_record.number_regressions
        if hasattr(db_record, 'number_antiregressions'):
            self.number_antiregressions = self.number_antiregressions + testNumbers.number_antiregressions

        return self


    def setValueForDatabaseRecord(self, db_record):
        db_record.number_passed = self.number_passed
        db_record.number_failed = self.number_failed
        db_record.number_assumption_failure = self.number_assumption_failure
        db_record.number_ignored = self.number_ignored
        db_record.number_total = self.number_total
        db_record.modules_done = self.modules_done
        db_record.modules_total = self.modules_total

        if hasattr(db_record, 'jobs_finished'):
            db_record.jobs_finished = self.jobs_finished
        if hasattr(db_record, 'jobs_total'):
            db_record.jobs_total = self.jobs_total

        if hasattr(db_record, 'number_regressions'):
            db_record.number_regressions = self.number_regressions
        if hasattr(db_record, 'number_antiregressions'):
            db_record.number_antiregressions = self.number_antiregressions
        return db_record


    def setHashValueForDatabaseRecord(db_record, numbers_of_result):
        db_record.number_passed = numbers_of_result.get('number_passed')
        db_record.number_failed = numbers_of_result.get('number_failed')
        db_record.number_assumption_failure = numbers_of_result.get('number_assumption_failure', 0)
        db_record.number_ignored = numbers_of_result.get('number_ignored', 0)
        db_record.number_total = numbers_of_result.get('number_total')
        db_record.modules_done = numbers_of_result.get('modules_done')
        db_record.modules_total = numbers_of_result.get('modules_total')

        if hasattr(db_record, 'jobs_finished'):
            db_record.jobs_finished = numbers_of_result.get('jobs_finished', 0)
        if hasattr(db_record, 'jobs_total'):
            db_record.jobs_total = numbers_of_result.get('jobs_total', 0)

        if hasattr(db_record, 'number_regressions'):
            db_record.number_regressions = numbers_of_result.get('number_regressions', 0)
        if hasattr(db_record, 'number_antiregressions'):
            db_record.number_antiregressions = numbers_of_result.get('number_antiregressions', 0)

        return db_record
