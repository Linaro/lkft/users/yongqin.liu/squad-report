#!/usr/bin/env python

kerverreports = {
    'Mainline': [
                    "android-mainline",
                    "android-mainline-16k",
                    "android-mainline-debug",
                    "android-mainline-sdcard",
                    "android-mainline-sm8550-sdcard",
                    "android-mainline-sm8550-sdcard-16k",
                ],
    'Linux 6.12': [
                    "android16-6.12",
                    "android16-6.12-full-cts-vts",
                    "android16-6.12-presubmit",
                ],
    'Linux 6.6': [
                    "android15-6.6",
                    "android15-6.6-debug",
                    "android15-6.6-full-cts-vts",
                    "android15-6.6-presubmit",
                ],
    'Linux 6.1': [
                    "android14-6.1",
                    "android14-6.1-full-cts-vts",
                    "android14-6.1-presubmit",
                    "android14-6.1-lts",
                ],
    'Linux 5.15': [
                    "android14-5.15-lts",
                    "android14-5.15",
                    "android13-5.15",
                    "android13-5.15-full-cts-vts",
                    "android13-5.15-presubmit",
                ],
    'Linux 5.10': [
                    "android13-5.10",
                    "android12-5.10-lts",
                    "android12-5.10",
                    "android12-5.10-presubmit",
                    "android12-5.10-full-cts-vts"
                ],
    'Linux 5.4': [
                    "android12-5.4-lts",
                    "android12-5.4",
                    "android12-5.4-presubmit",
                    "android11-5.4-lts",
                    "android11-5.4",
                    "android12-5.4-hikey",
                    "android11-5.4-hikey"
                ],
}

branch_categories_pair = {
    "android-mainline": [
                            "android-mainline",
                            "android-mainline-sanity",
                            "android-mainline-sanity-debug",
                            "android-mainline-sanity-16k",
                            "android-mainline-debug",
                            "android-mainline-16k",
                            "android-mainline-sdcard",
                        ],
    "android-mainline-weekly": [
                            "android-mainline-sm8550-sdcard-16k",
                        ],
    "android15-6.6": [
                        "android15-6.6",
                        "android15-6.6-debug",
                        "android15-6.6-boottime",
                        "android15-6.6-debug-boottime",
                        ],
    "android15-6.6-full-cts-vts": [
                        "android15-6.6-full-cts-vts",
                        ],
    "android15-6.6-presubmit": [
                        "android15-6.6-presubmit",
                        ],
    "android12-5.4": [
                        "android12-5.4",
                        "android12-5.4-hikey",
                        "android12-5.4-boottime",
                        ],
    "android11-5.4": [
                        "android11-5.4",
                        "android11-5.4-hikey",
                        "android11-5.4-boottime",
                        ],
}

rawkernels = {
    'android-mainline-sanity':[
            'mainline-aosp-master-sanity-rb5',
            'mainline-aosp-master-sanity-db845c',
            'mainline-aosp-master-sanity-db845c-sdcard',
            'mainline-aosp-master-sanity-sm8550-sdcard',
            ],

    'android-mainline':[
            'mainline-gki-aosp-master-rb5',
            'mainline-gki-aosp-master-db845c',
            ],

    'android-mainline-sdcard':[
            'mainline-gki-aosp-master-db845c-sdcard',
            ],

    ## There will be no jobs submitted to this SQUAD project
    'android-mainline-sm8550-sdcard':[
            'mainline-gki-aosp-master-sm8550-sdcard',
            ],

    'android-mainline-sanity-debug':[
            'mainline-debug-aosp-master-sanity',
            ],
    'android-mainline-debug':[
            'mainline-gki-debug-aosp-master-db845c',
            ],

    'android-mainline-sanity-16k':[
            'mainline-16k-aosp-master-sanity-db845c',
            'mainline-16k-aosp-master-sanity-rb5',
            ],
    'android-mainline-16k':[
            'mainline-gki-16k-aosp-master-db845c',
            'mainline-gki-16k-aosp-master-rb5',
            ],
    ## This report will be run weekly
    'android-mainline-sm8550-sdcard-16k':[
            'mainline-gki-16k-aosp-master-sm8550-sdcard',
            ],

    'android16-6.12':[
            '6.12-gki-android16-aosp-master-rb5',
            '6.12-gki-android16-aosp-master-db845c',
            ],

    'android15-6.6':[
            '6.6-gki-android15-aosp-master-rb5',
            '6.6-gki-android15-aosp-master-db845c',
            '6.6-gki-android15-android15-rb5',
            '6.6-gki-android15-android15-db845c',
            ],

    'android15-6.6-debug':[
            '6.6-gki-android15-debug-aosp-master-db845c',
            ],

    'android14-6.1-lts':[
            '6.1-lts-gki-android14-aosp-master-rb5',
            '6.1-lts-gki-android14-aosp-master-db845c',
            ],

    'android14-6.1':[
            '6.1-gki-android14-aosp-master-rb5',
            '6.1-gki-android14-aosp-master-db845c',
            '6.1-gki-android14-android15-rb5',
            '6.1-gki-android14-android15-db845c',
            '6.1-gki-android14-android14-rb5',
            '6.1-gki-android14-android14-db845c',
            ],

    'android14-5.15-lts':[
            '5.15-lts-gki-android14-aosp-master-rb5',
            '5.15-lts-gki-android14-aosp-master-db845c',
            ],

    'android14-5.15':[
            '5.15-gki-android14-aosp-master-rb5',
            '5.15-gki-android14-aosp-master-db845c',
            '5.15-gki-android14-android15-rb5',
            '5.15-gki-android14-android15-db845c',
            '5.15-gki-android14-android14-rb5',
            '5.15-gki-android14-android14-db845c',
            ],

    'android13-5.15':[
            '5.15-gki-android13-aosp-master-rb5',
            '5.15-gki-android13-aosp-master-db845c',
            '5.15-gki-android13-android15-rb5',
            '5.15-gki-android13-android15-db845c',
            '5.15-gki-android13-android14-rb5',
            '5.15-gki-android13-android14-db845c',
            '5.15-gki-android13-android13-rb5',
            '5.15-gki-android13-android13-db845c',
            ],

    'android13-5.10':[
            '5.10-gki-android13-aosp-master-db845c',
            '5.10-gki-android13-android15-db845c',
            '5.10-gki-android13-android14-db845c',
            '5.10-gki-android13-android13-db845c',
            ],

    'android12-5.10-lts':[
            '5.10-lts-gki-android12-android12-db845c',
            '5.10-lts-gki-android12-android12-hikey960',
            ],

    'android12-5.10':[
            '5.10-gki-aosp-master-db845c',
            '5.10-gki-aosp-master-hikey960',
            '5.10-gki-android12-android13-db845c',
            '5.10-gki-android12-android12-db845c',
            '5.10-gki-android12-android12-hikey960',
            ],

    'android12-5.4-lts':[
            '5.4-lts-gki-android12-android12-db845c', # android12-5.4-lts
            '5.4-lts-gki-android12-android12-hikey960', # android12-5.4-lts
            ],

    'android12-5.4':[
            '5.4-gki-aosp-master-db845c',  # android12-5.4
            '5.4-gki-aosp-master-hikey960', # android12-5.4
            '5.4-gki-android12-android13-db845c',
            '5.4-gki-android12-android12-db845c', # android12-5.4
            '5.4-gki-android12-android12-hikey960', # android12-5.4
            ],

    'android12-5.4-hikey':[
            '5.4-gki-android12-android12-hikey', # android12-5.4
            ],


    'android11-5.4-lts':[
            '5.4-lts-gki-android11-android11-db845c', # android11-5.4-lts
            '5.4-lts-gki-android11-android11-hikey960', # android11-5.4-lts
            ],

    'android11-5.4':[
            '5.4-gki-android11-android11-db845c', # android11-5.4
            '5.4-gki-android11-android11-hikey960', # android11-5.4
            '5.4-gki-android11-android11-android12gsi-db845c', # android11-5.4
            '5.4-gki-android11-android11-android12gsi-hikey960', # android11-5.4
            '5.4-gki-android11-android12-db845c', # android11-5.4
            '5.4-gki-android11-android12-hikey960', # android11-5.4
            '5.4-gki-android11-android13-db845c',
            '5.4-gki-android11-aosp-master-db845c', # android11-5.4
            '5.4-gki-android11-aosp-master-hikey960', # android11-5.4
            ],

    'android11-5.4-hikey':[
            '5.4-gki-android11-android11-hikey', # android12-5.4
            ],

    'android-hikey-linaro-4.19-stable-lkft': [
            '4.19-stable-android13-hikey960-lkft',
            '4.19-stable-android11-hikey960-lkft',
            '4.19-stable-android12-hikey-lkft',
            '4.19-stable-android11-hikey-lkft',
            '4.19-stable-android11-android12gsi-hikey-lkft',
            '4.19-stable-android11-android13gsi-hikey-lkft',
            '4.19-stable-android11-android12gsi-hikey960-lkft',
            '4.19-stable-android11-android13gsi-hikey960-lkft',
            '4.19-stable-android12-android13gsi-hikey960-lkft',
            '4.19-stable-android12-android13gsi-hikey-lkft',
            ],

    # For presubmit jobs
    'android16-6.12-presubmit': [
            '6.12-gki-android16-aosp-master-db845c-presubmit',
            ],
    'android15-6.6-presubmit': [
            '6.6-gki-android15-android15-db845c-presubmit',
            ],
    'android14-6.1-presubmit': [
            '6.1-gki-android14-android14-db845c-presubmit',
            ],
    'android13-5.15-presubmit': [
            '5.15-gki-android13-android13-db845c-presubmit',
            ],
    'android12-5.10-presubmit': [
            '5.10-gki-android12-android12-db845c-presubmit',
            ],
    'android12-5.4-presubmit': [
            '5.4-gki-android12-android12-db845c-presubmit',
            ],

    # For full cts vts jobs
    'android16-6.12-full-cts-vts':[
            '6.12-gki-android16-aosp-master-db845c-full-cts-vts',
            ],
    'android15-6.6-full-cts-vts':[
            '6.6-gki-android15-android15-db845c-full-cts-vts',
            ],
    'android14-6.1-full-cts-vts':[
            '6.1-gki-android14-android14-db845c-full-cts-vts',
            ],
    'android13-5.15-full-cts-vts':[
            '5.15-gki-android13-android13-db845c-full-cts-vts',
            ],
    'android12-5.10-full-cts-vts':[
            '5.10-gki-android12-android12-db845c-full-cts-vts',
            ],
}

projectids = {
    '4.19-stable-android13-hikey960-lkft':
                    {'slug': '4.19-stable-android13-hikey960-lkft',
                     'group':'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'Android13',
                     'kern' : '4.19',
                     'branch': 'Android-4.19-stable',},
    '4.19-stable-android11-hikey960-lkft':
                    {'slug': '4.19-stable-android11-hikey960-lkft',
                     'group':'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'Android11-GSI',
                     'kern' : '4.19',
                     'branch': 'Android-4.19-stable',},
    '4.19-stable-android12-hikey-lkft':
                    {'slug': '4.19-stable-android12-hikey-lkft',
                     'group':'android-lkft',
                     'hardware': 'HiKey',
                     'OS' : 'Android12',
                     'kern' : '4.19',
                     'branch': 'Android-4.19-stable',},
    '4.19-stable-android11-hikey-lkft':
                    {'slug': '4.19-stable-android11-hikey-lkft',
                     'group':'android-lkft',
                     'hardware': 'HiKey',
                     'OS' : 'Android11-GSI',
                     'kern' : '4.19',
                     'branch': 'Android-4.19-stable',},
    '4.19-stable-android11-android12gsi-hikey960-lkft':
                    {'slug': '4.19-stable-android11-android12gsi-hikey960-lkft',
                     'group':'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'Android12-GSI',
                     'kern' : '4.19',
                     'branch': 'Android-4.19-stable',},
    '4.19-stable-android11-android13gsi-hikey960-lkft':
                    {'slug': '4.19-stable-android11-android13gsi-hikey960-lkft',
                     'group':'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'Android13-GSI',
                     'kern' : '4.19',
                     'branch': 'Android-4.19-stable',},
    '4.19-stable-android11-android12gsi-hikey-lkft':
                    {'slug': '4.19-stable-android11-android12gsi-hikey-lkft',
                     'group':'android-lkft',
                     'hardware': 'HiKey',
                     'OS' : 'Android12-GSI',
                     'kern' : '4.19',
                     'branch': 'Android-4.19-stable',},
    '4.19-stable-android11-android13gsi-hikey-lkft':
                    {'slug': '4.19-stable-android11-android13gsi-hikey-lkft',
                     'group':'android-lkft',
                     'hardware': 'HiKey',
                     'OS' : 'Android13-GSI',
                     'kern' : '4.19',
                     'branch': 'Android-4.19-stable',},
    '4.19-stable-android12-android13gsi-hikey960-lkft':
                    {'slug': '4.19-stable-android12-android13gsi-hikey960-lkft',
                     'group':'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'Android13-GSI',
                     'kern' : '4.19',
                     'branch': 'Android-4.19-stable',},
    '4.19-stable-android12-android13gsi-hikey-lkft':
                    {'slug': '4.19-stable-android12-android13gsi-hikey-lkft',
                     'group':'android-lkft',
                     'hardware': 'HiKey',
                     'OS' : 'Android13-GSI',
                     'kern' : '4.19',
                     'branch': 'Android-4.19-stable',},

    # projects for android12-5.4
    '5.4-gki-aosp-master-hikey960':
                    {'slug': '5.4-gki-aosp-master-hikey960',
                     'group': 'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'AOSP',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},
    '5.4-gki-aosp-master-db845c':
                    {'slug': '5.4-gki-aosp-master-db845c',
                     'group': 'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},
    '5.4-gki-android12-android13-db845c':
                    {'slug': '5.4-gki-android12-android13-db845c',
                     'group': 'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},
    '5.4-gki-android12-android12-db845c':
                    {'slug': '5.4-gki-android12-android12-db845c',
                     'group': 'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},
    '5.4-gki-android12-android12-db845c-presubmit':
                    {'slug': '5.4-gki-android12-android12-db845c-presubmit',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},
    '5.4-gki-android12-android12-hikey960':
                    {'slug': '5.4-gki-android12-android12-hikey960',
                     'group':'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},
    '5.4-gki-android12-android12-hikey':
                    {'slug': '5.4-gki-android12-android12-hikey',
                     'group':'android-lkft',
                     'hardware': 'HiKey',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},

    # projects for android11-5.4-lts
    '5.4-lts-gki-android11-android11-db845c':
                    {'slug': '5.4-lts-gki-android11-android11-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android11-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4-lts',},
    '5.4-lts-gki-android11-android11-hikey960':
                    {'slug': '5.4-lts-gki-android11-android11-hikey960',
                     'group':'android-lkft',
                     'hardware': 'hikey960',
                     'OS' : 'Android11-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4-lts',},
    # projects for android11-5.4
    '5.4-gki-android11-android11-db845c':
                    {'slug': '5.4-gki-android11-android11-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android11-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android11-hikey960':
                    {'slug': '5.4-gki-android11-android11-hikey960',
                     'group':'android-lkft',
                     'hardware': 'hikey960',
                     'OS' : 'Android11-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android11-hikey':
                    {'slug': '5.4-gki-android11-android11-hikey',
                     'group':'android-lkft',
                     'hardware': 'HiKey',
                     'OS' : 'Android11',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android11-android12gsi-db845c':
                    {'slug': '5.4-gki-android11-android11-android12gsi-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android12-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android11-android12gsi-hikey960':
                    {'slug': '5.4-gki-android11-android11-android12gsi-hikey960',
                     'group':'android-lkft',
                     'hardware': 'hikey960',
                     'OS' : 'Android12-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android12-hikey960':
                    {'slug': '5.4-gki-android11-android12-hikey960',
                     'group': 'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android12-db845c':
                    {'slug': '5.4-gki-android11-android12-db845c',
                     'group': 'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android13-db845c':
                    {'slug': '5.4-gki-android11-android13-db845c',
                     'group': 'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-aosp-master-db845c':
                    {'slug': '5.4-gki-android11-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-aosp-master-hikey960':
                    {'slug': '5.4-gki-android11-aosp-master-hikey960',
                     'group':'android-lkft',
                     'hardware': 'hikey960',
                     'OS' : 'AOSP',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},

    # projects for android12-5.10
    '5.10-gki-aosp-master-hikey960':
                    {'slug': '5.10-gki-aosp-master-hikey960',
                     'group':'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'AOSP',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},
    '5.10-gki-aosp-master-db845c':
                    {'slug': '5.10-gki-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},
    '5.10-gki-android12-android13-db845c':
                    {'slug': '5.10-gki-android12-android13-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},
    '5.10-gki-android12-android12-db845c':
                    {'slug': '5.10-gki-android12-android12-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},
    '5.10-gki-android12-android12-db845c-presubmit':
                    {'slug': '5.10-gki-android12-android12-db845c-presubmit',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},
    '5.10-gki-android12-android12-hikey960':
                    {'slug': '5.10-gki-android12-android12-hikey960',
                     'group':'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'Android12',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},
    '5.10-gki-android12-android12-db845c-full-cts-vts':
                    {'slug': '5.10-gki-android12-android12-db845c-full-cts-vts',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},

    # projects for android13-5.10
    '5.10-gki-android13-aosp-master-db845c':
                    {'slug': '5.10-gki-android13-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.10',
                     'branch' : 'Android13-5.10',},
    '5.10-gki-android13-android15-db845c':
                    {'slug': '5.10-gki-android13-android15-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '5.10',
                     'branch' : 'Android13-5.10',},
    '5.10-gki-android13-android14-db845c':
                    {'slug': '5.10-gki-android13-android14-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android14',
                     'kern' : '5.10',
                     'branch' : 'Android13-5.10',},
    '5.10-gki-android13-android13-db845c':
                    {'slug': '5.10-gki-android13-android13-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.10',
                     'branch' : 'Android13-5.10',},

    # projects for android12-5.10-lts
    '5.10-lts-gki-android12-android12-hikey960':
                    {'slug': '5.10-lts-gki-android12-android12-hikey960',
                     'group': 'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'Android12',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10-lts',},
    '5.10-lts-gki-android12-android12-db845c':
                    {'slug': '5.10-lts-gki-android12-android12-db845c',
                     'group': 'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10-lts',},


    # projects for android12-5.4-lts
    '5.4-lts-gki-android12-android12-hikey960':
                    {'slug': '5.4-lts-gki-android12-android12-hikey960',
                     'group': 'android-lkft',
                     'hardware': 'HiKey960',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4-lts',},
    '5.4-lts-gki-android12-android12-db845c':
                    {'slug': '5.4-lts-gki-android12-android12-db845c',
                     'group': 'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4-lts',},

    # projects for android13-5.15
    '5.15-gki-android13-aosp-master-db845c':
                    {'slug': '5.15-gki-android13-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-aosp-master-rb5':
                    {'slug': '5.15-gki-android13-aosp-master-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android15-db845c':
                    {'slug': '5.15-gki-android13-android15-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android15-rb5':
                    {'slug': '5.15-gki-android13-android15-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'Android15',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android14-db845c':
                    {'slug': '5.15-gki-android13-android14-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android14',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android14-rb5':
                    {'slug': '5.15-gki-android13-android14-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'Android14',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android13-db845c':
                    {'slug': '5.15-gki-android13-android13-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android13-rb5':
                    {'slug': '5.15-gki-android13-android13-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'Android13',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android13-db845c-presubmit':
                    {'slug': '5.15-gki-android13-android13-db845c-presubmit',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android13-db845c-full-cts-vts':
                    {'slug': '5.15-gki-android13-android13-db845c-full-cts-vts',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},

    # projects for android14-5.15
    '5.15-gki-android14-aosp-master-db845c':
                    {'slug': '5.15-gki-android14-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},
    '5.15-gki-android14-aosp-master-rb5':
                    {'slug': '5.15-gki-android14-aosp-master-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},
    '5.15-gki-android14-android15-db845c':
                    {'slug': '5.15-gki-android14-android15-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},
    '5.15-gki-android14-android15-rb5':
                    {'slug': '5.15-gki-android14-android15-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'Android15',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},
    '5.15-gki-android14-android14-db845c':
                    {'slug': '5.15-gki-android14-android14-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android14',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},
    '5.15-gki-android14-android14-rb5':
                    {'slug': '5.15-gki-android14-android14-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'Android14',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},

    # projects for android14-5.15-lts
    '5.15-lts-gki-android14-aosp-master-db845c':
                    {'slug': '5.15-lts-gki-android14-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15-lts',},
    '5.15-lts-gki-android14-aosp-master-rb5':
                    {'slug': '5.15-lts-gki-android14-aosp-master-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15-lts',},

    # projects for android14-6.1
    '6.1-gki-android14-aosp-master-db845c':
                    {'slug': '6.1-gki-android14-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-aosp-master-rb5':
                    {'slug': '6.1-gki-android14-aosp-master-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-android15-db845c':
                    {'slug': '6.1-gki-android14-android15-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-android15-rb5':
                    {'slug': '6.1-gki-android14-android15-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'Android15',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-android14-db845c':
                    {'slug': '6.1-gki-android14-android14-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android14',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-android14-rb5':
                    {'slug': '6.1-gki-android14-android14-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'Android14',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-android14-db845c-presubmit':
                    {'slug': '6.1-gki-android14-android14-db845c-presubmit',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android14',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-android14-db845c-full-cts-vts':
                    {'slug': '6.1-gki-android14-android14-db845c-full-cts-vts',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android14',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},

    # projects for android14-6.1-lts
    '6.1-lts-gki-android14-aosp-master-db845c':
                    {'slug': '6.1-lts-gki-android14-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1-lts',},
    '6.1-lts-gki-android14-aosp-master-rb5':
                    {'slug': '6.1-lts-gki-android14-aosp-master-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1-lts',},

    # projects for android15-6.6
    '6.6-gki-android15-aosp-master-db845c':
                    {'slug': '6.6-gki-android15-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6',},
    '6.6-gki-android15-aosp-master-rb5':
                    {'slug': '6.6-gki-android15-aosp-master-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6',},
    '6.6-gki-android15-android15-db845c':
                    {'slug': '6.6-gki-android15-android15-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6',},
    '6.6-gki-android15-android15-rb5':
                    {'slug': '6.6-gki-android15-android15-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'Android15',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6',},

    '6.6-gki-android15-debug-aosp-master-db845c':
                    {'slug': '6.6-gki-android15-debug-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6-debug',},
    '6.6-gki-android15-android15-db845c-presubmit':
                    {'slug': '6.6-gki-android15-android15-db845c-presubmit',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6',},
    '6.6-gki-android15-android15-db845c-full-cts-vts':
                    {'slug': '6.6-gki-android15-android15-db845c-full-cts-vts',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6',},
    '6.12-gki-android16-aosp-master-db845c-presubmit':
                    {'slug': '6.12-gki-android16-aosp-master-db845c-presubmit',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.12',
                     'branch' : 'Android16-6.12',},
    '6.12-gki-android16-aosp-master-db845c-full-cts-vts':
                    {'slug': '6.12-gki-android16-aosp-master-db845c-full-cts-vts',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.12',
                     'branch' : 'Android16-6.12',},

    # projects for android16-6.12
    '6.12-gki-android16-aosp-master-db845c':
                    {'slug': '6.12-gki-android16-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.12',
                     'branch' : 'Android16-6.12',},
    '6.12-gki-android16-aosp-master-rb5':
                    {'slug': '6.12-gki-android16-aosp-master-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '6.12',
                     'branch' : 'Android16-6.12',},
    # projects for android-mainline
    'mainline-gki-aosp-master-db845c':
                    {'slug': 'mainline-gki-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline',},
    'mainline-gki-aosp-master-rb5':
                    {'slug': 'mainline-gki-aosp-master-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline',},
    'mainline-gki-debug-aosp-master-db845c':
                    {'slug': 'mainline-gki-debug-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline-debug',},
    'mainline-gki-aosp-master-db845c-sdcard':
                    {'slug': 'mainline-gki-aosp-master-db845c-sdcard',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline',},
    'mainline-gki-aosp-master-sm8550-sdcard':
                    {'slug': 'mainline-gki-aosp-master-sm8550-sdcard',
                     'group':'android-lkft',
                     'hardware': 'sm8550-hdk',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline',},
    'mainline-gki-16k-aosp-master-db845c':
                    {'slug': 'mainline-gki-16k-aosp-master-db845c',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline-16k',},
    'mainline-gki-16k-aosp-master-rb5':
                    {'slug': 'mainline-gki-16k-aosp-master-rb5',
                     'group':'android-lkft',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline-16k',},
    'mainline-gki-16k-aosp-master-sm8550-sdcard':
                    {'slug': 'mainline-gki-16k-aosp-master-sm8550-sdcard',
                     'group':'android-lkft',
                     'hardware': 's8550-hdk',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline-16k',},

    "mainline-aosp-master-sanity-rb5":
                    {'slug': 'mainline-aosp-master-sanity',
                     'group':'android-lkft',
                     'environment':  'qrb5165-rb5',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline',},
    "mainline-aosp-master-sanity-db845c":
                    {'slug': 'mainline-aosp-master-sanity',
                     'group':'android-lkft',
                     'environment':  'dragonboard-845c',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline',},
    "mainline-aosp-master-sanity-db845c-sdcard":
                    {'slug': 'mainline-aosp-master-sanity',
                     'group':'android-lkft',
                     'environment':  'dragonboard-845c-sdcard',
                     'hardware': 'db845c-sdcard',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline',},
    "mainline-aosp-master-sanity-sm8550-sdcard":
                    {'slug': 'mainline-aosp-master-sanity',
                     'group':'android-lkft',
                     'environment':  'sm8550-hdk',
                     'hardware': 'sm8550-hdk',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline',},
    "mainline-debug-aosp-master-sanity":
                    {'slug': 'mainline-debug-aosp-master-sanity',
                     'group':'android-lkft',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline-debug',},
    "mainline-16k-aosp-master-sanity-db845c":
                    {'slug': 'mainline-16k-aosp-master-sanity',
                     'group':'android-lkft',
                     'environment':  'dragonboard-845c',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline-16k',},
    "mainline-16k-aosp-master-sanity-rb5":
                    {'slug': 'mainline-16k-aosp-master-sanity',
                     'group':'android-lkft',
                     'environment':  'qrb5165-rb5',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : 'android-mainline',
                     'branch' : 'android-mainline-16k',},
}
## for benchmark projects
benchmark_rawkernels = {
    # For benchmarks
    'android12-5.10-benchmarks':[
            '5.10-gki-android12-android12-db845c-benchmarks',
            ],
    'android13-5.15-benchmarks':[
            '5.15-gki-android13-android13-db845c-benchmarks',
            ],
}
benchmark_projectids = {
    '5.10-gki-android12-android12-db845c-benchmarks':
                    {'slug': '5.10-gki-android12-android12-db845c-benchmarks',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.10',},
    '5.15-gki-android13-android13-db845c-benchmarks':
                    {'slug': '5.15-gki-android13-android13-db845c-benchmarks',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
}

benchmarkreports = {
    'Linux 5.15': [
                    'android13-5.15-benchmarks',
                ],
    'Linux 5.10': [
                    'android12-5.10-benchmarks',
                ],
}
## for boottime projects
boottime_rawkernels = {
    # For boottime projects
    'android16-6.12-boottime':[
            '6.12-gki-android16-aosp-master-rb5-boottime',
            '6.12-gki-android16-aosp-master-db845c-boottime',
            ],
    'android15-6.6-boottime':[
            '6.6-gki-android15-aosp-master-rb5-boottime',
            '6.6-gki-android15-aosp-master-db845c-boottime',
            '6.6-gki-android15-android15-rb5-boottime',
            '6.6-gki-android15-android15-db845c-boottime',
            ],

    'android15-6.6-debug-boottime':[
            '6.6-gki-android15-debug-aosp-master-db845c-boottime',
            ],

    'android14-6.1-lts-boottime':[
            '6.1-lts-gki-android14-aosp-master-rb5-boottime',
            '6.1-lts-gki-android14-aosp-master-db845c-boottime',
            ],

    'android14-6.1-boottime':[
            '6.1-gki-android14-aosp-master-rb5-boottime',
            '6.1-gki-android14-aosp-master-db845c-boottime',
            '6.1-gki-android14-android15-rb5-boottime',
            '6.1-gki-android14-android15-db845c-boottime',
            '6.1-gki-android14-android14-rb5-boottime',
            '6.1-gki-android14-android14-db845c-boottime',
            ],

    'android14-5.15-lts-boottime':[
            '5.15-lts-gki-android14-aosp-master-rb5-boottime',
            '5.15-lts-gki-android14-aosp-master-db845c-boottime',
            ],

    'android14-5.15-boottime':[
            '5.15-gki-android14-aosp-master-rb5-boottime',
            '5.15-gki-android14-aosp-master-db845c-boottime',
            '5.15-gki-android14-android15-rb5-boottime',
            '5.15-gki-android14-android15-db845c-boottime',
            '5.15-gki-android14-android14-rb5-boottime',
            '5.15-gki-android14-android14-db845c-boottime',
            ],

    'android13-5.15-boottime':[
            '5.15-gki-android13-aosp-master-rb5-boottime',
            '5.15-gki-android13-aosp-master-db845c-boottime',
            '5.15-gki-android13-android15-db845c-boottime',
            '5.15-gki-android13-android15-rb5-boottime',
            '5.15-gki-android13-android14-db845c-boottime',
            '5.15-gki-android13-android14-rb5-boottime',
            '5.15-gki-android13-android13-db845c-boottime',
            '5.15-gki-android13-android13-rb5-boottime',
            ],

    'android13-5.10-boottime':[
            '5.10-gki-android13-aosp-master-db845c-boottime',
            '5.10-gki-android13-android15-db845c-boottime',
            '5.10-gki-android13-android14-db845c-boottime',
            '5.10-gki-android13-android13-db845c-boottime',
            ],

    'android12-5.10-lts-boottime':[
            '5.10-lts-gki-android12-android12-db845c-boottime',
            '5.10-lts-gki-android12-android12-hikey960-boottime',
            ],

    'android12-5.10-boottime':[
            '5.10-gki-aosp-master-db845c-boottime',
            '5.10-gki-aosp-master-hikey960-boottime',
            '5.10-gki-android12-android12-db845c-boottime',
            '5.10-gki-android12-android12-hikey960-boottime',
            '5.10-gki-android12-android13-db845c-boottime',
            ],

    'android12-5.4-lts-boottime':[
            '5.4-lts-gki-android12-android12-db845c-boottime', # android12-5.4-lts
            '5.4-lts-gki-android12-android12-hikey960-boottime', # android12-5.4-lts
            ],
    'android12-5.4-boottime':[
            '5.4-gki-aosp-master-db845c-boottime',  # android12-5.4
            '5.4-gki-aosp-master-hikey960-boottime', # android12-5.4
            '5.4-gki-android12-android12-db845c-boottime', # android12-5.4
            '5.4-gki-android12-android12-hikey960-boottime', # android12-5.4
            '5.4-gki-android12-android13-db845c-boottime',
            ],
    'android11-5.4-lts-boottime':[
            '5.4-lts-gki-android11-android11-db845c-boottime', # android11-5.4-lts
            '5.4-lts-gki-android11-android11-hikey960-boottime', # android11-5.4-lts
            ],
    'android11-5.4-boottime':[
            '5.4-gki-android11-android11-db845c-boottime', # android11-5.4
            '5.4-gki-android11-android11-hikey960-boottime', # android11-5.4
            '5.4-gki-android11-android11-android12gsi-db845c-boottime', # android11-5.4
            '5.4-gki-android11-android11-android12gsi-hikey960-boottime', # android11-5.4
            '5.4-gki-android11-android12-db845c-boottime', # android11-5.4
            '5.4-gki-android11-android12-hikey960-boottime', # android11-5.4
            '5.4-gki-android11-aosp-master-db845c-boottime', # android11-5.4
            '5.4-gki-android11-aosp-master-hikey960-boottime', # android11-5.4
            '5.4-gki-android11-android13-db845c-boottime',
            ],

}
boottime_projectids = {
    ####################################################
    # projects for boottime
    ####################################################
    # projects for android16-6.12
    '6.12-gki-android16-aosp-master-db845c-boottime':
                    {'slug': '6.12-gki-android16-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.12',
                     'branch' : 'Android16-6.12',},
    '6.12-gki-android16-aosp-master-rb5-boottime':
                    {'slug': '6.12-gki-android16-aosp-master-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '6.12',
                     'branch' : 'Android16-6.12',},
    # projects for android15-6.6
    '6.6-gki-android15-aosp-master-db845c-boottime':
                    {'slug': '6.6-gki-android15-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6',},
    '6.6-gki-android15-aosp-master-rb5-boottime':
                    {'slug': '6.6-gki-android15-aosp-master-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6',},
    '6.6-gki-android15-android15-db845c-boottime':
                    {'slug': '6.6-gki-android15-android15-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6',},
    '6.6-gki-android15-android15-rb5-boottime':
                    {'slug': '6.6-gki-android15-android15-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'Android15',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6',},

    '6.6-gki-android15-debug-aosp-master-db845c-boottime':
                    {'slug': '6.6-gki-android15-debug-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.6',
                     'branch' : 'Android15-6.6-debug',},
    # projects for android14-6.1-lts
    '6.1-lts-gki-android14-aosp-master-db845c-boottime':
                    {'slug': '6.1-lts-gki-android14-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1-lts',},
    '6.1-lts-gki-android14-aosp-master-rb5-boottime':
                    {'slug': '6.1-lts-gki-android14-aosp-master-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1-lts',},

    # projects for android14-6.1
    '6.1-gki-android14-aosp-master-db845c-boottime':
                    {'slug': '6.1-gki-android14-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-aosp-master-rb5-boottime':
                    {'slug': '6.1-gki-android14-aosp-master-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-android15-db845c-boottime':
                    {'slug': '6.1-gki-android14-android15-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-android15-rb5-boottime':
                    {'slug': '6.1-gki-android14-android15-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'Android15',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-android14-db845c-boottime':
                    {'slug': '6.1-gki-android14-android14-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android14',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},
    '6.1-gki-android14-android14-rb5-boottime':
                    {'slug': '6.1-gki-android14-android14-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'Android14',
                     'kern' : '6.1',
                     'branch' : 'Android14-6.1',},

    # projects for android14-5.15-lts
    '5.15-lts-gki-android14-aosp-master-db845c-boottime':
                    {'slug': '5.15-lts-gki-android14-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15-lts',},
    '5.15-lts-gki-android14-aosp-master-rb5-boottime':
                    {'slug': '5.15-lts-gki-android14-aosp-master-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15-lts',},

    # projects for android14-5.15
    '5.15-gki-android14-aosp-master-db845c-boottime':
                    {'slug': '5.15-gki-android14-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},
    '5.15-gki-android14-aosp-master-rb5-boottime':
                    {'slug': '5.15-gki-android14-aosp-master-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},
    '5.15-gki-android14-android15-db845c-boottime':
                    {'slug': '5.15-gki-android14-android15-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},
    '5.15-gki-android14-android15-rb5-boottime':
                    {'slug': '5.15-gki-android14-android15-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'Android15',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},
    '5.15-gki-android14-android14-db845c-boottime':
                    {'slug': '5.15-gki-android14-android14-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android14',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},
    '5.15-gki-android14-android14-rb5-boottime':
                    {'slug': '5.15-gki-android14-android14-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'Android14',
                     'kern' : '5.15',
                     'branch' : 'Android14-5.15',},

    # android13-5.15
    '5.15-gki-android13-aosp-master-db845c-boottime':
                    {'slug': '5.15-gki-android13-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-aosp-master-rb5-boottime':
                    {'slug': '5.15-gki-android13-aosp-master-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'AOSP',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android15-db845c-boottime':
                    {'slug': '5.15-gki-android13-android15-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android15-rb5-boottime':
                    {'slug': '5.15-gki-android13-android15-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'Android15',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android14-db845c-boottime':
                    {'slug': '5.15-gki-android13-android14-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android14',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android14-rb5-boottime':
                    {'slug': '5.15-gki-android13-android14-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'Android14',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android13-db845c-boottime':
                    {'slug': '5.15-gki-android13-android13-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},
    '5.15-gki-android13-android13-rb5-boottime':
                    {'slug': '5.15-gki-android13-android13-rb5-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'rb5',
                     'OS' : 'Android13',
                     'kern' : '5.15',
                     'branch' : 'Android13-5.15',},

    # android13-5.10
    '5.10-gki-android13-aosp-master-db845c-boottime':
                    {'slug': '5.10-gki-android13-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.10',
                     'branch' : 'Android13-5.10',},
    '5.10-gki-android13-android14-db845c-boottime':
                    {'slug': '5.10-gki-android13-android14-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android14',
                     'kern' : '5.10',
                     'branch' : 'Android13-5.10',},
    '5.10-gki-android13-android15-db845c-boottime':
                    {'slug': '5.10-gki-android13-android15-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android15',
                     'kern' : '5.10',
                     'branch' : 'Android13-5.10',},
    '5.10-gki-android13-android13-db845c-boottime':
                    {'slug': '5.10-gki-android13-android13-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.10',
                     'branch' : 'Android13-5.10',},

    # android12-5.10-lts
    '5.10-lts-gki-android12-android12-hikey960-boottime':
                    {'slug': '5.10-lts-gki-android12-android12-hikey960-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'HiKey960',
                     'OS' : 'Android12',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10-lts',},
    '5.10-lts-gki-android12-android12-db845c-boottime':
                    {'slug': '5.10-lts-gki-android12-android12-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10-lts',},
    # android12-5.10
    '5.10-gki-aosp-master-hikey960-boottime':
                    {'slug': '5.10-gki-aosp-master-hikey960-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'HiKey960',
                     'OS' : 'AOSP',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},
    '5.10-gki-aosp-master-db845c-boottime':
                    {'slug': '5.10-gki-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},
    '5.10-gki-android12-android13-db845c-boottime':
                    {'slug': '5.10-gki-android12-android13-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},
    '5.10-gki-android12-android12-db845c-boottime':
                    {'slug': '5.10-gki-android12-android12-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},
    '5.10-gki-android12-android12-hikey960-boottime':
                    {'slug': '5.10-gki-android12-android12-hikey960-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'HiKey960',
                     'OS' : 'Android12',
                     'kern' : '5.10',
                     'branch' : 'Android12-5.10',},

    # for android12-5.4-lts
    '5.4-lts-gki-android12-android12-hikey960-boottime':
                    {'slug': '5.4-lts-gki-android12-android12-hikey960-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'HiKey960',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4-lts',},
    '5.4-lts-gki-android12-android12-db845c-boottime':
                    {'slug': '5.4-lts-gki-android12-android12-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4-lts',},

    # for android12-5.4
    '5.4-gki-aosp-master-hikey960-boottime':
                    {'slug': '5.4-gki-android12-aosp-master-hikey960-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'HiKey960',
                     'OS' : 'AOSP',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},
    '5.4-gki-aosp-master-db845c-boottime':
                    {'slug': '5.4-gki-android12-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},
    '5.4-gki-android12-android12-db845c-boottime':
                    {'slug': '5.4-gki-android12-android12-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},
    '5.4-gki-android12-android12-hikey960-boottime':
                    {'slug': '5.4-gki-android12-android12-hikey960-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'HiKey960',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},
    '5.4-gki-android12-android13-db845c-boottime':
                    {'slug': '5.4-gki-android12-android13-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.4',
                     'branch' : 'Android12-5.4',},
    # android11-5.4-lts
    '5.4-lts-gki-android11-android11-db845c-boottime':
                    {'slug': '5.4-lts-gki-android11-android11-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android11-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4-lts',},
    '5.4-lts-gki-android11-android11-hikey960-boottime':
                    {'slug': '5.4-lts-gki-android11-android11-hikey960-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'hikey960',
                     'OS' : 'Android11-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4-lts',},
    # android11-5.4
    '5.4-gki-android11-android11-db845c-boottime':
                    {'slug': '5.4-gki-android11-android11-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android11-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android11-hikey960-boottime':
                    {'slug': '5.4-gki-android11-android11-hikey960-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'hikey960',
                     'OS' : 'Android11-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android11-android12gsi-db845c-boottime':
                    {'slug': '5.4-gki-android11-android11-android12gsi-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android12-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android11-android12gsi-hikey960-boottime':
                    {'slug': '5.4-gki-android11-android11-android12gsi-hikey960-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'hikey960',
                     'OS' : 'Android12-GSI',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android12-hikey960-boottime':
                    {'slug': '5.4-gki-android11-android12-hikey960-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'HiKey960',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android12-db845c-boottime':
                    {'slug': '5.4-gki-android11-android12-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android12',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-aosp-master-db845c-boottime':
                    {'slug': '5.4-gki-android11-aosp-master-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'AOSP',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-aosp-master-hikey960-boottime':
                    {'slug': '5.4-gki-android11-aosp-master-hikey960-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'hikey960',
                     'OS' : 'AOSP',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},
    '5.4-gki-android11-android13-db845c-boottime':
                    {'slug': '5.4-gki-android11-android13-db845c-boottime',
                     'group':'android-lkft-benchmarks',
                     'hardware': 'db845c',
                     'OS' : 'Android13',
                     'kern' : '5.4',
                     'branch' : 'Android11-5.4',},

}



boottimereports = {
    'Linux 6.12': [
                    'android16-6.12-boottime',
                ],
    'Linux 6.6': [
                    'android15-6.6-boottime',
                    'android15-6.6-debug-boottime',
                ],
    'Linux 6.1': [
                    'android14-6.1-boottime',
                    'android14-6.1-lts-boottime',
                ],
    'Linux 5.15': [
                    'android14-5.15-lts-boottime',
                    'android14-5.15-boottime',
                    'android13-5.15-boottime',
                ],
    'Linux 5.10': [
                    'android13-5.10-boottime',
                    'android12-5.10-lts-boottime',
                    'android12-5.10-boottime',
                ],
    'Linux 5.4': [
                    'android12-5.4-boottime',
                    'android12-5.4-lts-boottime',
                    'android11-5.4-boottime',
                    'android11-5.4-lts-boottime',
                ],
}

def get_all_benchmark_report_kernels():
    return benchmark_rawkernels

def get_all_benchmark_report_projects():
    return benchmark_projectids

def get_all_benchmark_reports():
    return benchmarkreports

def get_all_boottime_report_kernels():
    return boottime_rawkernels

def get_all_boottime_report_projects():
    return boottime_projectids

def get_all_boottime_reports():
    return boottimereports

def get_branch_categories_pair():
    return branch_categories_pair

def get_all_report_kernels():
    return rawkernels

def get_all_report_projects():
    return projectids

def get_all_kerver_reports():
    return kerverreports
