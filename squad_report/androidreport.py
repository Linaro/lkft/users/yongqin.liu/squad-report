import datetime
import jinja2
import json
import logging
import os
import re
import requests
import resource
import shutil
import tarfile
import tempfile
import time
import zipfile

import squad_report.logging
import xml.etree.ElementTree as ET

from squad_client.core.models import Squad, Build, TestRun, TestRunMetadata, ALL
from squad_report.androidreportconfig import get_all_report_kernels, get_all_report_projects
from squad_report.androidreportconfig import get_all_benchmark_report_kernels, get_all_benchmark_report_projects
from squad_report.androidcommon import TestNumbers, LinaroAndroidLKFTBug, get_aware_datetime_from_str, tab_spaces
from squad_report.androidcommon import set_job_name_for_unsubmitted_jobs

try:
    from urllib.parse import urlsplit
except ImportError:
    from urlparse import urlsplit

# logging.basicConfig(format="[%(asctime)s.%(msecs)03d] %(levelname)s [%(name)s:%(lineno)s] %(message)s")
# logger = logging.getLogger(__name__)
# logger.setLevel("INFO")
logger = squad_report.logging.getLogger(__name__)

TEST_RESULT_XML_NAME = 'test_result.xml'
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DATA_FILE_DIR = os.path.join(BASE_DIR, "datafiles")
# Directoy used to store cts vts result files
FILES_DIR = os.path.join(DATA_FILE_DIR, "files/cts-vts")
DIR_ATTACHMENTS = os.path.join(FILES_DIR, 'lkft')

### NUMBERS DEFINITION #####
# More than 80% modulse should be done
# which means it will be marked as error when more then 20% modules not done
PERCENTAGE_MODULES_NUMBER_CHECK = 0.8
# only check the done percentage when there are more than 2 modules executed
NUMBER_MODULES_TOTAL_CHECK = 2


# Get a snapshot of the current mem consumption
mem_snapshot = lambda: resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024

# bugzilla instance used to get bugs
bugzilla_instance = LinaroAndroidLKFTBug(
                        host_name=os.environ.get("BUGZILLA_URL", "https://bugs.linaro.org"),
                        api_key=os.environ.get("BUGZILLA_TOKEN", ""))
# pool to cache bugs
project_platform_bugs = {}

# to compare durations for bugs or jobs
datetime_now = datetime.datetime.now(datetime.timezone.utc)

def is_cts_vts_job(lava_job_name):
    return lava_job_name.find('cts') >= 0 or lava_job_name.find('vts') >= 0

def is_kunit_job(lava_job_name):
    return lava_job_name.find('kunit') >= 0

def parse_kernel_version_string(versionString):
    ## 5.13.0, 5.13.0-50292ffdbbdb, 5.14.0-rc2, or 5.14.0-rc2-754a0abed174
    versionDict = { 'Major':0,
                    'Minor':0,
                    'Extra':0,
                    'versionString': versionString}

    if versionString.startswith('v'):
        versionString = versionString[1:]
    # print versionString
    tokens = re.split( r'[.-]', versionString)
    # print tokens
    if tokens[0].isnumeric() and tokens[1].isnumeric() and tokens[2].isnumeric():
        versionDict['Major'] = tokens[0]
        versionDict['Minor'] = tokens[1]
        versionDict['Extra'] = tokens[2]

    tokens_hyphen = versionString.split('-')
    if len(tokens_hyphen) >= 2:
        if tokens_hyphen[1].startswith('rc'):
            # for case of 5.14.0-rc2, or 5.14.0-rc2-754a0abed174
            versionDict['rc'] = tokens_hyphen[1]
            if len(tokens_hyphen) == 3:
                versionDict['sha'] = tokens_hyphen[2]
            else:
                # for case of 5.14.0-rc2, no sha specified
                pass
        else:
            # for case of 5.13.0-50292ffdbbdb, not rc version
            versionDict['sha'] = tokens_hyphen[1]
    else:
        # for case of 5.13.0, not rc version, and no sha specified
        pass

    return versionDict


def get_build_kernel_version(build):
    build_version = build.version
    ## 5.16.0-rc8-3b4efafcb8a9 should be older than 5.16.0-6e9d208fdc7b
    versionDict = parse_kernel_version_string(build_version)
    major = int(versionDict.get('Major', 0))
    minor = int(versionDict.get('Minor', 0))
    extra = int(versionDict.get('Extra', 0))
    # set rc to the max number, so that we could make sure that rc version listed before the release versions
    rc = int(versionDict.get('rc', '65536').replace('rc', ''))
    return (major, minor, extra, rc)


def get_attachment_urls(jobs=[]):
    '''
        ALL JOBS must be belong to the same build
    '''
    if len(jobs) == 0:
        return

    first_job = jobs[0]
    target_build_id = first_job.target_build.strip('/').split('/')[-1]
    # TODO squad is not defined here
    target_build = Build().get(target_build_id)
    target_build_metadata = target_build.metadata

    for job in jobs:
        if not is_cts_vts_job(job.name):
            job.attachment_url  = None
            continue

        if getattr(job, 'attachment_url', None):
            continue

        attachment_url_key = 'tradefed_results_url_%s' % job.job_id
        job.attachment_url = getattr(target_build_metadata, attachment_url_key, None)
        # if not job.job_status or job.job_status == 'Submitted' or job.job_status == 'Running' \
              # or job.job_status != 'Complete' or job.job_status == 'Incomplete' or job.job_status != 'Canceled':
        if (job.job_status is None or job.job_status == "Submitted" or job.job_status == "Running" or job.job_status == "Fetching") \
                and job.attachment_url is not None:
            ## TODO: HACK: should be dleted after STG-5142 resolved
            ## since the attachment_url is genrated, the most of the case should be
            ## just the job status is not set to complete correctly
            ## if it's found coconfusions are made by this change,
            ## maybe it needs to fetch results from lava directly
            job.job_status = "Complete"
        if job.job_status != 'Complete':
            job.attachment_url  = None
            continue


def extract_save_result_file(tar_path, result_zip_path):
    zip_parent = os.path.abspath(os.path.join(result_zip_path, os.pardir))
    if not os.path.exists(zip_parent):
        os.makedirs(zip_parent)
    # https://pymotw.com/2/zipfile/
    tar = tarfile.open(tar_path, "r")
    for f_name in tar.getnames():
        if f_name.endswith("/%s" % TEST_RESULT_XML_NAME):
            result_fd = tar.extractfile(f_name)
            with zipfile.ZipFile(result_zip_path, 'w') as f_zip_fd:
                f_zip_fd.writestr(TEST_RESULT_XML_NAME, result_fd.read(), compress_type=zipfile.ZIP_DEFLATED)
                logger.info('Save result in %s to %s' % (tar_path, result_zip_path))

    tar.close()


def get_result_file_path(job=None):
    job_id = job.job_id
    lava_netloc = urlsplit( job.external_url ).netloc
    result_file_path = os.path.join(DIR_ATTACHMENTS, "%s-%s.zip" % (lava_netloc.replace('.', '-'), job_id))
    return result_file_path


def download_urllib(url, path, header=None):
    check_dict = {'file_not_exist': False}
    if header is None or len(header) == 0:
        cmd_download = f"curl -fsSL --retry 20 {url} -o {path}"
    else:
        cmd_download = f"curl -fsSL --retry 20 --header '{header}' {url} -o {path}"
    #logger.info("download command:%s" % cmd_download)
    ret = os.system(cmd_download)
    if ret != 0:
        logger.info("Failed to download file: %s" % url)
        check_dict['file_not_exist'] = True
        os.system(f"rm -fr {path}")

    if not check_dict['file_not_exist']:
        logger.info("File is saved to %s" % path)
    return check_dict['file_not_exist']

def print_disk_usage():
    total, used, free = shutil.disk_usage("/")
    print(f"Total: {total // (2**30)} GiB, Used: {used // (2**30)} GiB, Free:{( free // 2**30)} GiB")

def download_attachments_save_result(jobs=[], fetch_latest=False):
    if len(jobs) == 0:
        return

    # https://lkft.validation.linaro.org/scheduler/job/566144
    get_attachment_urls(jobs=jobs)
    for job in jobs:
        if job.job_status != 'Complete':
            continue

        if not job.submitted:
            continue

        if is_cts_vts_job(job.name):
            set_job_modules_testcases_numbers_for_ctsvts(job)
        elif is_kunit_job(job.name):
            set_job_modules_testcases_numbers_for_kunit(job)


def remove_xml_unsupport_character(etree_content=""):
    rx = re.compile("&#([0-9]+);|&#x([0-9a-fA-F]+);")
    endpos = len(etree_content)
    pos = 0
    while pos < endpos:
        # remove characters that don't conform to XML spec
        m = rx.search(etree_content, pos)
        if not m:
            break
        mstart, mend = m.span()
        target = m.group(1)
        if target:
            num = int(target)
        else:
            num = int(m.group(2), 16)
        # #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
        if not(num in (0x9, 0xA, 0xD)
                or 0x20 <= num <= 0xD7FF
                or 0xE000 <= num <= 0xFFFD
                or 0x10000 <= num <= 0x10FFFF):
            etree_content = etree_content[:mstart] + etree_content[mend:]
            endpos = len(etree_content)
            # next time search again from the same position as this time
            # as the detected pattern was removed here
            pos = mstart
        else:
            # continue from the end of this match
            pos = mend
    return etree_content


# for KUnit test:
# Note:
#   This funtion may be used for other non-cts-vts tests as well,
#   But for the moment KUnit is supported, so set the name to _for_kunit
#   In the future, it it's tested for other case, the name could be updated then
def set_job_modules_testcases_numbers_for_kunit(job, module_name="android-kunit", abi="arm64-v8a"):
    # function to save testcase information to database for tradefed result
    job.modules = []
    job.testcases = []
    job.failures = []

    if not hasattr(job, 'numbers'):
        job.numbers = TestNumbers()

    if not hasattr(job, 'testrun_obj'):
        return

    job.numbers.modules_done = 1
    job.numbers.modules_total = 1
    job.numbers.jobs_finished = 1


    test_module = {
            'name': module_name,
            'done': True,
            'abi': abi,
            'job': job,
            }
    for test in job.testrun_obj.tests().values():
        if not test.name.startswith(module_name):
            continue

        if test.status == 'pass':
            job.numbers.number_passed = job.numbers.number_passed + 1
        elif test.status == 'fail':
            job.numbers.number_failed = job.numbers.number_failed + 1
        elif test.status == 'skip':
            job.numbers.number_ignored = job.numbers.number_ignored + 1
        elif test.status == 'assumption_failure':
            # there should be no assumption_failure for the KUnit test
            job.numbers.number_assumption_failure = job.numbers.number_assumption_failure + 1
        job.numbers.number_total = job.numbers.number_total + 1

        if test.status == 'fail' or test.status == 'assumption_failure':
            # test.name:
            # android-kunit/regmap-kunit.regmap#write_readonly.rbtree-default_fast_I/O_@0x0
            test_short_name = test.name.split("/", maxsplit=1)[1]
            testcase = {
                    'test_name': test_short_name,
                    'test_class': test_short_name.split("#", maxsplit=1)[0],
                    'test_method': test_short_name.split("#", maxsplit=1)[1],
                    'result': test.status,
                    'testsuite': test_module,
                    'job': job,
                    }
            job.failures.append(testcase)

    test_module.update({
                            'number_passed': job.numbers.number_passed,
                            'number_total': job.numbers.number_total,
                            'failures': job.failures,
                        })
    job.modules.append(test_module)


def set_job_modules_testcases_numbers_for_ctsvts(job):
    # for cts /vts jobs
    job_id = job.job_id
    job_url = job.external_url
    job_name = job.name
    result_file_path = get_result_file_path(job)
    if not result_file_path:
        logger.info("Skip to get the attachment as the result_file_path is not found: %s %s" % (job_url, job.url))
        return

    attachment_url = getattr(job, 'attachment_url', None)
    if not attachment_url:
        error_msg = "No attachment found for the job from the squad side"
        logger.info(f"{error_msg}: {job_name} {job_name}")
        job.failure = {'error_msg': error_msg}
        return

    if not os.path.exists(result_file_path):
        # only need to get one temporary file name to be used by the wget command in the download_urllib function
        (temp_fd, temp_path) = tempfile.mkstemp(prefix="lkft-", suffix='.tar.xz', text=False)
        os.close(temp_fd)
        os.system(f"rm -fr {temp_path}")

        logger.info("Start downloading result file for job %s %s: %s" % (job_url, job_name, temp_path))
        squad_header = None
        squad_token = os.environ.get("SQUAD_TOKEN", None)
        if squad_token is not None and len(squad_token) > 0:
            squad_header = f"Authorization: token {squad_token}"

        ret_err = download_urllib(attachment_url, temp_path, header=squad_header)
        if ret_err:
            logger.info("There is a problem with the size of the file: %s" % attachment_url)
            return

        tar_f = temp_path.replace(".xz", '')
        ret = os.system("xz -d %s" % temp_path)
        if ret != 0 :
            logger.info("Failed to decompress %s with xz -d command for job: %s " % (temp_path, job_url))
            os.system(f"rm -fr {temp_path}")
            return

        extract_save_result_file(tar_f, result_file_path)
        os.unlink(tar_f)

    if os.path.exists(result_file_path):
        string_method = os.getenv("TRADEFED_PARSE_METHOD_WITH_ITERPARSE", 'False').lower()
        use_iterparse = string_method.lower() == 'true'
        if not use_iterparse:
            set_job_modules_testcases_numbers(result_file_path, job)
        else:
            set_job_modules_testcases_numbers_with_iterparse(result_file_path, job)
    else:
        # for cases that test_result.xml does not exist in the tradefed result attachment zip file
        logger.info("Failed to save the test_result.xml file locally for : %s %s" % (job_url, job_name))
        return

# function to save testcase information to database for tradefed result
def set_job_modules_testcases_numbers_with_iterparse(result_file_path, job):
    job.modules = []
    job.failures = []
    assumption_failures = []
    ignored_testcases = []
    if not hasattr(job, 'numbers'):
        job.numbers = TestNumbers()

    abi = ""
    module_name = None
    test_module = {}
    test_class_name = None
    test_method_name = None
    testcase = {}
    all_testcases_number = 0
    with zipfile.ZipFile(result_file_path, 'r') as f_zip_fd:
        try:
            # https://docs.python.org/3/library/xml.etree.elementtree.html
            for event, element in ET.iterparse(f_zip_fd.open(TEST_RESULT_XML_NAME), events=['start', 'end']):
                # logger.info(f"{mem_snapshot()} MB memory used")
                if element.tag == 'Summary' and event == 'start':
                    job.numbers.number_passed = int(element.attrib['pass'])
                    job.numbers.number_failed = int(element.attrib['failed'])
                    # job.numbers.number_regressions = 0
                    # job.numbers.number_antiregressions = 0
                    job.numbers.modules_done = int(element.attrib['modules_done'])
                    job.numbers.modules_total = int(element.attrib['modules_total'])
                    job.numbers.jobs_finished = 1
                    job.numbers.jobs_total = 1
                    # job.numbers.number_total = len(all_testcases)

                    if job.numbers.modules_total <= 0:
                        return
                    element.clear()
                elif element.tag == 'Module' and event == 'start':
                    abi = element.attrib['abi']
                    module_name = element.attrib['name']
                    done = element.attrib['done']
                    module_number_pass = element.attrib['pass']
                    module_number_total = element.get('total_tests', 0)
                    # if module_number_total == 0:
                    #     module_number_total = len(elem.findall('.//Test'))
                    module_failures = []
                    test_module = {
                        'name': module_name,
                        'done': done == 'true',
                        'abi': abi,
                        'number_passed': int(module_number_pass),
                        'number_total': int(module_number_total),
                        'failures': module_failures,
                        'job': job,
                        }
                    job.modules.append(test_module)
                    element.clear()
                elif element.tag == 'TestCase' and event == 'start':
                    test_class_name = element.attrib['name']
                    element.clear()
                elif element.tag == 'Test' and event == 'start':
                    test_method_name = element.attrib["name"]
                    if test_method_name.endswith('_64bit') or test_method_name.endswith('_32bit'):
                        test_name = '%s#%s' % (test_class_name, test_method_name)
                    else:
                        test_name = '%s#%s#%s' % (test_class_name, test_method_name, abi)
                    #result is one of: 'pass', 'fail', 'IGNORED', ASSUMPTION_FAILURE'
                    test_result = element.get('result')
                    testcase = {
                                'test_name': test_name,
                                'test_class': test_class_name,
                                'test_method': test_method_name,
                                'result': test_result,
                                'testsuite': test_module,
                                'job': job,
                                }
                    all_testcases_number = all_testcases_number + 1
                    if test_result == 'fail':
                        job.failures.append(testcase)
                        module_failures.append(testcase)
                    elif test_result == 'IGNORED':
                        ignored_testcases.append(testcase)
                    elif test_result == 'ASSUMPTION_FAILURE':
                        assumption_failures.append(testcase)
                        job.failures.append(testcase)
                        module_failures.append(testcase)

                    element.clear()
                elif element.tag == 'Failure' and event == 'start':
                    message = element.attrib['message']
                    testcase['message'] = message
                    element.clear()
                elif element.tag == 'StackTrace':
                    if event == 'end':
                        stacktrace = element.text
                        testcase['stacktrace'] = stacktrace
                        element.clear()
                else:
                    # Release tag resources
                    element.clear()

            job.numbers.number_assumption_failure = len(assumption_failures)
            job.numbers.number_ignored = len(ignored_testcases)
            job.numbers.number_total = all_testcases_number
            all_testcases = []
            ignored_testcases = []
            assumption_failures = []
        except ET.ParseError as e:
            logger.error('xml.etree.ElementTree.ParseError: %s' % e)
            logger.info('Please Check %s manually' % result_zip_path)
            return False


# function to save testcase information to database for tradefed result
def set_job_modules_testcases_numbers(result_file_path, job):
    job.modules = []
    job.testcases = []
    job.failures = []
    if not hasattr(job, 'numbers'):
        job.numbers = TestNumbers()

    with zipfile.ZipFile(result_file_path, 'r') as f_zip_fd:
        try:
            # https://docs.python.org/3/library/xml.etree.elementtree.html
            root = ET.fromstring(remove_xml_unsupport_character(f_zip_fd.read(TEST_RESULT_XML_NAME).decode('utf-8')))
            summary_node = root.find('Summary')
            assumption_failures = root.findall(".//Module/TestCase/Test[@result='ASSUMPTION_FAILURE']")
            ignored_testcases = root.findall(".//Module/TestCase/Test[@result='IGNORED']")
            all_testcases = root.findall(".//Module/TestCase/Test")

            job.numbers.number_passed = int(summary_node.attrib['pass'])
            job.numbers.number_failed = int(summary_node.attrib['failed'])
            job.numbers.number_assumption_failure = len(assumption_failures)
            job.numbers.number_ignored = len(ignored_testcases)
            job.numbers.number_total = len(all_testcases)
            # job.numbers.number_regressions = 0
            # job.numbers.number_antiregressions = 0
            job.numbers.modules_done = int(summary_node.attrib['modules_done'])
            job.numbers.modules_total = int(summary_node.attrib['modules_total'])
            job.numbers.jobs_finished = 1
            all_testcases = []
            ignored_testcases = []
            assumption_failures = []
            job.numbers.jobs_total = 1

            if job.numbers.modules_total <= 0:
                return
            for elem in root.findall('Module'):
                abi = elem.attrib['abi']
                module_name = elem.attrib['name']
                done = elem.attrib['done']
                runtime = elem.attrib['runtime']
                module_number_pass = int(elem.get('pass', 0))
                module_number_total = int(elem.get('total_tests', 0))
                if module_number_total == 0:
                    module_number_total = len(elem.findall('.//Test'))

                if module_number_total == 0:
                    reason_node = elem.find('.//Reason')
                    if reason_node is not None:
                        module_reason = reason_node.get('message')
                    else:
                        module_reason = None
                else:
                    module_reason = None

                module_failures = []
                test_module = {
                    'name': module_name,
                    'done': done == 'true',
                    'abi': abi,
                    'number_passed': int(module_number_pass),
                    'number_total': int(module_number_total),
                    'failures': module_failures,
                    'job': job,
                    'reason': module_reason,
                    'runtime': int(runtime),
                    }
                job.modules.append(test_module)

                # test classes
                test_class_nodes = elem.findall('.//TestCase')
                for test_class_node in test_class_nodes:
                    test_class_name = test_class_node.get('name')
                    test_case_nodes = test_class_node.findall('.//Test')
                    for test_method_node in test_case_nodes:
                        test_method_name = test_method_node.get("name")
                        if test_method_name.endswith('_64bit') or test_method_name.endswith('_32bit'):
                            test_name = '%s#%s' % (test_class_name, test_method_name)
                        else:
                            test_name = '%s#%s#%s' % (test_class_name, test_method_name, abi)

                        #result is one of: 'pass', 'fail', 'IGNORED', ASSUMPTION_FAILURE'
                        test_result = test_method_node.get('result')
                        testcase = {
                                'test_name': test_name,
                                'test_class': test_class_name,
                                'test_method': test_method_name,
                                'result': test_result,
                                'testsuite': test_module,
                                'job': job,
                                }
                        # job.testcases.append(testcase)
                        if test_result == 'fail' or test_result == 'ASSUMPTION_FAILURE':
                            message = test_method_node.find('.//Failure').get('message')
                            stacktrace = test_method_node.find('.//Failure/StackTrace').text
                            testcase['message'] = message
                            testcase['stacktrace'] = stacktrace
                            job.failures.append(testcase)
                            module_failures.append(testcase)

        except ET.ParseError as e:
            logger.error('xml.etree.ElementTree.ParseError: %s' % e)
            logger.info('Please Check %s manually' % result_zip_path)
            return False

def get_job_short_name(job_name):
    if job_name is None:
        return "unsubmitted-job"
    # the lava jobs name is defined in the format like this: lkft-android-{{KERNEL_BRANCH}}-{{BUILD_NUMBER}}-cts-lkft
    # https://git.linaro.org/ci/job/configs.git/tree/lkft/lava-job-definitions/common/template-cts-lkft.yaml#n3
    # so assuming that there is no "-(\d+)-" pattern in the kernel branch name
    job_name_pattern = re.compile('^lkft-android-(?P<kernel_branch>\S+?)-(?P<ci_build_number>\d+?)-(?P<job_name_short>\S+)$')
    match = job_name_pattern.match(job_name)
    if match:
        # kernel_branch= match.group('kernel_branch')
        # ci_build_number = match.group('ci_build_number')
        job_name_short= match.group('job_name_short')
    else:
        job_name_short = job_name

    return job_name_short


def update_object(dst_obj, src_obj):
    if src_obj is None:
        return
    # all the attribute of src_obj are simple types, like string, int, boolean, etc
    for attr_name, attr_val in src_obj.__dict__.items():
        env_attr_val = getattr(dst_obj, attr_name, None)
        if env_attr_val is None:
            setattr(dst_obj, attr_name, attr_val)
        elif isinstance(env_attr_val, str) and (env_attr_val != attr_val):
            setattr(dst_obj, attr_name, [env_attr_val, attr_val])
        elif isinstance(env_attr_val, list) and (attr_val not in env_attr_val):
            env_attr_val.append(attr_val)
        else:
            # for cases that the value of the attribute are the same
            # or the value if already there in the target attribute
            pass

def get_classified_jobs(jobs=[], environment=None):
    '''
        remove the resubmitted jobs and duplicated jobs(needs the jobs to be sorted in job_id descending order)
        as the result for the resubmit(including the duplicated jobs) jobs should be ignored.
    '''
    # the lava jobs name is defined in the format like this: lkft-android-{{KERNEL_BRANCH}}-{{BUILD_NUMBER}}-cts-lkft
    # https://github.com/Linaro/lava-test-plans/blob/master/lava_test_plans/testcases/master/template-master.jinja2#L11
    # so assuming that there is no "-(\d+)-" pattern in the kernel branch name
    resubmitted_job_urls = [ job.parent_job for job in jobs if job.parent_job ]
    job_name_environments = []
    jobs_to_be_checked = []
    resubmitted_or_duplicated_jobs = []

    def get_job_external_url(item):
        external_url = item.external_url
        if external_url:
            return external_url
        # when the job is not submitted to lava server, external_url will be None
        # unorderable types: NoneType() < NoneType()
        return ""

    # sorted with the job id in lava server
    # to get the latest jobs to use
    # here assuming the same job will be resubmitted to the same lava server,
    # and the job for the old build would not be resubmitted if new build is retriggered
    sorted_jobs = sorted(jobs, key=get_job_external_url, reverse=True)

    for job in sorted_jobs:
        job_environment = job.environment
        if environment is not None and job_environment != environment:
            continue

        if job.url in resubmitted_job_urls:
            # ignore jobs which were resubmitted
            job.resubmitted = True
            resubmitted_or_duplicated_jobs.append(job)
            continue

        job_name_short = get_job_short_name(job.name)

        if f"{job_name_short}|{job_environment}" in job_name_environments:
            job.duplicated = True
            resubmitted_or_duplicated_jobs.append(job)
            continue

        jobs_to_be_checked.append(job)
        job_name_environments.append(f"{job_name_short}|{job_environment}")

    return {
        'final_jobs': jobs_to_be_checked,
        'resubmitted_or_duplicated_jobs': resubmitted_or_duplicated_jobs,
        }

def assign_testrun_for_testjobs(testjobs):
    for testjob in testjobs:
        if testjob.testrun is None:
            continue
        testrun_id = testjob.testrun.strip('/').split('/')[-1]
        testrun_obj = TestRun().get(testrun_id)
        if testrun_obj is not None:
            testjob.testrun_obj = testrun_obj
            testjob.metadata = testrun_obj.metadata
            testjob.metrics = testrun_obj.metrics()

def get_build_environment_metadata(build, jobs_to_be_checked):
    environment_metadata = TestRunMetadata()
    for testjob in jobs_to_be_checked:
        update_object(environment_metadata, testjob.metadata)
    return environment_metadata

def get_lkft_build_status(build, jobs_to_be_checked):
    if isinstance(build.created_at, str):
        last_fetched_timestamp = get_aware_datetime_from_str(build.created_at)
    else:
        last_fetched_timestamp = build.created_at
    has_unsubmitted = False
    has_canceled = False
    is_inprogress = False
    for job in jobs_to_be_checked:
        if not job.submitted:
            has_unsubmitted = True
        elif job.job_status == 'Canceled':
            has_canceled = True
        elif job.fetched:
            if job.fetched_at:
                job_last_fetched_timestamp = get_aware_datetime_from_str(job.fetched_at)
                # others are for the case that the job is fetched, but null for the fetched_at
            elif job.last_fetch_attempt:
                job_last_fetched_timestamp = get_aware_datetime_from_str(job.last_fetch_attempt)
            elif job.submitted_at:
                job_last_fetched_timestamp = get_aware_datetime_from_str(job.submitted_at)
            else:
                job_last_fetched_timestamp = get_aware_datetime_from_str(job.created_at)
            if job_last_fetched_timestamp > last_fetched_timestamp:
                last_fetched_timestamp = job_last_fetched_timestamp
        else:
            # running or submitted
            is_inprogress = True

    if has_unsubmitted:
        build.build_status = "JOBSNOTSUBMITTED"
    elif is_inprogress:
        build.build_status = "JOBSINPROGRESS"
    elif has_canceled:
        build.build_status = "CANCELED"
    else:
        build.build_status = "JOBSCOMPLETED"
        build.last_fetched_timestamp = last_fetched_timestamp

    return {
        'is_inprogress': is_inprogress,
        'has_unsubmitted': has_unsubmitted,
        'has_canceled': has_canceled,
        'last_fetched_timestamp': last_fetched_timestamp,
        }


def find_best_two_runs(builds, project_name, exact_ver1="", exact_ver2="", reverse_build_order=False, no_check_kernel_version=True, environment=None):
    goodruns = []
    bailaftertwo = 0
    number_of_build_with_jobs = 0
    baseExactVersionDict=None
    nextVersionDict=None

    if exact_ver1 is not None and len(exact_ver1) > 0 and exact_ver1 !='No':
        baseExactVersionDict = parse_kernel_version_string(exact_ver1)

    sorted_builds = sorted(builds, key=get_build_kernel_version, reverse=True)
    for build in sorted_builds:
        build_version = build.version

        if bailaftertwo == 2:
            break
        elif bailaftertwo == 0 :
            baseVersionDict = parse_kernel_version_string(build_version)
            if baseExactVersionDict is not None \
                    and not baseVersionDict['versionString'].startswith(exact_ver1):
                logger.info('Skip the check as it is not the specified version for %s %s', project_name, build_version)
                continue
            # print "baseset"
        elif bailaftertwo == 1 :
            nextVersionDict = parse_kernel_version_string(build_version)
            if exact_ver2 is not None \
                    and not nextVersionDict['versionString'].startswith(exact_ver2):
                # for case that second build version specified, but not this build
                logger.info('Skip the check as it is not the specified version for %s %s', project_name, build_version)
                continue
            elif not no_check_kernel_version and nextVersionDict['Extra'] == baseVersionDict['Extra']:
                # for case that need to check kernel version, and all major, minro, extra version are the same
                # then needs to check if the rc version is the same too
                nextRc = nextVersionDict.get('rc')
                baseRc = baseVersionDict.get('rc')
                if (nextRc is None and baseRc is None) \
                    or (nextRc is not None and baseRc is not None and nextRc == baseRc):
                    # for case that neither build is rc version or both have the same rc version
                    logger.info('Skip the check as it has the same version for %s %s', project_name, build_version)
                    continue
                else:
                    # for case that the rc versions are different, like
                    # 1. one build is rc version, but another is not rc version
                    # 2. both are rc versions, but are different rc versions
                    pass
            else:
                # for cases that
                # 1. the second build version not specified, or this build has the same version like the specified second build version
                # 2. no need to check the kernel version, or the kernel version is different, either extra version or the rc version
                pass

        logger.info("Checking for %s, %s", project_name, build_version)

        jobs = build.testjobs().values()
        set_job_name_for_unsubmitted_jobs(jobs)
        classified_information = get_classified_jobs(jobs=jobs, environment=environment)
        jobs_to_be_checked = classified_information.get('final_jobs')
        if not jobs:
            logger.info('Skip the check as the build has no job found: %s %s', project_name, build_version)
            continue
        assign_testrun_for_testjobs(jobs_to_be_checked)
        build.jobs_to_be_checked = jobs_to_be_checked
        build_status = get_lkft_build_status(build, jobs_to_be_checked)
        if build_status['has_unsubmitted']:
            logger.info('Skip the check as the build has unsubmitted jobs: %s %s', project_name, build_version)
            continue
        elif build_status['is_inprogress']:
            logger.info('Skip the check as the build is still inprogress: %s %s', project_name, build_version)
            continue

        if environment is not None:
            # namedtuples are immutable, just like standard tuples
            # the following two lines do not work
            # build._replace(metadata=classified_information.get('environment_metadata'))
            # build.metadata = classified_information.get('environment_metadata')
            build.environment = environment
            build.environment_metadata = get_build_environment_metadata(build, jobs_to_be_checked)

        else:
            # to avoid AttributeError: 'Build' object has no attribute 'environment_metadata'
            build.environment = None
            build.environment_metadata = None

        download_attachments_save_result(jobs=jobs_to_be_checked)

        build.failures = []
        #pdb.set_trace()
        total_jobs_finished_number = 0
        build.numbers = TestNumbers()
        # build.testcases = []
        build.modules = []
        has_cts_vts_job = False
        for job in jobs_to_be_checked:
            if is_cts_vts_job(job.name):
                has_cts_vts_job = True
            jobstatus = job.job_status
            jobfailure = job.failure
            # for some failed cases, numbers are not set for the job
            job_numbers = getattr(job, 'numbers', TestNumbers())
            if is_cts_vts_job(job.name) and jobstatus == 'Complete' and jobfailure is None and \
                    (job_numbers is not None and job_numbers.modules_total !=0) and \
                    (job_numbers.modules_total <= NUMBER_MODULES_TOTAL_CHECK or (job_numbers.modules_done / job_numbers.modules_total > PERCENTAGE_MODULES_NUMBER_CHECK)):
                total_jobs_finished_number = total_jobs_finished_number + 1
            elif jobstatus == 'Complete' and jobfailure is None and not is_cts_vts_job(job.name):
                # for non-cts vts jobs, assuming it's a finished successful job
                total_jobs_finished_number = total_jobs_finished_number + 1
            elif (jobstatus is None or jobstatus == "Submitted" or jobstatus == "Running" or job.job_status == "Fetching") and not is_cts_vts_job(job.name):
                ## TODO: HACK: STG-5142 for boot job
                total_jobs_finished_number = total_jobs_finished_number + 1

            if is_cts_vts_job(job.name):
                result_file_path = get_result_file_path(job=job)
                if not result_file_path or not os.path.exists(result_file_path):
                    continue


            # build.testcases.extend(job.testcases)
            build.modules.extend(getattr(job, 'modules', []))
            build.failures.extend(getattr(job, 'failures', []))
            build.numbers.addWithTestNumbers(job_numbers)

        # now let's see what we have. Do we have a complete yet?
        print("Total Finished Jobs Number / Total Jobs Number: %d / %d" % (total_jobs_finished_number, len(jobs_to_be_checked)))

        # when the finished successfully jobs number is the same as the number of all jobs
        # it means all the jobs are finished successfully, the build is OK to be used for comparisonfin
        if total_jobs_finished_number > 0 and len(jobs_to_be_checked) == total_jobs_finished_number \
                and ((build.numbers.modules_total > 0 and has_cts_vts_job) or not has_cts_vts_job):
            #pdb.set_trace()
            if nextVersionDict is not None:
                # add for the second build
                if exact_ver2 is not None:
                    if reverse_build_order:
                        # find regression in exact_ver2 compare to exact_ver1
                        goodruns.append(build)
                    else:
                        # find regression in exact_ver1 compare to exact_ver2
                        goodruns.insert(0, build)

                elif int(nextVersionDict['Extra']) > int(baseVersionDict['Extra']):
                    # for the case the second build is newer than the first build
                    # first build is goodruns[0], second build is goodruns[1]
                    # normally, the builds are sorted with the newest kernel version as the first build.
                    goodruns.append(build)
                else:
                    # for the case the second build is older than or equal to the first build
                    goodruns.insert(0, build)
            else:
                # add for the first build
                goodruns.append(build)

            bailaftertwo += 1
            logger.info("found one valid build bailaftertwo=%s %s, %s", bailaftertwo, project_name, build_version)
        elif bailaftertwo == 0 and exact_ver1 is not None and baseVersionDict is not None and baseVersionDict.get('versionString') == exact_ver1:
            # found the first build with exact_ver1, but that build does not have all jobs finished successfully
            # stop the loop for builds to find anymore
            bailaftertwo += 1

            goodruns.append(build)
            logger.info("The build specified with --exact-version-1 is not a valid build: %s, %s", project_name, build_version)
            return goodruns
        elif bailaftertwo == 1 and exact_ver2 is not None and nextVersionDict is not None and nextVersionDict.get('versionString') == exact_ver2:
            # found the second build with exact_ver2, but that build does not have all jobs finished successfully
            # stop the loop for builds to find anymore
            bailaftertwo += 1
            logger.info("The build specified with --exact-version-2 is not a valid build: %s, %s", project_name, build_version)
            return goodruns
        else:
            # for case that no completed build found, continute to check the next build
            logger.info("Not one valid will continue: %s, %s", project_name, build_version)
            continue

        failures_list = []
        def get_module_name_test_name_abi(testcase):
            return '%s.%s' % (testcase.get('testsuite').get('name'), testcase.get('test_name'))

        sorted_build_failures = sorted(build.failures, key=get_module_name_test_name_abi)
        build.failures = sorted_build_failures

    return goodruns


# Try to find the regressions in goodruns[1]
# compared to the result in goodruns[0]
def find_regressions(goodruns):
    runA = goodruns[1]
    failuresA = runA.failures
    runB = goodruns[0]
    failuresB = runB.failures
    regressions = []
    for failureA in failuresA:
        match = 0
        testAname = failureA['test_name']
        for failureB in failuresB:
            testBname = failureB['test_name']
            if testAname == testBname:
                match = 1
                break
        if match != 1 :
            # for failures in goodruns[1],
            # if they are not reported in goodruns[0],
            # then they are regressions
            regressions.append(failureA)
    return regressions


def find_antiregressions(goodruns):
    runA = goodruns[1]
    failuresA = runA.failures
    runB = goodruns[0]
    failuresB = runB.failures
    antiregressions = []
    for failureB in failuresB:
        match = 0
        for failureA in failuresA:
            testAname = failureA['test_name']
            testBname = failureB['test_name']
            if testAname == testBname:
                match = 1
                break
        if match != 1 :
            antiregressions.append(failureB)
    return antiregressions


"""  Example project_info dict
                    {'project_id': 210,
                     'hardware': 'hikey',
                     'OS' : 'Android10',
                     'branch' : 'Android-4.19-q',},
"""

def print_androidresultheader(output, project_info, run, priorrun):
    output.write("    " + project_info['OS'] + "/" + project_info['hardware'] + " - " )
    output.write(f"Current:{run.version}  Prior:{priorrun.version}\n")

    if run.environment_metadata is not None:
        build_metadata = run.environment_metadata
    else:
        build_metadata = run.metadata

    if priorrun.environment_metadata is not None:
        prior_build_metadata = priorrun.environment_metadata
    else:
        prior_build_metadata = priorrun.metadata

    def get_last_of_metadata(metadata):
        if metadata is None:
            return None
        if type(metadata) is str:
            return metadata
        if type(metadata) is list:
            return metadata[-1]

    def print_metadata(data, prior_data):
        if get_last_of_metadata(data) == get_last_of_metadata(prior_data):
            output.write("Current:" + get_last_of_metadata(data) + " == Prior:" + get_last_of_metadata(prior_data) + "\n")
        else:
            output.write("Current:" + get_last_of_metadata(data) + " != Prior:" + get_last_of_metadata(prior_data) + "\n")

    if getattr(build_metadata, 'android.build.gsi.fingerprint', None):
        output.write("    " + "GSI Fingerprint:" + " - " )
        data = getattr(build_metadata, 'android.build.gsi.fingerprint')
        prior_data = getattr(prior_build_metadata, 'android.build.gsi.fingerprint', 'UNKNOWN')
        print_metadata(data, prior_data)

    if getattr(build_metadata, 'android.build.vendor.fingerprint', None):
        output.write("    " + "Vendor Fingerprint:" + " - " )
        data = getattr(build_metadata, 'android.build.vendor.fingerprint')
        prior_data = getattr(prior_build_metadata, 'android.build.vendor.fingerprint', 'UNKNOWN')
        print_metadata(data, prior_data)


    if getattr(build_metadata,'cts_version', None):
        output.write("    " + "CTS Version:" + " - " )
        data = getattr(build_metadata, 'cts_version')
        prior_data = getattr(prior_build_metadata, 'cts_version', 'UNKNOWN')
        print_metadata(data, prior_data)

    # there is the case like presubmit jobs that there are not vts is run
    if getattr(build_metadata, 'vts_version', None):
        output.write("    " + "VTS Version:" + " - " )
        data = getattr(build_metadata, 'vts_version')
        prior_data = getattr(prior_build_metadata, 'vts_version', 'UNKNOWN')
        print_metadata(data, prior_data)


def add_unique_kernel(unique_kernels, kernel_version, project, unique_kernel_info):
    #pdb.set_trace()
    if kernel_version not in unique_kernels:
        unique_kernels.append(kernel_version)
        project_list = []
        project_list.append(project)
        unique_kernel_info[kernel_version] = project_list
    else:
        project_list= unique_kernel_info[kernel_version]
        project_list.append(project)


def report_kernels_in_report(f_outputfile, unique_kernels, unique_kernel_info, work_total_numbers):
    f_outputfile.write("\n")
    f_outputfile.write("\n")
    f_outputfile.write("Kernel/OS Combo(s) in this report:\n")
    for kernel in unique_kernels:
        project_list = unique_kernel_info[kernel]
        project_alias_names = [ project.project_alias.get('alias_name') for project in project_list ]
        f_outputfile.write("    %s - %s\n" % (kernel, ', '.join(project_alias_names)))

    f_outputfile.write("\n")
    f_outputfile.write("%d Prior Failures now pass\n" % work_total_numbers.number_antiregressions)
    f_outputfile.write("%d Regressions of %d Failures, %d Passed, %d Ignored, %d Assumption Failures, %d Total\n" % (
                            work_total_numbers.number_regressions,
                            work_total_numbers.number_failed,
                            work_total_numbers.number_passed,
                            work_total_numbers.number_ignored,
                            work_total_numbers.number_assumption_failure,
                            work_total_numbers.number_total))


def report_results(output, run, regressions, project_alias, priorrun, antiregressions, trim_number=0):
    #pdb.set_trace()
    regressions_failure = [ failure for failure in regressions if failure.get('result') == 'fail' ]
    regressions_assumption = [ failure for failure in regressions if failure.get('result') == 'ASSUMPTION_FAILURE' ]

    numbers = run.numbers
    project_info = project_alias
    output.write(project_info['branch'] + "\n")
    print_androidresultheader(output, project_info, run, priorrun)
    #pdb.set_trace()
    if numbers.modules_total == 0:
        logger.info("Skip printing the regressions information as this build might not have any cts vts jobs run")
        output.write("\n")
        return
    output.write("    " + str(len(antiregressions)) + " Prior Failures now pass\n")
    output.write("    " + str(len(regressions_failure)) + " Regressions of ")
    output.write(str(numbers.number_failed) + " Failures, ")
    output.write(str(numbers.number_passed) + " Passed, ")
    if numbers.number_ignored > 0 :
        output.write(str(numbers.number_ignored) + " Ignored, ")
    if numbers.number_assumption_failure > 0 :
        output.write(str(numbers.number_assumption_failure) + " Assumption Failures, ")
    output.write(str(numbers.number_total) + " Total\n" )
    output.write("    " + "Modules Run: " + str(numbers.modules_done) + " Module Total: " + str(numbers.modules_total) + "\n")

    def print_regressions(output, target_regressions, trim_number=0):
        if trim_number > 0 and len(target_regressions) > trim_number:
            print_number=int(trim_number/2)
            for regression in target_regressions[:print_number]:
                # pdb.set_trace()
                output.write("        " + regression['testtype'] + " " + regression['testsuite']['name'] + "."  + regression['test_name'] + "\n")
            output.write("    [...%d lines removed...]\n" % (len(target_regressions) - print_number * 2))
            for regression in target_regressions[-print_number:]:
                # pdb.set_trace()
                output.write("        " + regression['testtype']  + " " + regression['testsuite']['name'] + "." + regression['test_name'] + "\n")
        else:
            for regression in target_regressions:
                # pdb.set_trace()
                output.write("        " + regression['testtype']  + " " + regression['testsuite']['name'] + "." + regression['test_name'] + "\n")

    if len(regressions_failure) > 0:
        output.write("    " + str(len(regressions_failure)) + " Test Regressions:\n")
        print_regressions(output, regressions_failure, trim_number=trim_number)
    if len(regressions_assumption) > 0:
        output.write("    " + str(len(regressions_assumption)) + " New Assumption Failures:\n")
        print_regressions(output, regressions_assumption, trim_number=trim_number)

    if len(regressions) > 0:
        output.write("    " + "Current jobs\n")
        for job in run.jobs_to_be_checked:
            output.write("        " + "%s %s\n" % (job.external_url, job.name))
        output.write("    " + "Prior jobs\n")
        for job in priorrun.jobs_to_be_checked:
            output.write("        " + "%s %s\n" % (job.external_url, job.name))

    output.write("\n")


# a flake entry
# name, state, bugzilla
def process_flakey_file(path_flakefile):
    Dict414 = {'version' : 4.14, 'flakelist' : [] }
    Dict419 = {'version' : 4.19, 'flakelist' : [] }
    Dict54 = {'version' : 5.4, 'flakelist' : [] }
    Dict510 = {'version' : 5.10, 'flakelist' : [] }
    Dict515 = {'version' : 5.15, 'flakelist' : [] }
    Dict61 = {'version' : 6.1, 'flakelist' : [] }
    flakeDicts = [Dict414, Dict419, Dict54, Dict510, Dict515, Dict61]

    kernelsmatch = re.compile('[0-9]+.[0-9]+')
    androidmatch = re.compile('ANDROID[0-9]+|AOSP')
    hardwarematch = re.compile('HiKey|db845|HiKey960|rb5')
    allmatch = re.compile('ALL')
    #pdb.set_trace()

    f_flakefile = open(path_flakefile, "r")
    Lines = f_flakefile.readlines()
    f_flakefile.close()
    for Line in Lines:
        newstate = ' '
        if Line[0] == '#':
            continue
        if Line[0] == 'I' or Line[0] == 'F' or Line[0] == 'B' or Line[0] == 'E':
            newstate = Line[0]
            Line = Line[2:]
        m = Line.find(' ')
        if m:
            testname = Line[0:m]
            Line = Line[m:]
            testentry = {'name' : testname, 'state': newstate, 'board': [], 'androidrel':[] }
            if Line[0:4] == ' ALL':
               Line = Line[5:]
               Dict414['flakelist'].append(testentry)
               Dict419['flakelist'].append(testentry)
               Dict54['flakelist'].append(testentry)
               Dict510['flakelist'].append(testentry)
               Dict515['flakelist'].append(testentry)
               Dict61['flakelist'].append(testentry)
            else:
               n = kernelsmatch.match(Line)
               if n:
                  Line = Line[n.end():]
                  for kernel in n:
                      for Dict in flakeDicts:
                          if kernel == Dict['version']:
                              Dict['flakelist'].append(testentry)
               else:
                   continue
            if Line[0:3] == 'ALL':
               Line = Line[4:]
               testentry['board'].append("HiKey")
               testentry['board'].append("HiKey960")
               testentry['board'].append("db845c")
               testentry['board'].append("rb5")
            else:
               h = hardwarematch.findall(Line)
               if h:
                  for board in h:
                      testentry['board'].append(board)
               else:
                   continue
            a = allmatch.search(Line)
            if a:
               testentry['androidrel'].append('Android11') #R
               testentry['androidrel'].append('Android12') #S
               testentry['androidrel'].append('Android13') #T
               testentry['androidrel'].append('Android14') #U
               testentry['androidrel'].append('Android15') #V
               testentry['androidrel'].append('AOSP')
            else:
               a = androidmatch.findall(Line)
               if a:
                  for android in a:
                      testentry['androidrel'].append(android)
               else:
                   continue
        else:
            continue

    return flakeDicts

# take the data dictionaries, the testcase name and ideally the list of failures
# and determine how to classify a test case. This might be a little slow espeically
# once linked into bugzilla
def classifyTest(flakeDicts, testcasename, hardware, kernel, android):
    for dict in flakeDicts:
        if dict['version'] == kernel:
            break
    #pdb.set_trace()
    foundboard = 0
    foundandroid = 0
    #if testcasename == 'VtsKernelLinuxKselftest.timers_set-timer-lat_64bit':
    #    pdb.set_trace()
    #if testcasename == 'android.webkit.cts.WebChromeClientTest#testOnJsBeforeUnloadIsCalled#arm64-v8a':
    #    pdb.set_trace()
    for flake in dict['flakelist']:
        if flake['name'] == testcasename:
            for board in flake['board'] :
                if board == hardware:
                    foundboard = 1
                    break
            for rel in flake['androidrel']:
                if rel == android:
                    foundandroid = 1
                    break
            if foundboard == 1 and foundandroid == 1:
                return flake['state']
            else:
                return 'I'
    return 'I'


def set_regressions_testtype(regressions, flakes, project_info):
    if 'baseOS' in project_info:
        OS = project_info['baseOS']
    else:
        OS = project_info['OS']

    for regression in regressions:
        testtype = classifyTest(flakes, regression['test_name'], project_info['hardware'], project_info['kern'], OS)
        regression['testtype'] = testtype


def do_boilerplate(output):
    output.write("\n\nFailure Key:\n")
    output.write("--------------------\n")
    output.write("I == Investigation\nB == Bug#, link to bugzilla\nF == Flakey\nE == Expected Fails\n\n")


def reset_qajob_failure_msg(job_failure_message):
    if type(job_failure_message) == str:
        failure_str = job_failure_message
        new_str = failure_str.replace('"', '\\"').replace('\'', '"')
        try:
            failure_dict = json.loads(new_str)
        except ValueError:
            # {'case': 'job', 'definition': 'lava', 'error_msg': "Command '['docker', 'pull', 'linaro/lava-android-test:latest']' returned non-zero exit status 1.", 'error_type': 'Bug', 'result': 'fail'}
            error_msg_pattern = re.compile(".*\"error_msg\":\s+\\\\\\\"(?P<error_msg>.+)\\\\\\\",.*")
            error_msg_match = error_msg_pattern.match(new_str)
            if error_msg_match:
                error_msg = error_msg_match.group('error_msg')
                failure_dict = {'error_msg': error_msg}
            else:
                failure_dict = {'error_msg': new_str}
        return failure_dict
    elif type(job_failure_message) == dict:
        # already reset
        return job_failure_message
    elif job_failure_message is not None:
        # not sure what it is
        return {'error_msg': str(job_failure_message)}
    else:
        return {'error_msg': ''}

def check_console_logs(log_url, stacks=False):
    critical_error_pats = re.compile(".*(?P<critical_error_msg>("
                        "\] disagrees about version of symbol"
                        "|\] init: Failed to insmod"
                        "|\] init: Failed to load kernel modules"
                        # Kernel panic - not syncing: Asynchronous SError Interrupt
                        # Kernel panic - not syncing: Attempted to kill init! exitcode=0x0000000b
                        # Kernel panic - not syncing: Fatal exception
                        "|\] Kernel panic - not syncing:"
                        #"|\] Unable to handle kernel write to read-only memory at virtual address" # just a problem to cause panic
                        #"|\] Unable to handle kernel NULL pointer dereference at virtual address" # just a problem to cause panic
                        "|\] Unable to handle kernel" # just a problem to cause panic
                        "|\] Internal error: synchronous external abort:"
                        # messages that cause reboot
                        "|\] init: Reboot start, reason:"
                        "|\] init: Reboot ending, jumping to kernel"
                        # 01-01 00:07:55.768 11387 11387 F DEBUG   : Abort message: 'no suitable EGLConfig found, giving up'
                        # [   15.711683][  T356] DEBUG: Abort message: 'Failed to open database.: Failed to attach database persistent.
                        "|Abort message:"
                        "|fastboot: error:"
                        ").*)$")

    # resp = requests.get(log_url, stream=True)
    resp = requests.get(log_url, stream=True)
    log_lines = []
    for line in resp.iter_lines():
        if line: log_lines.append(line)
    log_lines.reverse()

    critical_error = None
    critical_stacks = []
    for line in log_lines:
        match = critical_error_pats.match(line.decode("utf-8") )
        if match:
            critical_error = match.group('critical_error_msg')
            if critical_error and critical_error.startswith("] "):
                critical_error = critical_error.replace("] ", "")

            if stacks:
                critical_stacks.append(critical_error)
            else:
                break

    if stacks:
        return critical_stacks
    else:
        return critical_error

infra_stg_map = {
    ## All the message here should be in the LAVA Log,
    ## Otherwise it won't be found

    # failed to download android-cts.zip or android-vts.zip
    "ERROR 403: Forbidden": "STG-4934",
    #   HTTP/1.1 504 Gateway Time-out
    #   Server: nginx/1.18.0
    #   Date: Tue, 13 Jun 2023 19:22:39 GMT
    #   Content-Type: text/html
    #   Content-Length: 167
    #   Connection: keep-alive
    # Giving up.
    "HTTP/1.1 504 Gateway Time-out": None,
    "HTTP/1.1 504 Gateway Timeout": None,
    "Giving up.": "STG-4833",
    "Could not resolve host: qa-reports.linaro.org": None,
    "502 Bad Gateway": None,
    "502 Proxy Error": None,
    #"curl: \\(22\\) The requested URL returned error": None,
    #"curl: \\(28\\) Failed to connect to android-git.linaro.org port": None,
    #"curl: \\(56\\) Recv failure: Connection reset by peer", None,
    "curl: \\(\\d+\\) ": None,
    # failed: Temporary failure in name resolution.
    # wget: unable to resolve host address 'testdata.linaro.org'
    "failed: Temporary failure in name resolution": "STG-4773",
    "wget: unable to resolve host address": "STG-4773",
    # Connecting to lab-cache.lavalab (lab-cache.lavalab)|10.0.0.25|:80... failed: No route to host.
    "failed: No route to host": "STG-4773",
    # Sending 'boot.img' (18104 KB)                      FAILED (Write to device failed in SendBuffer() (Protocol error))
    # fastboot: error: Command failed
    "fastboot: error: Command failed": "STG-5007",
    "fastboot timing out, power-off the DuT": None,
    "Error response from daemon: cannot stop container": None,
    # test-attachment fail
    # Expected the basename of the attachment file (\"tradefed-output-20230908030952.tar.xz\") to be part of the Artifactorial URL, but curl returned \"http://archive.validation.linaro.org/artifacts/team/qa/2023/09/08/03/10/tradefed-output-20230908030952.tar_Ku9g2Af.xz\".
    "Expected one SQUAD testrun id returend, but curl returned": None,
    "Expected the basename of the attachment file": "STG-4903",
    #"Error, should not happen! Check your USB connection": "STG-4427",
}
def check_console_logs_for_infra_errors(log_url):
    infra_error_pats = re.compile(".*(?P<infra_error_msg>(" + '|'.join(infra_stg_map.keys()) + ").*)$")
    # resp = requests.get(log_url, stream=True)
    resp = requests.get(log_url, stream=True)
    log_lines = []
    for line in resp.iter_lines():
        if line: log_lines.append(line)
    log_lines.reverse()

    infra_error = None
    stg_card = None
    for line in log_lines:
        match = infra_error_pats.match(line.decode("utf-8") )
        if match:
            infra_error = match.group("infra_error_msg")
            for pat in infra_stg_map.keys():
                if infra_error.find(pat) >= 0:
                    stg_card = infra_stg_map.get(pat)
            break
    return (infra_error, stg_card)

def print_error_project(output, error_project):
    output_errorprojects = output
    target_squad_project = error_project.get('project')
    project_alias = target_squad_project.project_alias
    project_name = target_squad_project.full_name
    goodruns = error_project.get('goodruns')
    args = error_project.get('args')
    opt_exact_ver1 = args.build

    if opt_exact_ver1 is not None:
        output_errorprojects.write("NOTE: project " + target_squad_project.name + " did not have results for %s\n" % (opt_exact_ver1))
    elif args.report_kerne:
        output_errorprojects.write("NOTE: project " + target_squad_project.name  + " did not have results for %s\n" % (args.report_kerne))
    else:
        output_errorprojects.write("NOTE: project " + target_squad_project.name   + " did not have results to report\n")

    if len(goodruns) == 1:
        # assuming that it's the latest build a invalid build
        # and that caused only one goodruns returned.
        # if the first latest build is a valid build,
        # then the second build should be alwasy there
        run = goodruns[0]
        output_errorprojects.write(project_alias['branch'] + "\n")
        output_errorprojects.write("    " + project_alias['OS'] + "/" + project_alias['hardware'] + " - " + "Current:" + run.version + "\n")
        output_errorprojects.write("    Current jobs\n")

        for job in run.jobs_to_be_checked:
            job_numbers = getattr(job, 'numbers', TestNumbers())
            if job_numbers.modules_total > NUMBER_MODULES_TOTAL_CHECK and (job_numbers.modules_done / job_numbers.modules_total <= PERCENTAGE_MODULES_NUMBER_CHECK):
                output_errorprojects.write(f"        {job.external_url} {job.name} {job.environment} {job.job_status} {job_numbers.modules_done}/{job_numbers.modules_total}\n")
            elif job_numbers.modules_total == 0 and is_cts_vts_job(job.name):
                output_errorprojects.write(f"        {job.external_url} {job.name} {job.environment} {job.job_status} {job_numbers.modules_done}/{job_numbers.modules_total}\n")
            else:
                output_errorprojects.write(f"        {job.external_url} {job.name} {job.environment} {job.job_status}\n")
            #log_url = job.testrun_obj.log_file
            # change back to use the android.l.o log url
            # as there will be no log url on squad if no result generated
            # like the case that the cts/vts packages failed to be downloaded
            job_squad_url = job.url.strip('/')
            job_squad_id = job_squad_url.split('/')[-1]
            log_url = f"https://android.linaro.org/lkft/lavalog/{job_squad_id}/"

            if job.failure: # and job.failure.get('error_msg'):
                failure_mesg = reset_qajob_failure_msg(job.failure)
                failure_error_type = failure_mesg.get('error_type', 'UNKNOWN')
                failure_error_msg = failure_mesg.get('error_msg', 'UNKNOWN')
                if failure_error_type == "Infrastructure":
                    failure_error_msg = f"Infrastructure error: {failure_error_msg}"

                # Infrastructure error: Unable to reboot: '/usr/local/lab-scripts/snmp_pdu_control --hostname pdu04 --command reboot --port 17' failed
                # Command "["docker", "pull", "linaro/lava-android-test:latest"]" returned non-zero exit status 1.
                pat_no_attachment_url = 'No attachment found for the job from the squad side'
                pat_pdu_reboot_single_quote = "Unable to reboot: '/usr/local/lab-scripts/snmp_pdu_control"
                pat_pdu_reboot_double_quote = "Unable to reboot: \"/usr/local/lab-scripts/snmp_pdu_control"
                pat_docker_pull = '["docker", "pull", "linaro/lava-android-test:latest"]'
                pat_stg_map = {
                                ## The failures here include the failures reported by SQUAD
                                ## Which might not be in the lava logs.
                                ## the message here should not be listed in infra_stg_map
                                ## and the message here will be printed first

                                pat_no_attachment_url:  "STG-5005",
                                pat_pdu_reboot_single_quote: "STG-4699",
                                pat_pdu_reboot_double_quote: "STG-4699",
                                "Unable to power-off: \"/usr/local/lab-scripts/snmp_pdu_control": None,
                                pat_docker_pull: "STG-4701",
                                "Connection closed": "STG-5035",
                                'Unable to fetch git repository "https://github.com/Linaro/test-definitions.git"': "STG-5036",
                                'Could not terminate the child': "STG-5054",
                                "job-cleanup timed out after": None,
                                'Infrastructure error:': None,
                                "There is already a test run with job_id": None,
                                "Max retries exceeded with url: /RPC2": None,
                                "Connection aborted.": None,
                                "lava-create-overlay timed out after": None,
                              }
                infra_patterns = pat_stg_map.keys()
                failure_msg_match_infra_patterns = False
                failure_msg_matched_infra_pat = None
                pat_stg_card = None
                for infra_pat in infra_patterns:
                    if failure_error_msg.find(infra_pat) >= 0:
                        failure_msg_match_infra_patterns = True
                        failure_msg_matched_infra_pat = infra_pat
                        pat_stg_card = pat_stg_map.get(infra_pat)

                if failure_msg_match_infra_patterns:
                    if job.job_status == "Complete" and (pat_stg_card is None or pat_stg_card != "STG-5005"):
                        # for cases like no attachment url,
                        # where the job status is complete, but the failure message is set by the report tool
                        output_errorprojects.write("            " + f"INFRA ERROR({pat_stg_card}: {failure_error_msg}: {log_url})\n")
                    else:
                        # print the job failure message first
                        if pat_stg_card is not None:# and pat_stg_card == "STG-5005":
                            output_errorprojects.write("            " + f"INFRA ERROR({pat_stg_card}: {failure_error_msg}: {log_url})\n")
                        else:
                            output_errorprojects.write("            " + f"INFRA ERROR({failure_error_msg}: {log_url})\n")

                        # print the message if some patterns are found from the lava log
                        infra_error, stg_card = check_console_logs_for_infra_errors(log_url)
                        if infra_error is not None and stg_card is not None:
                            output_errorprojects.write("            " + f"INFRA ERROR({stg_card}: {infra_error}): {log_url}\n")
                        elif infra_error is not None:
                            output_errorprojects.write("            " + f"INFRA ERROR({infra_error}): {log_url}\n")
                        else:
                            # no pattern found from the log, nothing printed here.
                            pass
                else:
                    output_errorprojects.write("            " + f"{failure_error_msg}\n")
                    critical_error = check_console_logs(log_url)
                    if critical_error is not None:
                        output_errorprojects.write("            " + f"CRITICAL ERROR({critical_error}): {log_url}\n")
                        critical_stacks = check_console_logs(log_url, stacks=True)
                        critical_stacks.reverse()
                        for stack in critical_stacks:
                            output_errorprojects.write("                " + f"{stack}\n")
                    else:
                        infra_error, stg_card = check_console_logs_for_infra_errors(log_url)
                        if infra_error is not None and stg_card is not None:
                            output_errorprojects.write("            " + f"INFRA ERROR({stg_card}: {infra_error}): {log_url}\n")
                        elif infra_error is not None:
                            output_errorprojects.write("            " + f"INFRA ERROR({infra_error}): {log_url}\n")
            elif job.job_status is None or job.job_status == "Submitted" or job.job_status == "Running" or job.job_status == "Fetching":
                #### TODO ###
                ## NEED TO THINK ABOUT HOW TO DEAL WITH SUCH CASES
                ## STG-5142 is a temporary STG card, it may not cover all cases here
                ## Another case is STG-5005
                squad_build_url = f"https://qa-reports.linaro.org/{project_name}/build/{run.version}/testjobs/"
                output_errorprojects.write("            " + f"INFRA ERROR(STG-5142: Jobs status seems not correct, please check): {squad_build_url}\n")
            elif is_cts_vts_job(job.name) and (job_numbers is None or job_numbers.modules_total == 0):
                infra_error, stg_card = check_console_logs_for_infra_errors(log_url)
                if infra_error is not None and stg_card is not None:
                    output_errorprojects.write("            " + f"INFRA ERROR({stg_card}: {infra_error}): {log_url}\n")
                elif infra_error is not None:
                    output_errorprojects.write("            " + f"INFRA ERROR({infra_error}): {log_url}\n")

        project_bugs_opened = get_project_open_bugs(target_squad_project)
        if len(project_bugs_opened) > 0:
            output_errorprojects.write("    Known bugs:\n")
            for bug in project_bugs_opened:
                last_change_days= (datetime_now - get_aware_datetime_from_str(bug.last_change_time)).days
                output_errorprojects.write(tab_spaces * 2 + f"{bug.id} {bug.status} {bug.assigned_to} (\"{bug.summary}\") Last updated {last_change_days} days ago\n")
        output_errorprojects.write("    Want to resubmit the failed jobs for a try? https://android.linaro.org/lkft/jobs/?build_id=%s&fetch_latest=true\n" %  run.id)
    elif len(goodruns) == 0 and opt_exact_ver1 is not None:
        output_errorprojects.write(project_alias['branch'] + "\n")
        output_errorprojects.write("    " + project_alias['OS'] + "/" + project_alias['hardware'] + " - build for kernel version " + opt_exact_ver1 + " is not found or still in progress!"+ "\n")
        output_errorprojects.write("    Builds list: https://android.linaro.org/lkft/builds/?project_id=%s&fetch_latest=true\n" %  target_squad_project.id)
    elif len(goodruns) == 0:
        output_errorprojects.write(project_alias['branch'] + "\n")
        if args.report_kerne:
            output_errorprojects.write("    " + project_alias['OS'] + "/" + project_alias['hardware'] + " - no build available for " + args.report_kerne + "!\n")
        else:
            output_errorprojects.write("    " + project_alias['OS'] + "/" + project_alias['hardware'] + " - no build available to report for %s!\n" %(project_name))

    output_errorprojects.write("\n")


def print_success_project(output, success_project):
    target_squad_project = success_project.get('project')
    goodruns = success_project.get('goodruns')
    regressions = success_project.get('regressions')
    antiregressions = success_project.get('antiregressions')
    trim_number = success_project.get('trim_number')

    report_results(output, goodruns[1], regressions, target_squad_project.project_alias, goodruns[0], antiregressions, trim_number=trim_number)

def get_project_open_bugs(target_squad_project):
    project_bugs = bugzilla_instance.get_bugs_for_project(project_name=target_squad_project.name, cachepool=project_platform_bugs)
    project_bugs_opened = [ bug for bug in project_bugs if (bug.status != "RESOLVED" and bug.status != "VERIFIED" ) ]
    return project_bugs_opened

def print_regression_project(output, regression_project, number_regression_modules=5, number_testcases_per_regression_modules=3):
    output_errorprojects = output
    target_squad_project = regression_project.get('project')
    project_alias = target_squad_project.project_alias
    project_trimmed_lines = regression_project.get('project_trimmed_lines')
    goodruns = regression_project.get('goodruns')

    output_errorprojects.write("\nNOTE: %d lines were removed for %s\n" % (project_trimmed_lines, target_squad_project.name))
    run = goodruns[1]
    output_errorprojects.write(project_alias['branch'] + "\n")
    output_errorprojects.write("    " + project_alias['OS'] + "/" + project_alias['hardware'] + " - " + "Current:" + run.version + "\n")

    for module in run.modules:
        module_name = module.get("name")
        module_abi = module.get("abi")
        failures = [ failure for failure in module.get("failures") if failure.get('result') == 'fail' ]
        assumptions = [ failure for failure in module.get("failures") if failure.get('result') == 'ASSUMPTION_FAILURE' ]
        module["number_regressions"] = 0
        for prior_module in goodruns[0].modules:
            if module_name == prior_module.get('name') and module_abi == prior_module.get("abi"):
                module['prior'] = prior_module
                prior_failures = [ failure for failure in prior_module.get("failures") if failure.get('result') == 'fail' ]
                prior_assumptions = [ failure for failure in prior_module.get("failures") if failure.get('result') == 'ASSUMPTION_FAILURE' ]
                def classify_failures(current_failures, prior_failures):
                    #failures_new = [] ## test cases new and failed
                    failures_new_or_regressions = [] ##  test cases new and failed, or test cases failed this build but pass prior build, regression
                    failures_reproduced = []  ## test cases failed both this build and the prior build, reproduced
                    for current_failure in current_failures:
                        failure_found = False
                        for prior_failure in prior_failures:
                            if prior_failure.get("test_class") == current_failure.get("test_class") \
                                    and prior_failure.get("test_method") == current_failure.get("test_method"):
                                failures_reproduced.append(current_failure)
                                break
                        else:
                            failures_new_or_regressions.append(current_failure)
                    return {
                            'failures_new_or_regressions': failures_new_or_regressions,
                            'failures_reproduced': failures_reproduced,
                          }

                classified_failures = classify_failures(failures, prior_failures)
                module['classified_failures'] = classified_failures
                module['number_regressions'] = len(classified_failures.get("failures_new_or_regressions", []))

                classified_assumptions = classify_failures(assumptions, prior_assumptions)
                module['classified_assumptions'] = classified_assumptions
                module['number_assumptions'] = len(classified_assumptions.get("failures_new_or_regressions", []))
                break
        else:
            module["number_regressions"] = len(failures)
            module['classified_failures'] = {
                                                'failures_new_or_regressions': failures,
                                                'failures_reproduced': [],
                                            }
            module["number_assumptions"] = len(assumptions)
            module['classified_assumptions'] = {
                                                'failures_new_or_regressions': assumptions,
                                                'failures_reproduced': [],
                                            }

    sorted_modules = sorted(run.modules, key=lambda module: module.get('number_regressions'), reverse=True)
    sorted_modules_based_on_failures = []
    for module in sorted_modules[:number_regression_modules]:
        sorted_modules_based_on_failures.append({
            'failure_type': 'FAIL',
            'number_regressions': module.get('number_regressions'),
            'module': module,
            })

    sorted_modules = sorted(run.modules, key=lambda module: module.get('number_assumptions'), reverse=True)
    sorted_modules_based_on_assumptions = []
    for module in sorted_modules[:number_regression_modules]:
        sorted_modules_based_on_assumptions.append({
            'failure_type': "ASSUMPTION",
            'number_assumptions': module.get('number_assumptions'),
            'module': module,
            })

    def get_module_regressions_number(module_wrapper):
        if module_wrapper.get("failure_type") == "ASSUMPTION":
            return module_wrapper.get("number_assumptions")
        else:
            return module_wrapper.get("number_regressions")
    sorted_module_wrappers_mixed = sorted(sorted_modules_based_on_failures + sorted_modules_based_on_assumptions,
                                    key=get_module_regressions_number,
                                    reverse=True)

    def print_regressions_module_testcases(module_wrappers=[], number_testcases_per_regression_modules=3, job_attachments=[]):
        for module_wrapper in module_wrappers:
            module = module_wrapper.get('module')
            module_failure_type = module_wrapper.get("failure_type")

            module_name = module.get("name")
            module_abi = module.get("abi")
            job = module.get("job")

            if module_failure_type == "FAIL":
                failure_type_index = "classified_failures"
            elif module_failure_type == "ASSUMPTION":
                failure_type_index = "classified_assumptions"
            else:
                failure_type_index = 'unknown'

            module_regressions = module.get(failure_type_index, {}).get("failures_new_or_regressions", [])
            number_regressions = len(module_regressions)
            if number_regressions <= 0:
                # No need to print no regressions module here.
                # And the modules have been sorted by the regression numbers
                break

            if job.attachment_url:
                job_attachments[job.name] = job.attachment_url
            output_errorprojects.write(tab_spaces * 2 + f"{number_regressions:4} {module_failure_type} {module_name}#{module_abi} {job.external_url} {job.name}\n")
            for m_regression in module_regressions[:number_testcases_per_regression_modules]:
                test_class = m_regression.get("test_class")
                test_method = m_regression.get("test_method")
                test_message = m_regression.get("message")
                if test_message is not None:
                    output_errorprojects.write(tab_spaces * 3 + f"{test_class}#{test_method} {test_message}\n")
                else:
                    output_errorprojects.write(tab_spaces * 3 + f"{test_class}#{test_method}\n")

    job_attachments = {}
    output_errorprojects.write(tab_spaces * 1 + f"Regression Number for the maximun {number_regression_modules} modules:\n")
    output_errorprojects.write(tab_spaces * 2 + f"## RegressionNumber NormalFailureOrAssumptionFailure ModuleInfo LAVAJobURL LAVAJobName\n")
    output_errorprojects.write(tab_spaces * 3 + f"## {number_testcases_per_regression_modules} regression test cases with failure message\n")
    print_regressions_module_testcases(module_wrappers=sorted_module_wrappers_mixed[:number_regression_modules],
                                         number_testcases_per_regression_modules=number_testcases_per_regression_modules,
                                         job_attachments=job_attachments)

    if len(job_attachments) > 0:
        output_errorprojects.write(tab_spaces * 1 + f"Job attachments\n")
        job_attachments_names = sorted(job_attachments.keys())
        for job_attachment_name in job_attachments_names:
            job_attachment = job_attachments.get(job_attachment_name)
            output_errorprojects.write(tab_spaces * 2 + f"{job_attachment_name} {job_attachment}\n")

    project_bugs_opened = get_project_open_bugs(target_squad_project)
    if len(project_bugs_opened) > 0:
        output_errorprojects.write("    Known bugs:\n")
        for bug in project_bugs_opened:
            last_change_days= (datetime_now - get_aware_datetime_from_str(bug.last_change_time)).days
            output_errorprojects.write(tab_spaces * 2 + f"{bug.id} {bug.status} {bug.assigned_to} (\"{bug.summary}\") Last updated {last_change_days} days ago\n")

    output_errorprojects.write("    Want to resubmit the failed jobs for a try? https://android.linaro.org/lkft/jobs/?build_id=%s&fetch_latest=true\n" %  run.id)
    output_errorprojects.write("\n")


def android_report(args, squad):
    before_mem = mem_snapshot()
    start = time.time()

    opt_exact_ver1 = args.build
    trim_number = args.trim_number
    path_outputfile = args.output
    path_flakefile = args.flakefile

    # map kernel to all available kernel, board, OS combos that match
    work = []
    unique_kernel_info = { }
    unique_kernels=[]
    work_total_numbers = TestNumbers()

    flakes = process_flakey_file(path_flakefile)

    if args.report_kernel and args.report_kernel.find("benchmarks") > 0:
        all_supported_kernels = get_all_benchmark_report_kernels()
        all_supported_projects = get_all_benchmark_report_projects()
    else:
        all_supported_kernels = get_all_report_kernels()
        all_supported_projects = get_all_report_projects()
    target_squad_projects = []
    if args.group and args.project:
        group = squad.group(args.group)
        squad_project = group.project(args.project)
        squad_project.project_alias = None
        for project_alias_name, project_alias in all_supported_projects.items():
            if (project_alias.get('project_id', None) and int(project_alias.get('project_id')) == squad_project.id) \
                    or (project_alias.get('slug', None) and project_alias.get('group', None) and project_alias.get('group') == args.group and project_alias.get('slug') == args.project):
                project_alias['alias_name'] = project_alias_name
                squad_project.project_alias = project_alias
        if squad_project.project_alias is None:
            print("The project is not supported yet: %s" % squad_project.full_name)
            return
        target_squad_projects.append(squad_project)
    else:
        kernel_projects = all_supported_kernels.get(args.report_kernel)
        if kernel_projects is None:
            print("The specified kernel is not supported yet:", args.report_kernel)
            print("The supported kernels are:", ' '.join(all_supported_kernels.keys()))
            return
        for project_alias_name in kernel_projects:
            project_alias = all_supported_projects.get(project_alias_name)
            if project_alias is None:
                print(f"The project is not configured correctly: {project_alias_name}, project_alias is not found")
                return

            project_alias['alias_name'] = project_alias_name
            ## https://gist.github.com/mrchapp/b902ae8f27adfb5b420d271d3e9869d7
            if project_alias.get('slug') and project_alias.get('group'):
                squad_group = squad.group(project_alias.get('group'))
                if squad_group is not None:
                    squad_project = squad_group.project(project_alias.get('slug'))
                else:
                    logger.warn(f"The squad group is not found: {project_alias.get('group')}")
            elif project_alias.get('project_id'):
                squad_project = squad.projects(count=1, id=project_alias.get('project_id'))[project_alias.get('project_id')]
            else:
                squad_project = None

            if squad_project is not None:
                squad_project.project_alias = project_alias
                target_squad_projects.append(squad_project)
            else:
                print(f"The project is not configured correctly: {project_alias_name}, it could not be found on squad" )
                return

    error_projects = []
    success_projects = []
    regression_projects = []
    for target_squad_project in target_squad_projects:
        project_alias = target_squad_project.project_alias
        environment = project_alias.get("environment", None)
        project_name = target_squad_project.full_name
        # builds = target_squad_project.builds(count=10, fields="id,version,finished,datetime", ordering="-datetime").values()

        builds = target_squad_project.builds().values()
        goodruns = find_best_two_runs(builds, project_name, exact_ver1=args.build, exact_ver2=args.base_build, environment=environment)

        if len(goodruns) < 2 :
            print("\nNOTE: project " + target_squad_project.name  + " did not have 2 good runs\n")
            error_project = {
                                'project':target_squad_project,
                                'goodruns': goodruns,
                                'args': args
                            }
            error_projects.append(error_project)
        else:
            add_unique_kernel(unique_kernels, goodruns[1].version, target_squad_project, unique_kernel_info)
            regressions = find_regressions(goodruns)
            antiregressions = find_antiregressions(goodruns)

            regressions_failure = [ failure for failure in regressions if failure.get('result') == 'fail' ]
            regressions_assumption = [ failure for failure in regressions if failure.get('result') == 'ASSUMPTION_FAILURE' ]
            antiregressions_failure = [ failure for failure in antiregressions if failure.get('result') == 'fail' ]

            goodruns[1].numbers.number_regressions = len(regressions_failure)
            goodruns[1].numbers.number_antiregressions = len(antiregressions_failure)
            work_total_numbers.addWithTestNumbers(goodruns[1].numbers)

            set_regressions_testtype(regressions, flakes, project_alias)

            success_project = {
                                    'project':target_squad_project,
                                    'goodruns': goodruns,
                                    'regressions': regressions,
                                    'antiregressions': antiregressions,
                                    'trim_number': trim_number,
                                }
            success_projects.append(success_project)

            if trim_number > 0:
                print_number = int(trim_number/2)
                project_trimmed_lines = 0
                if len(regressions_failure) > trim_number:
                    project_trimmed_lines_failure = len(regressions_failure) - print_number * 2
                    project_trimmed_lines = project_trimmed_lines + project_trimmed_lines_failure
                if len(regressions_assumption) > trim_number:
                    project_trimmed_lines_assumption = len(regressions_assumption) - print_number * 2
                    project_trimmed_lines = project_trimmed_lines + project_trimmed_lines_assumption

                if project_trimmed_lines > 0:
                    regression_project = {
                                            'project':target_squad_project,
                                            'goodruns': goodruns,
                                            'project_trimmed_lines': project_trimmed_lines
                                        }
                    regression_projects.append(regression_project)

    with open(path_outputfile, "w") as fd_output:
        for error_project in error_projects:
            print_error_project(fd_output, error_project)

        for regression_project in regression_projects:
            print_regression_project(fd_output, regression_project, number_regression_modules=5, number_testcases_per_regression_modules=3)

        ##  output summary information
        report_kernels_in_report(fd_output, unique_kernels, unique_kernel_info, work_total_numbers)

        do_boilerplate(fd_output)

        for success_project in success_projects:
            print_success_project(fd_output, success_project)

    duration = time.time() - start
    print("PERFORMANCE INFO: it took %.3f seconds to finish the report" % (duration))
    after_mem = mem_snapshot()
    print("PERFORMANCE INFO: mem before: %d MB, mem after: %d MB (%d MB more)" % (before_mem, after_mem, after_mem - before_mem))
    return # end of this tunction
