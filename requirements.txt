jinja2
pandas
requests
pyyaml
squad-client>=0.22
bugzilla
reportlab==3.6.12
matplotlib
