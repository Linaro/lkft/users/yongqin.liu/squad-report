#!/usr/bin/env python3

import argparse
import boto3
import botocore.exceptions
import email.message
import email.parser
import email.policy
import email.utils


def send_email(msg):
    client = boto3.client("ses", region_name="us-east-1")

    source = _build_source(msg)
    destination = _build_destination(msg)
    message = _build_message(msg)

    try:
        response = client.send_email(
            Source=source,
            Destination=destination,
            Message=message,
        )
    except botocore.exceptions.NoCredentialsError:
        raise
    except botocore.exceptions.ParamValidationError:
        raise

    return response


def _build_source(msg):
    return msg["From"]


def _build_destination(msg):
    to_addresses = email.utils.getaddresses(msg.get_all("to", []))
    cc_addresses = email.utils.getaddresses(msg.get_all("cc", []))

    return {
        "ToAddresses": [y for x, y in to_addresses],
        "CcAddresses": [y for x, y in cc_addresses],
    }


def _build_message(msg):
    subject = msg["Subject"]
    body = msg.get_body(("plain",)).get_content()

    return {
        "Subject": {"Charset": "UTF-8", "Data": subject},
        "Body": {"Text": {"Charset": "UTF-8", "Data": body}},
    }


def arg_parser():
    parser = argparse.ArgumentParser(description="Send an email via AWS SES")

    parser.add_argument(
        "email",
        help="Send this email",
    )

    return parser


def email_parser():
    return email.parser.Parser(policy=email.policy.default)


def main():
    args = arg_parser().parse_args()

    try:
        with open(args.email) as fp:
            msg = email_parser().parse(fp)
    except FileNotFoundError:
        raise

    send_email(msg)


if __name__ == "__main__":
    main()
