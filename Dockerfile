FROM python:3.8-slim-buster

COPY . /build

ENV DEBIAN_FRONTEND=noninteractive
ENV PERL_MM_USE_DEFAULT=1

# hadolint ignore=DL3008
RUN apt-get update && apt-get install -y --no-install-recommends \
      curl \
      gcc \
      g++ \
      git \
      jq \
      libc6-dev \
      make \
      procps \
      r-base \
      unzip \
      wget \
      && apt-get clean \
      && rm -rf /var/lib/apt/lists/* \
      && cpan JSON \
      Cpanel::JSON::XS \
      List::BinarySearch \
      YAML \
      && R -e 'install.packages("rjson")' \
      && apt-get remove -y \
      g++ \
      gcc \
      libc6-dev \
      make \
      && apt-get autoremove -y

WORKDIR /build

RUN pip3 install --no-cache-dir --quiet -r requirements.txt && \
    python3 setup.py sdist bdist_wheel && \
    pip3 install --no-cache-dir --quiet ./dist/*.whl && \
    pip3 install --no-cache-dir --quiet -r scripts/requirements.txt
COPY scripts/build-email scripts/send-email scripts/squad-find-regressions /usr/local/bin/
RUN rm -rf /build

CMD ["squad-report", "--help"]
