usage: squad-report [-h] [--cache CACHE] [--token TOKEN] [--url URL]
                    [--group GROUP] [--project PROJECT] [--build BUILD]
                    [--base-build BASE_BUILD]
                    [--environments ENVIRONMENTS | --environment-prefixes ENVIRONMENT_PREFIXES]
                    [--suites SUITES | --suite-prefixes SUITE_PREFIXES]
                    [--email-cc EMAIL_CC] [--email-from EMAIL_FROM]
                    [--email-subject EMAIL_SUBJECT] [--email-to EMAIL_TO]
                    [--output OUTPUT] [--send-on SEND_ON]
                    [--template TEMPLATE] [--config CONFIG]
                    [--config-report-type CONFIG_REPORT_TYPE]
                    [--perf-report-hook PERF_REPORT_HOOK]
                    [--perf-report-hook-args PERF_REPORT_HOOK_ARGS]
                    [--unfinished] [-v]

Create a report using data from SQUAD

optional arguments:
  -h, --help            show this help message and exit
  --cache CACHE         Cache squad-client requests with a timeout
  --token TOKEN         Authenticate to SQUAD using this token
  --url URL             URL of the SQUAD service
  --group GROUP         SQUAD group
  --project PROJECT     SQUAD project
  --build BUILD         SQUAD build
  --base-build BASE_BUILD
                        SQUAD build to compare to
  --environments ENVIRONMENTS
                        List of SQUAD environments to include
  --environment-prefixes ENVIRONMENT_PREFIXES
                        List of prefixes of SQUAD environments to include
  --suites SUITES       List of SQUAD suites to include
  --suite-prefixes SUITE_PREFIXES
                        List of prefixes of SQUAD suites to include
  --email-cc EMAIL_CC   Create the report with email cc
  --email-from EMAIL_FROM
                        Create the report with email from
  --email-subject EMAIL_SUBJECT
                        Create the report with this email subject
  --email-to EMAIL_TO   Create the report with email to
  --output OUTPUT       Write the report to this file
  --send-on SEND_ON     Always send an email, on failures, regressions or
                        always
  --template TEMPLATE   Create the report with this template
  --config CONFIG       Create the report using this configuration
  --config-report-type CONFIG_REPORT_TYPE
                        Set the report type to use in the local config file
  --perf-report-hook PERF_REPORT_HOOK
                        Set performance report script hook
  --perf-report-hook-args PERF_REPORT_HOOK_ARGS
                        Set performance script comma separated arguments
  --unfinished          Create a report even if a build is not finished
  -v, --version         Print out the version
